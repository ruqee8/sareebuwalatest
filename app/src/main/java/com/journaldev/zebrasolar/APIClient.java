package com.journaldev.zebrasolar;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by anupamchugh on 05/01/17.
 */

public class APIClient {

    private static Retrofit retrofit = null;
    public static String token_key ="";

    public static Retrofit getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        /*OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request()
                        .newBuilder()
                        .addHeader("X-API-KEY", "wssko8s4k0ccsks8oc8cc00oc0o4g0sookc880k8")
                        .build());
            }
        }).addInterceptor(interceptor).build();
        */

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request()
                        .newBuilder()
                        .addHeader("X-API-KEY", token_key)
                        .build());
            }
        }).addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                //.baseUrl("http://localhost/dinansa_test1/index.php")
                //.baseUrl("https://sandbox.momentumlk.net/index.php/")
                //.baseUrl("https://dev.momentumlk.net/index.php/")

                //.baseUrl("https://localhost/momentumerp/index.php/")
                //.baseUrl("https://thakshilawa.thakshila.lk/index.php/")

                //.baseUrl("http://10.0.2.2/premium-zebra/premium_classic/index.php/")
                //.baseUrl("https://zebra.momentum.web.lk/index.php/")
                //.baseUrl("https://demopremium.momentum.web.lk")
                .baseUrl("https://bandaranayakatraders.momentum.web.lk")
                //.baseUrl("https://ranaweerainvestment.momentum.web.lk")
                //.baseUrl("https://rudilproducts.momentumlk.net/index.php/")

                //.baseUrl("http://10.0.0.2/dinansa_test1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit;
    }

    public static Retrofit getClientWithoutKey() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                //.baseUrl("http://localhost/dinansa_test1/index.php")
                //.baseUrl("https://sandbox.momentumlk.net/index.php/")
                //.baseUrl("https://dev.momentumlk.net/index.php/")

                //.baseUrl("https://localhost/momentumerp/index.php/")
//                .baseUrl("https://thakshilawa.thakshila.lk/index.php/")

                //.baseUrl("http://10.0.2.2/premium-zebra/premium_classic/index.php/")
                //.baseUrl("https://zebra.momentum.web.lk/index.php/")
                //.baseUrl("https://demopremium.momentum.web.lk")
                .baseUrl("https://bandaranayakatraders.momentum.web.lk")
                //.baseUrl("https://ranaweerainvestment.momentum.web.lk")
                //.baseUrl("https://rudilproducts.momentumlk.net/index.php/")

                //.baseUrl("http://10.0.0.2/dinansa_test1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit;
    }


}
