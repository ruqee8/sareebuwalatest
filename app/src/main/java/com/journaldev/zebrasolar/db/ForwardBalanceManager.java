package com.journaldev.zebrasolar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.journaldev.zebrasolar.model.ForwardBalanceModel;
import com.journaldev.zebrasolar.model.ForwardPayment;

public class ForwardBalanceManager {

    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;
    private Context m_context;

    public ForwardBalanceManager(Context context) {

        m_dbHelper = DataBaseHelper.getInstance(context);
        m_context = context;
    }

    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void close() {
        m_dbHelper.close();
    }


    public void insertForwardPayment(ForwardPayment payment) {

        open();

        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_FORWARD_JOBID,payment.getJobId());
        values.put(DataBaseHelper.FI_DUEPAYMENT_DATE ,payment.getRelatedDuedate());
        values.put(DataBaseHelper.FI_AMOUNT ,payment.getPaymentAmount());
        values.put(DataBaseHelper.FI_PAYMENT_ID,payment.getPaymentId());

        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_FORWARD_PAYMENT, null, values);
        close();

        Log.d("FORWARD PAYMENT INS:" , "Inserted @" + id + "  " + values.toString());

    }


    public ForwardPayment getMaxDatedForwardPayment(String jobId)
    {
        open();

        Cursor c = m_database.rawQuery("SELECT "+
                        "MAX("+DataBaseHelper.FI_DUEPAYMENT_DATE+"),"
                        +DataBaseHelper.FI_AMOUNT+","
                        +DataBaseHelper.FI_PAYMENT_ID
                        +" FROM "+DataBaseHelper.TAB_FORWARD_PAYMENT
                        + " WHERE " + DataBaseHelper.FI_FORWARD_JOBID+
                        "='"+jobId+"'"
                , null);

       ForwardPayment fwPayment = new ForwardPayment();
       fwPayment.setSet(false);


        if (c.moveToFirst()){
            do {
                // Passing values
                String forwardDueDate = c.getString(0);
                String amount = c.getString(1);
                String paymentId= c.getString(2);

                //add all the data here
                fwPayment.setJobId(jobId);
                fwPayment.setRelatedDuedate(forwardDueDate);
                fwPayment.setPaymentAmount(amount);
                fwPayment.setPaymentId(paymentId);
                fwPayment.setSet(true);

                // Do something Here with values
            } while(c.moveToNext());
        }

        c.close();

        close();

        return fwPayment;
    }



    ////////////////////////////////////FORWARD BALANCE PAYMENT///////////////////////////////////////////

    public void insertForwardBalance(ForwardBalanceModel fwb) {

        open();

        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_FORWARD_JOBID,fwb.getJobId());
        values.put(DataBaseHelper.FI_FORWARD_BALANCE ,fwb.getForwardBalance());


        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_JOB_FORWARD_BALANCE, null, values);
        close();

        Log.d("FORWARD Balance INS:" , "Inserted @" + id + "  " + values.toString());

    }

    public void updateForwardBalance(ForwardBalanceModel fwb) {

        open();

        String query = "UPDATE " + DataBaseHelper.TAB_JOB_FORWARD_BALANCE + " SET " + DataBaseHelper.FI_FORWARD_BALANCE +
                        " = " + fwb.getForwardBalance() +
                        " WHERE " + DataBaseHelper.FI_FORWARD_JOBID + "='" + fwb.getJobId() + "'";

        m_database.execSQL(query);

        Log.d("FORWARD Balance INS:" , "Inserted @" + fwb.getJobId() + "  " + fwb.getForwardBalance());

        close();

    }

    public String getCurrentForwardBalance(String jobId)
    {

        open();

        String currForwardBalance = "0";

        String query = "SELECT "+DataBaseHelper.FI_FORWARD_BALANCE +" FROM "+DataBaseHelper.TAB_JOB_FORWARD_BALANCE + " WHERE "
                       +DataBaseHelper.FI_FORWARD_JOBID +"='" + jobId+ "'";
        Cursor c = m_database.rawQuery(query, null);
        if (c.moveToFirst()){
            do {
                currForwardBalance = c.getString(0);
            } while(c.moveToNext());
        }
        c.close();

        close();

        return currForwardBalance;

    }

    public void removeForwardBalancesForJob(String jobId)
    {
        open();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_JOB_FORWARD_BALANCE+" WHERE "+DataBaseHelper.FI_FORWARD_JOBID +"='" + jobId+ "'");
        m_database.execSQL("DELETE FROM "+DataBaseHelper.TAB_FORWARD_PAYMENT + " WHERE " + DataBaseHelper.FI_FORWARD_JOBID + "='"+jobId+"'");
        close();
    }


}
