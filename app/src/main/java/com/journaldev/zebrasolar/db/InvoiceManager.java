package com.journaldev.zebrasolar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.journaldev.zebrasolar.model.Invoice;
import com.journaldev.zebrasolar.model.Payment;

import java.util.ArrayList;
import java.util.List;

public class InvoiceManager {

    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;
    private Context m_context;

    public InvoiceManager(Context context) {

        m_dbHelper = DataBaseHelper.getInstance(context);
        m_context = context;
    }

    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void close() {
        m_dbHelper.close();
    }


    public void insertInvoice(Invoice invoice) {

        open();

        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_INV_JOBID,invoice.getJobId());
        values.put(DataBaseHelper.FI_INV_INVID ,invoice.getInvoiceID());
        values.put(DataBaseHelper.FI_INV_REFID ,invoice.getRefId());
        values.put(DataBaseHelper.FI_INV_REFSEQ ,invoice.getRefSeqId());
        values.put(DataBaseHelper.FI_INV_DESCRIPTION ,invoice.getDescription());
        values.put(DataBaseHelper.FI_INV_TOTAL ,invoice.getTotal());
        values.put(DataBaseHelper.FI_INV_CREATED_AT ,invoice.getDate());


        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_INVOICES, null, values);
        close();

        Log.d("PAYMENT INS:" , "Inserted @" + id + "  " + values.toString());

    }

    public List<Invoice> getInvoiceList(String jobId)
    {
        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_INV_JOBID+","
                        +DataBaseHelper.FI_INV_REFID+","
                        +DataBaseHelper.FI_INV_REFSEQ+","
                        +DataBaseHelper.FI_INV_INVID+","
                        +DataBaseHelper.FI_INV_DESCRIPTION+","
                        +DataBaseHelper.FI_INV_TOTAL+","
                        +DataBaseHelper.FI_PAY_CREATED_AT
                        +" FROM "+DataBaseHelper.TAB_INVOICES
                        + " WHERE " + DataBaseHelper.FI_INV_JOBID+
                        "='"+jobId+"'"
                , null);

        List<Invoice> invList  = new ArrayList<Invoice>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                String userId = c.getString(1);
                String userSeq= c.getString(2);
                String invId= c.getString(3);
                String description= c.getString(4);
                String total= c.getString(5);
                String createdAt= c.getString(6);

                //add all the data here
                Invoice sample = new Invoice();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setInvoiceID(invId);
                sample.setDescription(description);
                sample.setTotal(total);
                sample.setDate(createdAt);

                invList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        c.close();

        close();


        if (invList == null){
            return new ArrayList<Invoice>();
        }

        return invList;
    }

    public List<Payment> getNotSyncedPaymentList(int LastSyncedRefSeq)
    {
        open();

        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_PAY_JOBID+","
                        +DataBaseHelper.FI_PAY_REFID+","
                        +DataBaseHelper.FI_PAY_REFSEQ+","
                        +DataBaseHelper.FI_PAY_PAYID+","
                        +DataBaseHelper.FI_PAY_AMOUNT+","
                        +DataBaseHelper.FI_PAY_PANALTY+","
                        +DataBaseHelper.FI_PAY_PAYTYPE+","
                        +DataBaseHelper.FI_PAY_CASH_CHECQUE+","
                        +DataBaseHelper.FI_PAY_CREATED_AT+","
                        +DataBaseHelper.FI_PAY_CHEQUE_STATUS
                        +" FROM "+DataBaseHelper.TAB_PAYMENTS
                        + " where "+DataBaseHelper.FI_PAY_REFSEQ+" > "+ LastSyncedRefSeq
                , null);

        List<Payment> payList  = new ArrayList<Payment>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                String userId = c.getString(1);
                String userSeq= c.getString(2);
                String payId= c.getString(3);
                String payAmount= c.getString(4);
                String payPanalty= c.getString(5);
                String payType= c.getString(6);
                String cashCheque = c.getString(7);
                String createdAt= c.getString(8);
                String chequeStatus = c.getString(9);

                //add all the data here
                Payment sample = new Payment();
                /*sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setPaymentId(payId);
                sample.setPaymentAmount(payAmount);
                sample.setPanaltyAmount(payPanalty);
                sample.setPayType(payType);
                sample.setPayCashChequeType(cashCheque);
                sample.setDateWhenSynced(createdAt);
                sample.setChequeStatus(chequeStatus);*/

                payList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        close();


        if (payList == null){
            return new ArrayList<Payment>();
        }


        return payList;
    }





    public String getNextInvoiceIdForThisJob(String jobId)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_INV_INVID + ")");
        query.append(" FROM " + DataBaseHelper.TAB_INVOICES);
        query.append(" WHERE "+DataBaseHelper.FI_INV_JOBID+"='"+ jobId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                Integer payId = c.getInt(0);
                payId = payId + 1;
                return Integer.toString(payId);

            } while(c.moveToNext());
        }
        c.close();

        close();

        return "0";

    }

    public String getNextInvoiceRefSeq(String refId)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_INV_REFSEQ + ")");
        query.append(" FROM " + DataBaseHelper.TAB_INVOICES);
        query.append(" WHERE "+DataBaseHelper.FI_INV_REFID+"='"+ refId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                Integer refSeq = c.getInt(0);
                refSeq = refSeq + 1;
                return Integer.toString(refSeq);

            } while(c.moveToNext());
        }
        c.close();

        close();

        return "0";

    }


}
