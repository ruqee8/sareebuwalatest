package com.journaldev.zebrasolar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.utils.GeneralUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class JobManager {

    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;
    private Context m_context;

    public JobManager(Context context) {

        m_dbHelper = DataBaseHelper.getInstance(context);
        m_context = context;
    }

    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void close() {
        /*m_dbHelper.close()*/;
    }


    public void insertJob(Job job) {

        open();

        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_JOBS_ORIGIN_ID ,job.getJobId());
        values.put(DataBaseHelper.FI_JOBS_JOB_ID ,job.getJobId());
        values.put(DataBaseHelper.FI_JOBS_USER_ID ,job.getRefId());
        values.put(DataBaseHelper.FI_JOBS_USER_SEQ_ID ,job.getRefSeqId());
        values.put(DataBaseHelper.FI_JOBS_CUSTOMER_ID ,job.getCustomerId());
        values.put(DataBaseHelper.FI_JOBS_CUSTOMER_NAME ,job.getCustomerName());
        values.put(DataBaseHelper.FI_JOBS_CUSTOMER_PHONE ,job.getPhone());
        values.put(DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS ,job.getAddress());
        values.put(DataBaseHelper.FI_JOBS_TOTAL_AMOUNT ,job.getTotalAmount());
        values.put(DataBaseHelper.FI_JOBS_DOWNPAYMENT  ,job.getDownPayment());
        values.put(DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS  ,job.getNoOfInstallments());
        values.put(DataBaseHelper.FI_JOBS_PROD_ID  ,job.getProdId());
        values.put(DataBaseHelper.FI_JOBS_AREA_ID  ,job.getAreaId());
        values.put(DataBaseHelper.FI_JOBS_LATITUDE  ,job.getLatitude());
        values.put(DataBaseHelper.FI_JOBS_LONGTITUDE  ,job.getLongtitude());
        values.put(DataBaseHelper. FI_JOBS_INSTALLMENT_TYPE  ,job.getInstallmentType());
        values.put(DataBaseHelper.FI_JOBS_CREATED_AT , GeneralUtils.getDateTime());
        values.put(DataBaseHelper.FI_JOBS_DATE ,job.getDate());
        values.put(DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE ,job.getFirstPaymentDate());
        values.put(DataBaseHelper.FI_JOBS_COLLECTOR_ID ,job.getCollectorId());
        values.put(DataBaseHelper.FI_JOBS_STATUS ,job.getStatus());
        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_JOBS, null, values);
        close();

        Log.d("JOB INS:" , "Inserted @" + id + "  " + values.toString());

    }


    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


    public List<Job> getJobList()
    {
        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_JOBS_JOB_ID+","
                        +DataBaseHelper.FI_JOBS_ORIGIN_ID+","
                        +DataBaseHelper.FI_JOBS_USER_ID+","
                        +DataBaseHelper.FI_JOBS_USER_SEQ_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_NAME+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_PHONE+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS+","
                        +DataBaseHelper.FI_JOBS_TOTAL_AMOUNT+","
                        +DataBaseHelper.FI_JOBS_DOWNPAYMENT +","
                        +DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS +","
                        +DataBaseHelper.FI_JOBS_PROD_ID +","
                        +DataBaseHelper.FI_JOBS_AREA_ID +","
                        +DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE +","
                        +DataBaseHelper.FI_JOBS_CREATED_AT+","
                        +DataBaseHelper.FI_JOBS_DATE+","
                        +DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+","
                        +DataBaseHelper.FI_JOBS_COLLECTOR_ID+","
                        +DataBaseHelper.FI_JOBS_STATUS+","
                        +DataBaseHelper.FI_JOBS_LATITUDE+","
                        +DataBaseHelper.FI_JOBS_LONGTITUDE
                        +" FROM "+DataBaseHelper.TAB_JOBS
                        +" WHERE "+DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.NEW.getValue()+"' OR "
                        +DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.UPDATED.getValue()+"'"
                , null);

        List<Job> jobList  = new ArrayList<Job>();

        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int originId = c.getInt(1);
                int userId = c.getInt(2);
                int userSeq= c.getInt(3);
                String customerId= c.getString(4);
                String customerName= c.getString(5);
                String customerPhone= c.getString(6);
                String customerAddress= c.getString(7);
                double totalAmount= c.getDouble(8);
                double downPayment = c.getDouble(9);
                int noOfInstallments = c.getInt(10);
                int prodId = c.getInt(11);
                int areaId = c.getInt(12);
                int installmentType = c.getInt(13);
                String createdAt = c.getString(14);
                String date = c.getString(15);
                String firstPaymentdate = c.getString(16);
                int collectorId = c.getInt(17);
                int status = c.getInt(18);
                double latitude = c.getDouble(19);
                double longtitude = c.getDouble(20);


                //add all the data here
                Job sample = new Job();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setCustomerId(customerId);
                sample.setCustomerName(customerName);
                sample.setPhone(customerPhone);
                sample.setAddress(customerAddress);
                sample.setTotalAmount(totalAmount);
                sample.setDownPayment(downPayment);
                sample.setInstallmentType(installmentType);
                sample.setNoOfInstallments(noOfInstallments);
                sample.setDate(date);
                sample.setFirstPaymentDate(firstPaymentdate);
                sample.setProdId(prodId);
                sample.setAreaId(areaId);
                sample.setCollectorId(collectorId);
                sample.setOrginId(originId);
                sample.setStatus(status);
                sample.setLatitude(latitude);
                sample.setLongtitude(longtitude);



                jobList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        close();


        if (jobList == null){
            return new ArrayList<Job>();
        }

        Collections.sort(jobList, new Comparator<Job>() {
            @Override
            public int compare(Job s1, Job s2) {
                return s1.getJobId().compareToIgnoreCase(s2.getJobId());
            }
        });

        return jobList;
    }

    public List<Job> getJobListForCollectorId(int collectorId)
    {
        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_JOBS_JOB_ID+","
                        +DataBaseHelper.FI_JOBS_ORIGIN_ID+","
                        +DataBaseHelper.FI_JOBS_USER_ID+","
                        +DataBaseHelper.FI_JOBS_USER_SEQ_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_NAME+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_PHONE+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS+","
                        +DataBaseHelper.FI_JOBS_TOTAL_AMOUNT+","
                        +DataBaseHelper.FI_JOBS_DOWNPAYMENT +","
                        +DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS +","
                        +DataBaseHelper.FI_JOBS_PROD_ID +","
                        +DataBaseHelper.FI_JOBS_AREA_ID +","
                        +DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE +","
                        +DataBaseHelper.FI_JOBS_CREATED_AT+","
                        +DataBaseHelper.FI_JOBS_DATE+","
                        +DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+","
                        +DataBaseHelper.FI_JOBS_COLLECTOR_ID+","
                        +DataBaseHelper.FI_JOBS_STATUS+","
                        +DataBaseHelper.FI_JOBS_LATITUDE+","
                        +DataBaseHelper.FI_JOBS_LONGTITUDE
                        +" FROM "+DataBaseHelper.TAB_JOBS
                        +" WHERE ("+DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.NEW.getValue()+"' OR "
                        +DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.UPDATED.getValue()+"'"
                        +") AND "+DataBaseHelper.FI_JOBS_COLLECTOR_ID + "='" +collectorId+"'"
                , null);

        List<Job> jobList  = new ArrayList<Job>();

        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int originId = c.getInt(1);
                int userId = c.getInt(2);
                int userSeq= c.getInt(3);
                String customerId= c.getString(4);
                String customerName= c.getString(5);
                String customerPhone= c.getString(6);
                String customerAddress= c.getString(7);
                double totalAmount= c.getDouble(8);
                double downPayment = c.getDouble(9);
                int noOfInstallments = c.getInt(10);
                int prodId = c.getInt(11);
                int areaId = c.getInt(12);
                int installmentType = c.getInt(13);
                String createdAt = c.getString(14);
                String date = c.getString(15);
                String firstPaymentDate = c.getString(16);
                int collectorIdThis = c.getInt(17);
                int status = c.getInt(18);
                double latitude = c.getDouble(19);
                double longtitude = c.getDouble(20);


                //add all the data here
                Job sample = new Job();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setCustomerId(customerId);
                sample.setCustomerName(customerName);
                sample.setPhone(customerPhone);
                sample.setAddress(customerAddress);
                sample.setTotalAmount(totalAmount);
                sample.setDownPayment(downPayment);
                sample.setInstallmentType(installmentType);
                sample.setNoOfInstallments(noOfInstallments);
                sample.setDate(date);
                sample.setFirstPaymentDate(firstPaymentDate);
                sample.setProdId(prodId);
                sample.setAreaId(areaId);
                sample.setCollectorId(collectorIdThis);
                sample.setOrginId(originId);
                sample.setStatus(status);
                sample.setLatitude(latitude);
                sample.setLongtitude(longtitude);


                jobList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        close();


        if (jobList == null){
            return new ArrayList<Job>();
        }

        Collections.sort(jobList, new Comparator<Job>() {
            @Override
            public int compare(Job s1, Job s2) {
                return s1.getJobId().compareToIgnoreCase(s2.getJobId());
            }
        });

        return jobList;
    }

    public List<Job> getJobListWithArgs(int collectorId,int areaID,boolean isDay,boolean isWeek,boolean isMonth)
    {

        String collecetor  = "";
        String area = "";
        String types = "";

        ////////////////////////////////////////
        if (collectorId == -1)
        {
            collecetor = "%";
        }
        else {
            collecetor = Integer.toString(collectorId);
        }

        /////////////////////////////////
        if (areaID == -1)
        {
            area = "%";
        }
        else {
            area = Integer.toString(areaID);
        }

        /////////////////////////////////////////

        if(isDay)
        {
            types = types + Enums.INSTALLMENTTYPE.DAILY.getValue() +",";
        }
        if(isWeek)
        {
            types = types + Enums.INSTALLMENTTYPE.WEEKLY.getValue() +",";
        }
        if(isMonth)
        {
            types = types + Enums.INSTALLMENTTYPE.MONTHLY.getValue() +",";
        }

        if (types.endsWith(",")) {
            types = types.substring(0, types.length()-1);
        }




        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_JOBS_JOB_ID+","
                        +DataBaseHelper.FI_JOBS_ORIGIN_ID+","
                        +DataBaseHelper.FI_JOBS_USER_ID+","
                        +DataBaseHelper.FI_JOBS_USER_SEQ_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_NAME+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_PHONE+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS+","
                        +DataBaseHelper.FI_JOBS_TOTAL_AMOUNT+","
                        +DataBaseHelper.FI_JOBS_DOWNPAYMENT +","
                        +DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS +","
                        +DataBaseHelper.FI_JOBS_PROD_ID +","
                        +DataBaseHelper.FI_JOBS_AREA_ID +","
                        +DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE +","
                        +DataBaseHelper.FI_JOBS_CREATED_AT+","
                        +DataBaseHelper.FI_JOBS_DATE+","
                        +DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+","
                        +DataBaseHelper.FI_JOBS_COLLECTOR_ID+","
                        +DataBaseHelper.FI_JOBS_STATUS+","
                        +DataBaseHelper.FI_JOBS_LATITUDE+","
                        +DataBaseHelper.FI_JOBS_LONGTITUDE
                        +" FROM "+DataBaseHelper.TAB_JOBS
                        +" WHERE ("+DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.NEW.getValue()+"' OR "
                        +DataBaseHelper.FI_JOBS_STATUS + "='" +Enums.Status.UPDATED.getValue()+"'"
                        +") AND "+DataBaseHelper.FI_JOBS_COLLECTOR_ID + " like '" +collecetor+"'"
                        +" AND "+DataBaseHelper.FI_JOBS_AREA_ID + " like '" +area+"'"
                        +" AND " + DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE + " IN " + "(" +types+ ")"
                , null);

        List<Job> jobList  = new ArrayList<Job>();

        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int originId = c.getInt(1);
                int userId = c.getInt(2);
                int userSeq= c.getInt(3);
                String customerId= c.getString(4);
                String customerName= c.getString(5);
                String customerPhone= c.getString(6);
                String customerAddress= c.getString(7);
                double totalAmount= c.getDouble(8);
                double downPayment = c.getDouble(9);
                int noOfInstallments = c.getInt(10);
                int prodId = c.getInt(11);
                int areaId = c.getInt(12);
                int installmentType = c.getInt(13);
                String createdAt = c.getString(14);
                String date = c.getString(15);
                String firstPaymentDate = c.getString(16);
                int collectorIdThis = c.getInt(17);
                int status = c.getInt(18);
                double latitude = c.getDouble(19);
                double longtitude = c.getDouble(20);


                //add all the data here
                Job sample = new Job();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setCustomerId(customerId);
                sample.setCustomerName(customerName);
                sample.setPhone(customerPhone);
                sample.setAddress(customerAddress);
                sample.setTotalAmount(totalAmount);
                sample.setDownPayment(downPayment);
                sample.setInstallmentType(installmentType);
                sample.setNoOfInstallments(noOfInstallments);
                sample.setDate(date);
                sample.setFirstPaymentDate(firstPaymentDate);
                sample.setProdId(prodId);
                sample.setAreaId(areaId);
                sample.setCollectorId(collectorIdThis);
                sample.setOrginId(originId);
                sample.setStatus(status);
                sample.setLatitude(latitude);
                sample.setLongtitude(longtitude);


                jobList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        close();


        if (jobList == null){
            return new ArrayList<Job>();
        }

        Collections.sort(jobList, new Comparator<Job>() {
            @Override
            public int compare(Job s1, Job s2) {
                return s1.getJobId().compareToIgnoreCase(s2.getJobId());
            }
        });

        return jobList;
    }


    public Job getJob(String jobId)
    {
        open();
        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_JOBS_JOB_ID+","
                        +DataBaseHelper.FI_JOBS_ORIGIN_ID+","
                        +DataBaseHelper.FI_JOBS_USER_ID+","
                        +DataBaseHelper.FI_JOBS_USER_SEQ_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_NAME+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_PHONE+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS+","
                        +DataBaseHelper.FI_JOBS_TOTAL_AMOUNT+","
                        +DataBaseHelper.FI_JOBS_DOWNPAYMENT +","
                        +DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS +","
                        +DataBaseHelper.FI_JOBS_PROD_ID +","
                        +DataBaseHelper.FI_JOBS_AREA_ID +","
                        +DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE +","
                        +DataBaseHelper.FI_JOBS_CREATED_AT+","
                        +DataBaseHelper.FI_JOBS_DATE+","
                        +DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+","
                        +DataBaseHelper.FI_JOBS_COLLECTOR_ID+","
                        +DataBaseHelper.FI_JOBS_STATUS+","
                        +DataBaseHelper.FI_JOBS_LATITUDE+","
                        +DataBaseHelper.FI_JOBS_LONGTITUDE
                        +" FROM "+DataBaseHelper.TAB_JOBS
                        +" WHERE "+DataBaseHelper.FI_JOBS_JOB_ID + "='" +jobId+"'"
                , null);

        Job sample = new Job();

        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int originId = c.getInt(1);
                int userId = c.getInt(2);
                int userSeq= c.getInt(3);
                String customerId= c.getString(4);
                String customerName= c.getString(5);
                String customerPhone= c.getString(6);
                String customerAddress= c.getString(7);
                double totalAmount= c.getDouble(8);
                double downPayment = c.getDouble(9);
                int noOfInstallments = c.getInt(10);
                int prodId = c.getInt(11);
                int areaId = c.getInt(12);
                int installmentType = c.getInt(13);
                String createdAt = c.getString(14);
                String date = c.getString(15);
                String firstPaymentDate = c.getString(16);
                int collectorId = c.getInt(17);
                int status = c.getInt(18);
                double latitude =  c.getDouble(19);
                double longitude =  c.getDouble(20);


                //add all the data here
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setCustomerId(customerId);
                sample.setCustomerName(customerName);
                sample.setPhone(customerPhone);
                sample.setAddress(customerAddress);
                sample.setTotalAmount(totalAmount);
                sample.setDownPayment(downPayment);
                sample.setInstallmentType(installmentType);
                sample.setNoOfInstallments(noOfInstallments);
                sample.setDate(date);
                sample.setFirstPaymentDate(firstPaymentDate);
                sample.setProdId(prodId);
                sample.setAreaId(areaId);
                sample.setCollectorId(collectorId);
                sample.setOrginId(originId);
                sample.setStatus(status);
                sample.setLatitude(latitude);
                sample.setLongtitude(longitude);

                // Do something Here with values
            } while(c.moveToNext());
        }

        close();
        return sample;

    }

    public List<Job> getNotSyncedJobList(int LastSyncedRefSeq)
    {
        open();

        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_JOBS_JOB_ID+","
                        +DataBaseHelper.FI_JOBS_ORIGIN_ID+","
                        +DataBaseHelper.FI_JOBS_USER_ID+","
                        +DataBaseHelper.FI_JOBS_USER_SEQ_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ID+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_NAME+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_PHONE+","
                        +DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS+","
                        +DataBaseHelper.FI_JOBS_TOTAL_AMOUNT+","
                        +DataBaseHelper.FI_JOBS_DOWNPAYMENT +","
                        +DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS +","
                        +DataBaseHelper.FI_JOBS_PROD_ID +","
                        +DataBaseHelper.FI_JOBS_AREA_ID +","
                        +DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE +","
                        +DataBaseHelper.FI_JOBS_CREATED_AT+","
                        +DataBaseHelper.FI_JOBS_DATE+","
                        +DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+","
                        +DataBaseHelper.FI_JOBS_COLLECTOR_ID+","
                        +DataBaseHelper.FI_JOBS_STATUS+","
                        +DataBaseHelper.FI_JOBS_LONGTITUDE+","
                        +DataBaseHelper.FI_JOBS_LATITUDE
                        +" FROM "+DataBaseHelper.TAB_JOBS
                        + " where "+DataBaseHelper.FI_JOBS_USER_SEQ_ID+" > "+ LastSyncedRefSeq
                        + " AND "+DataBaseHelper.FI_JOBS_USER_ID + "='" + Users.getCurrentUserId()+"'"
                , null);

        List<Job> jobList  = new ArrayList<Job>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int originId = c.getInt(1);
                int userId = c.getInt(2);
                int userSeq= c.getInt(3);
                String customerId= c.getString(4);
                String customerName= c.getString(5);
                String customerPhone= c.getString(6);
                String customerAddress= c.getString(7);
                double totalAmount= c.getDouble(8);
                double downPayment = c.getDouble(9);
                int noOfInstallments = c.getInt(10);
                int prodId = c.getInt(11);
                int areaId = c.getInt(12);
                int installmentType = c.getInt(13);
                String createdAt = c.getString(14);
                String date = c.getString(15);
                String firstPaymentDate = c.getString(16);
                int collectorIdThis = c.getInt(17);
                int status = c.getInt(18);
                double longtitude = c.getDouble(19);
                double latitude = c.getDouble(20);


                //add all the data here
                Job sample = new Job();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setCustomerId(customerId);
                sample.setCustomerName(customerName);
                sample.setPhone(customerPhone);
                sample.setAddress(customerAddress);
                sample.setTotalAmount(totalAmount);
                sample.setDownPayment(downPayment);
                sample.setInstallmentType(installmentType);
                sample.setNoOfInstallments(noOfInstallments);
                sample.setDate(date);
                sample.setFirstPaymentDate(firstPaymentDate);
                sample.setProdId(prodId);
                sample.setAreaId(areaId);
                sample.setCollectorId(collectorIdThis);
                sample.setOrginId(originId);
                sample.setStatus(status);
                sample.setLongtitude(longtitude);
                sample.setLatitude(latitude);



                jobList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        close();


        if (jobList == null){
            return new ArrayList<Job>();
        }


        return jobList;
    }

    public boolean isValidJobID(String jobId) {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append(DataBaseHelper.FI_JOBS_JOB_ID );
        query.append(" FROM " + DataBaseHelper.TAB_JOBS);
        query.append(" WHERE "+DataBaseHelper.FI_JOBS_JOB_ID+"='"+ jobId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                return true;

            } while(c.moveToNext());
        }
        c.close();

        close();

        return false;
    }

    public int getNextRefSeq(int refId)
    {
        open();

        int jobTabSync1 = 0;

        //FI job I from job tale
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_JOBS_USER_SEQ_ID + ")");
        query.append(" FROM " + DataBaseHelper.TAB_JOBS);
        query.append(" WHERE "+DataBaseHelper.FI_JOBS_USER_ID+"='"+ refId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                jobTabSync1 = c.getInt(0);

            } while(c.moveToNext());
        }
        c.close();


        //get from sync Table
        int jobTabSync2 = 0;

        StringBuilder query2 = new StringBuilder();
        query2.append("SELECT ");
        query2.append(DataBaseHelper.FI_SYNC_LAST_REF);
        query2.append(" FROM " + DataBaseHelper.TAB_SYNC_TABLE);
        query2.append(" WHERE "+DataBaseHelper.FI_SYNC_USER_ID+"='"+ refId +"'");
        query2.append(" AND "+DataBaseHelper.FI_SYNC_TABLE_ID+"="+ Enums.TableType.JOB.getValue());
        Cursor c2 = m_database.rawQuery(query2.toString(), null);
        if (c2.moveToFirst()){
            do {
                jobTabSync2 = c2.getInt(0);

            } while(c2.moveToNext());
        }
        c2.close();
        close();

        int nextRefSeq = 0;

        if (jobTabSync1 > jobTabSync2)
        {
            nextRefSeq = jobTabSync1+1;
        }
        else
        {
            nextRefSeq =jobTabSync2+1;
        }

        return nextRefSeq;

    }

    public String getLastJobIdForUser(String user)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_JOBS_USER_SEQ_ID + ")");
        query.append(","+DataBaseHelper.FI_JOBS_JOB_ID);
        query.append(" FROM " + DataBaseHelper.TAB_JOBS);
        query.append(" WHERE "+DataBaseHelper.FI_JOBS_USER_ID+"='"+ user +"'");

        Log.w("JobManager - getLastJob",query.toString());

        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                String jobId = c.getString(1);
                close();
                return jobId;



            } while(c.moveToNext());
        }
        c.close();

        close();

        return "-";

    }

    public int getLastJobSeqForUser(String user)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_JOBS_USER_SEQ_ID + ")");
        query.append(" FROM " + DataBaseHelper.TAB_JOBS);
        query.append(" WHERE "+DataBaseHelper.FI_JOBS_USER_ID+"='"+ user +"'");

        Log.w("JobManager - getLastJob",query.toString());

        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                int jobSeq = c.getInt(0);
                close();
                return jobSeq;



            } while(c.moveToNext());
        }
        c.close();

        close();

        return 0;

    }

    public int getTotalJobs()
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("select count(*) from jobs");

        Log.w("JobManager-getJobCount ",query.toString());

        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                int jobCount = c.getInt(0);
                close();
                return jobCount;

            } while(c.moveToNext());
        }
        c.close();

        close();

        return 0;

    }

    public void deleteJob(String jobId)
    {
        open();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_JOBS+" WHERE "+DataBaseHelper.FI_JOBS_JOB_ID+"='"+jobId+"'");
        close();

    }

    public void updateJob(Job job)
    {
        open();
        String query = "";
        query = "UPDATE " + DataBaseHelper.TAB_JOBS +
                " SET "+ DataBaseHelper.FI_JOBS_USER_ID + "= " + job.getRefId() +","+
                DataBaseHelper.FI_JOBS_USER_SEQ_ID + "= " + job.getRefSeqId()+","+
                DataBaseHelper.FI_JOBS_CUSTOMER_ID + "= '" + job.getCustomerId() +"',"+
                DataBaseHelper.FI_JOBS_CUSTOMER_NAME + "= '" + job.getCustomerName() +"',"+
                DataBaseHelper.FI_JOBS_CUSTOMER_PHONE + "= '" + job.getPhone() +"',"+
                DataBaseHelper.FI_JOBS_CUSTOMER_ADDRESS + "= '" + job.getAddress() +"',"+
                DataBaseHelper.FI_JOBS_TOTAL_AMOUNT + "= '" + job.getTotalAmount() +"',"+
                DataBaseHelper.FI_JOBS_DOWNPAYMENT + "= '" + job.getDownPayment() +"',"+
                DataBaseHelper.FI_JOBS_INSTALLMENT_TYPE + "= '" + job.getInstallmentType() +"',"+
                DataBaseHelper.FI_JOBS_NO_OF_INSTALLMENTS + "= '" + job.getNoOfInstallments() +"',"+
                DataBaseHelper.FI_JOBS_DATE+ "= '" + job.getDate() +"',"+
                DataBaseHelper.FI_JOBS_FIRST_PAYMENT_DATE+ "= '" + job.getFirstPaymentDate() +"',"+
                DataBaseHelper.FI_JOBS_PROD_ID + "= '" + job.getProdId() +"',"+
                DataBaseHelper.FI_JOBS_AREA_ID + "= '" + job.getAreaId() +"',"+
                DataBaseHelper.FI_JOBS_LATITUDE + "= '" + job.getLatitude() +"',"+
                DataBaseHelper.FI_JOBS_LONGTITUDE + "= '" + job.getLongtitude() +"',"+
                DataBaseHelper.FI_JOBS_CREATED_AT + "= '" + GeneralUtils.getDateTime() +"',"+
                DataBaseHelper.FI_JOBS_STATUS + "= " + job.getStatus()+","+
                DataBaseHelper.FI_JOBS_ORIGIN_ID + "= " + job.getOrginId()+","+
                DataBaseHelper.FI_JOBS_COLLECTOR_ID + "= " + job.getCollectorId()+
                " WHERE "+DataBaseHelper.FI_JOBS_JOB_ID+"='"+job.getJobId()+"'";


        m_database.execSQL(query);
        close();

    }

}
