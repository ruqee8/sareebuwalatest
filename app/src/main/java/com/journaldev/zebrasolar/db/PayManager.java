package com.journaldev.zebrasolar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.utils.GeneralUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PayManager {

    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;
    private Context m_context;

    public PayManager(Context context) {

        m_dbHelper = DataBaseHelper.getInstance(context);
        m_context = context;
    }

    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void close() {
        m_dbHelper.close();
    }


    public void insertPayment(Payment payment) {

        open();

        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_PAY_JOBID,payment.getJobId());
        values.put(DataBaseHelper.FI_PAY_PAYID ,payment.getPaymentId());
        values.put(DataBaseHelper.FI_PAY_REFID ,payment.getRefId());
        values.put(DataBaseHelper.FI_PAY_REFSEQ ,payment.getRefSeqId());
        values.put(DataBaseHelper.FI_PAY_AMOUNT ,payment.getPaymentAmount());
        values.put(DataBaseHelper.FI_PAY_PANALTY ,payment.getPanaltyAmount());
        values.put(DataBaseHelper.FI_PAY_PAYTYPE ,payment.getPayType());
        values.put(DataBaseHelper.FI_PAY_CASH_CHECQUE  ,payment.getPayCashChequeType());
        values.put(DataBaseHelper.FI_PAY_CHEQUE_STATUS ,payment.getChequeStatus());
        values.put(DataBaseHelper.FI_PAY_DATE  ,payment.getDate());
        values.put(DataBaseHelper.FI_PAY_NEXT_DATE  ,payment.getNextDate());
        values.put(DataBaseHelper.FI_PAY_CREATED_AT,GeneralUtils.getDate());
        values.put(DataBaseHelper.FI_PAY_STATUS  ,payment.getStatus());

        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_PAYMENTS, null, values);
        close();

        Log.d("PAYMENT INS:" , "Inserted @" + id + "  " + values.toString());

    }

    public List<Payment> getPaymentList(String jobId)
    {
        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_PAY_JOBID+","
                        +DataBaseHelper.FI_PAY_REFID+","
                        +DataBaseHelper.FI_PAY_REFSEQ+","
                        +DataBaseHelper.FI_PAY_PAYID+","
                        +DataBaseHelper.FI_PAY_AMOUNT+","
                        +DataBaseHelper.FI_PAY_PANALTY+","
                        +DataBaseHelper.FI_PAY_PAYTYPE+","
                        +DataBaseHelper.FI_PAY_CASH_CHECQUE+","
                        +DataBaseHelper.FI_PAY_DATE+","
                        +DataBaseHelper.FI_PAY_CHEQUE_STATUS+","
                        +DataBaseHelper.FI_PAY_STATUS
                        +" FROM "+DataBaseHelper.TAB_PAYMENTS
                        +" WHERE " + DataBaseHelper.FI_PAY_JOBID
                        +"='"+jobId+"'"
                        +" AND ("+DataBaseHelper.FI_PAY_STATUS + "='" +Enums.Status.NEW.getValue()+"' OR "
                        +DataBaseHelper.FI_PAY_STATUS + "='" +Enums.Status.UPDATED.getValue()+"')"
                        +" ORDER BY "+ DataBaseHelper.FI_PAY_PAYID+ " DESC;"
                , null);

        List<Payment> payList  = new ArrayList<Payment>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int userId = c.getInt(1);
                int userSeq= c.getInt(2);
                int payId= c.getInt(3);
                double payAmount= c.getDouble(4);
                double payPanalty= c.getDouble(5);
                int payType= c.getInt(6);
                int cashCheque = c.getInt(7);
                String payDate= c.getString(8);
                int chequeStatus = c.getInt(9);
                Integer status = c.getInt(10);

                //add all the data here
                Payment sample = new Payment();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setPaymentId(payId);
                sample.setPaymentAmount(payAmount);
                sample.setPanaltyAmount(payPanalty);
                sample.setPayType(payType);
                sample.setPayCashChequeType(cashCheque);
                sample.setDateWhenSynced(payDate);
                sample.setChequeStatus(chequeStatus);
                sample.setStatus(status);

                payList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        c.close();

        close();


        if (payList == null){
            return new ArrayList<Payment>();
        }

        return payList;
    }

    public List<Payment> getNotSyncedPaymentList(int LastSyncedRefSeq)
    {
        open();

        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_PAY_JOBID+","
                        +DataBaseHelper.FI_PAY_REFID+","
                        +DataBaseHelper.FI_PAY_REFSEQ+","
                        +DataBaseHelper.FI_PAY_PAYID+","
                        +DataBaseHelper.FI_PAY_AMOUNT+","
                        +DataBaseHelper.FI_PAY_PANALTY+","
                        +DataBaseHelper.FI_PAY_PAYTYPE+","
                        +DataBaseHelper.FI_PAY_CASH_CHECQUE+","
                        +DataBaseHelper.FI_PAY_DATE+","
                        +DataBaseHelper.FI_PAY_NEXT_DATE+","
                        +DataBaseHelper.FI_PAY_CHEQUE_STATUS+","
                        +DataBaseHelper.FI_PAY_STATUS
                        +" FROM "+DataBaseHelper.TAB_PAYMENTS
                        + " where "+DataBaseHelper.FI_PAY_REFSEQ+" > "+ LastSyncedRefSeq
                        + " AND "+DataBaseHelper.FI_PAY_REFID + "='" + Users.getCurrentUserId()+"'"
                , null);

        List<Payment> payList  = new ArrayList<Payment>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int userId = c.getInt(1);
                int userSeq= c.getInt(2);
                int payId= c.getInt(3);
                double payAmount= c.getDouble(4);
                double payPanalty= c.getDouble(5);
                int payType= c.getInt(6);
                int cashCheque = c.getInt(7);
                String payDate= c.getString(8);
                String payNextDate= c.getString(9);
                int chequeStatus = c.getInt(10);
                Integer status = c.getInt(11);

                //add all the data here
                Payment sample = new Payment();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setPaymentId(payId);
                sample.setPaymentAmount(payAmount);
                sample.setPanaltyAmount(payPanalty);
                sample.setPayType(payType);
                sample.setPayCashChequeType(cashCheque);
                sample.setDateWhenSynced(payDate);
                sample.setNextDate(payNextDate);
                sample.setChequeStatus(chequeStatus);
                sample.setStatus(status);

                payList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        close();


        if (payList == null){
            return new ArrayList<Payment>();
        }


        return payList;
    }





    public int getNextPaymentIdforThisJob(String jobId)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_PAY_PAYID + ")");
        query.append(" FROM " + DataBaseHelper.TAB_PAYMENTS);
        query.append(" WHERE "+DataBaseHelper.FI_PAY_JOBID+"='"+ jobId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                Integer payId = c.getInt(0);
                payId = payId + 1;
                return payId;

            } while(c.moveToNext());
        }
        c.close();

        close();

        return 0;

    }

    public int getNextPaymentRefSeq(String refId)
    {
        open();


        int payTabSync1 = 0;

        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+DataBaseHelper.FI_PAY_REFSEQ+ ")");
        query.append(" FROM " + DataBaseHelper.TAB_PAYMENTS);
        query.append(" WHERE "+DataBaseHelper.FI_PAY_REFID+"='"+ refId +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                payTabSync1 = c.getInt(0);

            } while(c.moveToNext());
        }
        c.close();

        //get from sync Table
        int payTabSync2 = 0;

        StringBuilder query2 = new StringBuilder();
        query2.append("SELECT ");
        query2.append(DataBaseHelper.FI_SYNC_LAST_REF);
        query2.append(" FROM " + DataBaseHelper.TAB_SYNC_TABLE);
        query2.append(" WHERE "+DataBaseHelper.FI_SYNC_USER_ID+"='"+ refId +"'");
        query2.append(" AND "+DataBaseHelper.FI_SYNC_TABLE_ID+"="+ Enums.TableType.PAYMENT.getValue());
        Cursor c2 = m_database.rawQuery(query2.toString(), null);
        if (c2.moveToFirst()){
            do {
                payTabSync2 = c2.getInt(0);

            } while(c2.moveToNext());
        }
        c2.close();


        int nextRefSeq = 0;

        if (payTabSync2 > payTabSync1)
        {
            nextRefSeq = payTabSync2+1;
        }
        else
        {
            nextRefSeq = payTabSync1+1;
        }

        return nextRefSeq;


    }

    public int getLastPaySeqForUser(String user)
    {
        open();


        int payTabSync1 = 0;

        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+DataBaseHelper.FI_PAY_REFSEQ+ ")");
        query.append(" FROM " + DataBaseHelper.TAB_PAYMENTS);
        query.append(" WHERE "+DataBaseHelper.FI_PAY_REFID+"='"+ user +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                payTabSync1 = c.getInt(0);
                return payTabSync1;

            } while(c.moveToNext());
        }
        c.close();

        close();

        return 0;

    }

    public void deletePayment(Payment pay)
    {

        open();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_PAYMENTS+
                " where "+DataBaseHelper.FI_PAY_JOBID+" ='"+ pay.getJobId()+"'"
                        + " AND "+DataBaseHelper.FI_PAY_PAYID + "='" + pay.getPaymentId()+"'");
        close();
    }

    public void deletePaymentsForJob(Job job)
    {

        open();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_PAYMENTS+
                " where "+DataBaseHelper.FI_PAY_JOBID+" ='"+ job.getJobId()+"'");
        close();
    }

    public  void updatePayment(Payment pay)
    {
        open();
        String query = "";
        query = "UPDATE " + DataBaseHelper.TAB_PAYMENTS +
                " SET "+ DataBaseHelper.FI_PAY_JOBID + "= '" + pay.getJobId() +"',"+
                 DataBaseHelper.FI_PAY_REFID+ "= '" + pay.getRefId()+"',"+
                 DataBaseHelper.FI_PAY_REFSEQ + "= '" + pay.getRefSeqId() +"',"+
                 DataBaseHelper.FI_PAY_PAYID + "= '" + pay.getPaymentId() +"',"+
                 DataBaseHelper.FI_PAY_AMOUNT + "= '" + pay.getPaymentAmount() +"',"+
                 DataBaseHelper.FI_PAY_PANALTY + "= '" + pay.getPanaltyAmount() +"',"+
                 DataBaseHelper.FI_PAY_PAYTYPE + "= '" + pay.getPayType() +"',"+
                 DataBaseHelper.FI_PAY_CASH_CHECQUE + "= '" + pay.getPayCashChequeType() +"',"+
                 DataBaseHelper.FI_PAY_DATE + "= '" + pay.getDate() +"',"+
                 DataBaseHelper.FI_PAY_NEXT_DATE + "= '" + pay.getNextDate() +"',"+
                 DataBaseHelper.FI_PAY_CREATED_AT + "= '" + GeneralUtils.getDate() +"',"+
                 DataBaseHelper.FI_PAY_CHEQUE_STATUS + "= '" + pay.getChequeStatus() +"',"+
                 DataBaseHelper.FI_PAY_STATUS + "= " + pay.getStatus() +
                " where "+DataBaseHelper.FI_PAY_JOBID+"='"+ pay.getJobId()+"'"
                + " AND "+DataBaseHelper.FI_PAY_PAYID + "='" + pay.getPaymentId()+"'";



        m_database.execSQL(query);
        close();


    }


    public int getTodayPayments()
    {

        open();
        int count = -1;
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        Cursor c = m_database.rawQuery("select COUNT(" + DataBaseHelper.FI_PAY_JOBID +") as paycount from "
                + DataBaseHelper.TAB_PAYMENTS + " where date("+ DataBaseHelper.FI_PAY_DATE+")" +
                "= '"+currentDate +"' ;" ,null);
        if (c.moveToFirst()){
            do {
                count = c.getInt(0) ;
            } while(c.moveToNext());
        }
        c.close();

        close();

        return count;

    }

    public List<Payment> getPaymentListForCollectorAndDate(int userID,String date)
    {
        open();


        Cursor c = m_database.rawQuery("SELECT "+
                        DataBaseHelper.FI_PAY_JOBID+","
                        +DataBaseHelper.FI_PAY_REFID+","
                        +DataBaseHelper.FI_PAY_REFSEQ+","
                        +DataBaseHelper.FI_PAY_PAYID+","
                        +DataBaseHelper.FI_PAY_AMOUNT+","
                        +DataBaseHelper.FI_PAY_PANALTY+","
                        +DataBaseHelper.FI_PAY_PAYTYPE+","
                        +DataBaseHelper.FI_PAY_CASH_CHECQUE+","
                        +DataBaseHelper.FI_PAY_DATE+","
                        +DataBaseHelper.FI_PAY_NEXT_DATE+","
                        +DataBaseHelper.FI_PAY_CHEQUE_STATUS+","
                        +DataBaseHelper.FI_PAY_STATUS
                        +" FROM "+DataBaseHelper.TAB_PAYMENTS
                        +" WHERE " + DataBaseHelper.FI_PAY_REFID
                        +"='"+userID+"'"
                        +" AND date("+ DataBaseHelper.FI_PAY_DATE+")" +
                        "= '"+date +"' ;"
                , null);

        List<Payment> payList  = new ArrayList<Payment>();


        if (c.moveToFirst()){
            do {
                // Passing values
                String jobid = c.getString(0);
                int userId = c.getInt(1);
                int userSeq= c.getInt(2);
                int payId= c.getInt(3);
                double payAmount= c.getDouble(4);
                double payPanalty= c.getDouble(5);
                int payType= c.getInt(6);
                int cashCheque = c.getInt(7);
                String payDate= c.getString(8);
                String payNextDate= c.getString(9);
                int chequeStatus = c.getInt(10);
                Integer status = c.getInt(11);

                //add all the data here
                Payment sample = new Payment();
                sample.setJobId(jobid);
                sample.setRefId(userId);
                sample.setRefSeqId(userSeq);
                sample.setPaymentId(payId);
                sample.setPaymentAmount(payAmount);
                sample.setPanaltyAmount(payPanalty);
                sample.setPayType(payType);
                sample.setPayCashChequeType(cashCheque);
                sample.setDateWhenSynced(payDate);
                sample.setNextDate(payNextDate);
                sample.setChequeStatus(chequeStatus);
                sample.setStatus(status);

                payList.add(sample);

                // Do something Here with values
            } while(c.moveToNext());
        }

        c.close();

        close();


        if (payList == null){
            return new ArrayList<Payment>();
        }

        return payList;
    }

    public String getMaxNextDate(String jobId)
    {
        open();
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX("+ DataBaseHelper.FI_PAY_NEXT_DATE + ")");
        query.append(" FROM " + DataBaseHelper.TAB_PAYMENTS);
        query.append(" WHERE "+DataBaseHelper.FI_PAY_JOBID+"='"+ jobId +"'");
        query.append(" AND "+DataBaseHelper.FI_PAY_STATUS+"='"+ Enums.Status.NEXTDATE.getValue() +"'");
        Cursor c = m_database.rawQuery(query.toString(), null);
        if (c.moveToFirst()){
            do {
                String nextDate = c.getString(0);

                if(nextDate == null)
                {
                    return "";
                }
                return nextDate;

            } while(c.moveToNext());
        }
        c.close();

        close();

        return "";
    }

}
