package com.journaldev.zebrasolar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.pojo.area.Area;
import com.journaldev.zebrasolar.pojo.product.Product;
import com.journaldev.zebrasolar.pojo.user.User;

import java.util.Map;

import static com.journaldev.zebrasolar.db.DataBaseHelper.TAB_SYNC_TABLE;

public class ReferenceDataManager {

    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;
    private Context m_context;

    public ReferenceDataManager(Context context) {

        m_dbHelper = DataBaseHelper.getInstance(context);
        m_context = context;
    }

    public void beginDynamicUpdate() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void endDynamicUpdate() {
        /*m_dbHelper.close();*/
    }

    ///////////////////////////////////ZEBRA////////////////////////////////////////////////////////
    public void clearUsersTable()
    {
        beginDynamicUpdate();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_USERS);
        endDynamicUpdate();
    }


    public void clearProductsTable()
    {
        beginDynamicUpdate();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_PRODUCTS);
        endDynamicUpdate();
    }

    public void clearAreaTable()
    {
        beginDynamicUpdate();
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_AREAS);
        endDynamicUpdate();
    }

    public void insertUser(User user)
    {
        beginDynamicUpdate();
        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_USERS_USER_ID, user.getId());
        values.put(DataBaseHelper.FI_USERS_USER_NAME, user.getUserName());
        values.put(DataBaseHelper.FI_USERS_USER_TYPE, user.getType());
        values.put(DataBaseHelper.FI_USERS_USER_FIRST_NAME, user.getFirstName());
        values.put(DataBaseHelper.FI_USERS_USER_LAST_NAME, user.getLastName());

        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_USERS, null, values);
        Log.d("USERS INS:" , "Inserted @" + id + "  " + values.toString());
        endDynamicUpdate();
    }

    public void insertProduct(Product product)
    {
        beginDynamicUpdate();
        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_PRODUCTS_ID, product.getId());
        values.put(DataBaseHelper.FI_PRODUCTS_NAME, product.getName());

        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_PRODUCTS, null, values);
        Log.d("PROD INS:" , "Inserted @" + id + "  " + values.toString());
        endDynamicUpdate();
    }

    public void insertArea(Area area)
    {
        beginDynamicUpdate();
        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_AREA_ID, area.getId());
        values.put(DataBaseHelper.FI_AREA_NAME, area.getName());

        // insert row
        long id = m_database.insert(DataBaseHelper.TAB_AREAS, null, values);
        Log.d("AREA INS:" , "Inserted @" + id + "  " + values.toString());
        endDynamicUpdate();
    }

    public void clearSyncTable()
    {
        beginDynamicUpdate();
        // insert row
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_SYNC_TABLE);
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_JOBS);
        m_database.execSQL("DELETE FROM " + DataBaseHelper.TAB_PAYMENTS);
        Log.d("Sync Table:" , "cleared" );
        endDynamicUpdate();
    }



    public void loadProducts(Map<String,Integer> mapProducts,Map<Integer,String> mapProductsById)
    {
        beginDynamicUpdate();

        Cursor c = m_database.rawQuery("SELECT "+ DataBaseHelper.FI_PRODUCTS_NAME+","+
                DataBaseHelper.FI_PRODUCTS_ID+" FROM "+DataBaseHelper.TAB_PRODUCTS, null);
        if (c.moveToFirst()){
            do {
                // Passing values
                String prod_name = c.getString(0);
                Integer prod_id = c.getInt(1);
                mapProducts.put(prod_name,prod_id);
                mapProductsById.put(prod_id,prod_name);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        endDynamicUpdate();


    }

    public void loadAreas(Map<String,Integer> mapAreas,Map<Integer,String> mapAreasById)
    {
        beginDynamicUpdate();

        Cursor c = m_database.rawQuery("SELECT "+ DataBaseHelper.FI_AREA_NAME+","+
                DataBaseHelper.FI_AREA_ID+" FROM "+DataBaseHelper.TAB_AREAS, null);
        if (c.moveToFirst()){
            do {
                // Passing values
                String area_name = c.getString(0);
                Integer area_id = c.getInt(1);
                mapAreas.put(area_name,area_id);
                mapAreasById.put(area_id,area_name);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        endDynamicUpdate();


    }

    public void loadUsers(Map<String,Integer> mapUsers)
    {
        beginDynamicUpdate();

        Cursor c = m_database.rawQuery("SELECT "+ DataBaseHelper.FI_USERS_USER_NAME+","+
                DataBaseHelper.FI_USERS_USER_ID+" FROM "+DataBaseHelper.TAB_USERS, null);
        if (c.moveToFirst()){
            do {
                // Passing values
                String user_name = c.getString(0);
                Integer user_id = c.getInt(1);
                mapUsers.put(user_name,user_id);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        endDynamicUpdate();


    }

    public void loadCollectorUsers(Map<Integer,String> mapUsers)
    {
        beginDynamicUpdate();

        Cursor c = m_database.rawQuery("SELECT "+ DataBaseHelper.FI_USERS_USER_FIRST_NAME+","+
                        DataBaseHelper.FI_USERS_USER_LAST_NAME+","+
                DataBaseHelper.FI_USERS_USER_ID+" FROM "+DataBaseHelper.TAB_USERS+
                " WHERE "+ DataBaseHelper.FI_USERS_USER_TYPE+ "=" + Enums.USERTYPES.COLLECTOR.getValue()
                , null);
        if (c.moveToFirst()){
            do {
                // Passing values
                String user_name = c.getString(0)+ " " +c.getString(1) ;
                Integer user_id = c.getInt(2);
                mapUsers.put(user_id,user_name);

                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        endDynamicUpdate();


    }

    public boolean checkSyncValueExist(String userId, Enums.TableType tableType)
    {
        beginDynamicUpdate();

        Cursor c = m_database.rawQuery("SELECT * FROM "+ TAB_SYNC_TABLE
                        +" WHERE "+DataBaseHelper.FI_SYNC_USER_ID+"='"+userId+"' AND "
                        +DataBaseHelper.FI_SYNC_TABLE_ID+"="+tableType.getValue(), null);
        if (c.moveToFirst()){
            do {
                return true;
                // Do something Here with values
            } while(c.moveToNext());
        }
        c.close();

        endDynamicUpdate();
        return false;

    }

    public void setInitialSyncData(String userId, Enums.TableType tableType)
    {
        beginDynamicUpdate();
        ContentValues values = new ContentValues();

        values.put(DataBaseHelper.FI_SYNC_USER_ID, userId);
        values.put(DataBaseHelper.FI_SYNC_TABLE_ID, tableType.getValue());
        values.put(DataBaseHelper.FI_SYNC_LAST_REF, 0);

        // insert row
        long id = m_database.insert(TAB_SYNC_TABLE, null, values);
        Log.d("SYNC TABLES INS:" , "Inserted @" + id + "  " + values.toString());

        endDynamicUpdate();

    }





    //////////////////////////////////ZEBRA/////////////////////////////////////////////////////////





}