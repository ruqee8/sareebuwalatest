package com.journaldev.zebrasolar.db;

/**
 * Created by rukshanr on 10/3/2018.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Pair;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.pojo.job.JobOrPaymentSyncRequest;

import java.util.ArrayList;
import java.util.List;

public class SyncOperations {

    private Context m_context;
    private DataBaseHelper m_dbHelper;

    private SQLiteDatabase m_database;


    public SyncOperations(Context context) {
        m_context = context;
        m_dbHelper = DataBaseHelper.getInstance(context);
    }

    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();
    }

    public void close() {
        m_dbHelper.close();
    }






    public int getLastSeq(Enums.TableType type, String user)
    {
        open();
        int lastref = 0;

        String query = "SELECT "+DataBaseHelper.FI_SYNC_LAST_REF +" FROM "+DataBaseHelper.TAB_SYNC_TABLE + " WHERE "
                        +DataBaseHelper.FI_SYNC_TABLE_ID +"='"+type.getValue()+"' AND "
                        +DataBaseHelper.FI_SYNC_USER_ID +"='"+user+"'";

        Cursor c = m_database.rawQuery(query, null);
        if (c.moveToFirst()){
            do {
                lastref = c.getInt(0);
            } while(c.moveToNext());
        }
        c.close();
        close();

        return lastref;
    }

    public String getSynctableData()
    {

        open();
        StringBuilder data = new StringBuilder();

        String query = "SELECT * FROM "+DataBaseHelper.TAB_SYNC_TABLE ;
        Cursor c = m_database.rawQuery(query, null);
        if (c.moveToFirst()){
            do {
                int tableType = c.getInt(1);
                String table = "";
                if(tableType == 1)
                {
                    table = "Job";
                }
                else if(tableType == 2) {
                    table = "Payment";
                }

                data.append("Table Type : " + table);

                String user = c.getString(2);
                data.append(" userId : " + user);

                int refSeq = c.getInt(3);
                data.append(" refSeq : " + refSeq);
                data.append("\n");


            } while(c.moveToNext());
        }
        c.close();

        close();

        return data.toString();

    }


    public void checkpointLastSyncedTxnID(int txnID, int userId, Enums.TableType tableType) {
        open();

        String query = "";
        query = "UPDATE " + DataBaseHelper.TAB_SYNC_TABLE + " SET " +DataBaseHelper.FI_SYNC_LAST_REF +" = " + txnID +
                " WHERE " + DataBaseHelper.FI_SYNC_TABLE_ID + "=" + tableType.getValue() +
                " AND "+ DataBaseHelper.FI_SYNC_USER_ID + "='"+ userId +"'"+
                " AND "+ DataBaseHelper.FI_SYNC_LAST_REF +" < " + txnID;
        m_database.execSQL(query);
        Log.w("Checkpoint Job/Payment", "checkpointLastSyncedTxnID: "+query);

        close();

    }

    public List<JobOrPaymentSyncRequest> getUsersAndRefSequences( Enums.TableType tableType)
    {
        open();

        String query = "SELECT "+ DataBaseHelper.FI_SYNC_USER_ID +","+DataBaseHelper.FI_SYNC_LAST_REF
                +" FROM "+DataBaseHelper.TAB_SYNC_TABLE + " WHERE "
                +DataBaseHelper.FI_SYNC_TABLE_ID +"='"+tableType.getValue()+"'";
                /*AND "
                +DataBaseHelper.FI_SYNC_USER_ID +"!='"+currentUserId+"'";*/

        List<JobOrPaymentSyncRequest> otherUserList  = new ArrayList<JobOrPaymentSyncRequest>();

        Cursor c = m_database.rawQuery(query, null);
        if (c.moveToFirst()){
            do {
                String userId  = c.getString(0);
                String lastRef = Integer.toString(c.getInt(1));

                JobOrPaymentSyncRequest userRef  = new JobOrPaymentSyncRequest();
                userRef.setUserId(userId);
                userRef.setLastRefSeq(lastRef);

                otherUserList.add(userRef);

            } while(c.moveToNext());
        }
        c.close();

        close();

        return otherUserList;

    }


}