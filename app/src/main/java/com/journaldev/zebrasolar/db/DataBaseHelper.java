package com.journaldev.zebrasolar.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rukshanr on 10/3/2018.
 */

import android.content.Context;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

    /////////////////////////////////////////////////////////
    private static DataBaseHelper sInstance;

    //////////////////////////////////////////////////////////

    //////////////////ZEBRA////////////////////////////////////

    public static final String ID= "id";

    public static final String TAB_USERS = "users";
    public static final String FI_USERS_USER_NAME = "user_name";
    public static final String FI_USERS_USER_ID = "user_id";
    public static final String FI_USERS_USER_FIRST_NAME = "first_name";
    public static final String FI_USERS_USER_LAST_NAME = "last_name";
    public static final String FI_USERS_USER_TYPE = "user_type";

    public static final String TAB_PRODUCTS = "products";
    public static final String FI_PRODUCTS_NAME = "prod_name";
    public static final String FI_PRODUCTS_ID = "prod_id";

    public static final String TAB_AREAS = "areas";
    public static final String FI_AREA_NAME = "area_name";
    public static final String FI_AREA_ID = "area_id";

    public static final String TAB_JOBS = "jobs";
    public static final String FI_JOBS_ORIGIN_ID = "origin_id";
    public static final String FI_JOBS_USER_ID = "user_id";
    public static final String FI_JOBS_USER_SEQ_ID = "user_seq_id";
    public static final String FI_JOBS_JOB_ID = "job_id";
    public static final String FI_JOBS_COLLECTOR_ID = "collector_id";
    public static final String FI_JOBS_CUSTOMER_NAME = "customer_name";
    public static final String FI_JOBS_CUSTOMER_ID = "customer_id";
    public static final String FI_JOBS_CUSTOMER_PHONE = "customer_phone";
    public static final String FI_JOBS_CUSTOMER_ADDRESS = "customer_address";
    public static final String FI_JOBS_INSTALLMENT_TYPE = "installment_type";
    public static final String FI_JOBS_TOTAL_AMOUNT = "total_amount";
    public static final String FI_JOBS_NO_OF_INSTALLMENTS = "no_of_installments";
    public static final String FI_JOBS_PROD_ID = "prod_id";
    public static final String FI_JOBS_AREA_ID = "area_id";
    public static final String FI_JOBS_LATITUDE = "latitude";
    public static final String FI_JOBS_LONGTITUDE = "longtitude";
    public static final String FI_JOBS_DOWNPAYMENT = "down_payment";
    public static final String FI_JOBS_DATE = "job_date";
    public static final String FI_JOBS_FIRST_PAYMENT_DATE = "first_payment_date";
    public static final String FI_JOBS_CREATED_AT = "created_at";
    public static final String FI_JOBS_STATUS = "status";



    public static final String TAB_PAYMENTS = "payments";
    public static final String FI_PAY_JOBID = "jobid";
    public static final String FI_PAY_PAYID = "payid";
    public static final String FI_PAY_REFID = "refid";
    public static final String FI_PAY_REFSEQ = "refseq";
    public static final String FI_PAY_AMOUNT = "payamount";
    public static final String FI_PAY_PANALTY = "panalty";
    public static final String FI_PAY_PAYTYPE = "paytype";
    public static final String FI_PAY_CASH_CHECQUE = "paycashcheck";
    public static final String FI_PAY_DATE = "payDate";
    public static final String FI_PAY_NEXT_DATE = "payNextDate";
    public static final String FI_PAY_CREATED_AT = "created_at";
    public static final String FI_PAY_CHEQUE_STATUS = "cheque_status";
    public static final String FI_PAY_STATUS = "status";


    public static final String TAB_INVOICES = "invoices";
    public static final String FI_INV_JOBID = "jobid";
    public static final String FI_INV_INVID = "invid";
    public static final String FI_INV_REFID = "refid";
    public static final String FI_INV_REFSEQ = "refseq";
    public static final String FI_INV_DESCRIPTION = "description";
    public static final String FI_INV_TOTAL = "total";
    public static final String FI_INV_CREATED_AT = "created_at";


    public static final String TAB_SYNC_TABLE = "syncTable";
    public static final String FI_SYNC_TABLE_ID = "table_id";
    public static final String FI_SYNC_USER_ID = "user_id";
    public static final String FI_SYNC_LAST_REF = "last_ref";


    public static final String TAB_FORWARD_PAYMENT = "forwardPayment";
    public static final String FI_DUEPAYMENT_DATE = "duePaymentDate";
    public static final String FI_AMOUNT = "amount";
    public static final String FI_PAYMENT_ID = "paymentId";

    public static final String TAB_JOB_FORWARD_BALANCE = "forwardBalance";
    public static final String FI_FORWARD_JOBID= "jobId";
    public static final String FI_FORWARD_BALANCE = "forwardBalance";


    private Context m_context;


    private static final String DATABASE_NAME = "TempData.db";
    private static final int DATABASE_VERSION = 1;


    //////////////////////ZEBRA/////////////////////////////////////////////////////////////////////

    //User Table
    private static final String TABLE_USER_CREATE = "create table if not exists " + TAB_USERS
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_USERS_USER_ID + " INTEGER UNIQUE, "
            + FI_USERS_USER_NAME +" text,"
            + FI_USERS_USER_FIRST_NAME +" text,"
            + FI_USERS_USER_LAST_NAME +" text,"
            + FI_USERS_USER_TYPE +" INTEGER );";

    //Products Table
    private static final String TABLE_PRODUCTS_CREATE = "create table if not exists " + TAB_PRODUCTS
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_PRODUCTS_ID + " INTEGER, "
            + FI_PRODUCTS_NAME +" text );";

    //Products Table
    private static final String TABLE_AREAS_CREATE = "create table if not exists " + TAB_AREAS
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_AREA_ID + " INTEGER, "
            + FI_AREA_NAME +" text );";


    //Jobs Table
    //Jobs Table
    private static final String TABLE_JOBS_CREATE = "create table if not exists " + TAB_JOBS
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_JOBS_ORIGIN_ID + " INTEGER, "
            + FI_JOBS_USER_ID + " INTEGER, "
            + FI_JOBS_USER_SEQ_ID + " INTEGER, "
            + FI_JOBS_JOB_ID + " text, "
            + FI_JOBS_COLLECTOR_ID + " INTEGER, "
            + FI_JOBS_CUSTOMER_NAME + " text, "
            + FI_JOBS_CUSTOMER_ID + " text, "
            + FI_JOBS_CUSTOMER_PHONE + " text, "
            + FI_JOBS_CUSTOMER_ADDRESS+ " text, "
            + FI_JOBS_INSTALLMENT_TYPE + " INTEGER, "
            + FI_JOBS_TOTAL_AMOUNT + " DECIMAL(10,2), "
            + FI_JOBS_NO_OF_INSTALLMENTS + " INTEGER, "
            + FI_JOBS_DOWNPAYMENT + " DECIMAL(10,2), "
            + FI_JOBS_PROD_ID + " INTEGER, "
            + FI_JOBS_AREA_ID + " INTEGER, "
            + FI_JOBS_LATITUDE+ " Decimal(9,6), "
            + FI_JOBS_LONGTITUDE+ " Decimal(9,6), "
            + FI_JOBS_DATE + " text, "
            + FI_JOBS_FIRST_PAYMENT_DATE+ " text, "
            + FI_JOBS_CREATED_AT + " text, "
            + FI_JOBS_STATUS + " INTEGER );";

    private static final String TABLE_PAYMENT_CREATE = "create table if not exists " + TAB_PAYMENTS
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_PAY_JOBID+ " text, "
            + FI_PAY_PAYID + " INTEGER, "
            + FI_PAY_REFID + " INTEGER, "
            + FI_PAY_REFSEQ+ " INTEGER, "
            + FI_PAY_AMOUNT + " DECIMAL(10,2), "
            + FI_PAY_PANALTY + " DECIMAL(10,2), "
            + FI_PAY_PAYTYPE + " text, "
            + FI_PAY_CASH_CHECQUE + " text, "
            + FI_PAY_CHEQUE_STATUS + " text, "
            + FI_PAY_DATE + " text, "
            + FI_PAY_NEXT_DATE + " text, "
            + FI_PAY_CREATED_AT +  " text, "
            + FI_PAY_STATUS + " INTEGER );";

    private static final String TABLE_INVOICE_CREATE = "create table if not exists " + TAB_INVOICES
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_INV_JOBID+ " text, "
            + FI_INV_INVID + " text, "
            + FI_INV_REFID + " text, "
            + FI_INV_REFSEQ+ " text, "
            + FI_INV_DESCRIPTION + " text, "
            + FI_INV_TOTAL + " text, "
            + FI_PAY_CREATED_AT + " text );";


    // Sync Table
    private static final String SYNC_TABLE_CREATE = "create table if not exists " + TAB_SYNC_TABLE
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_SYNC_TABLE_ID + " INTEGER, "
            + FI_SYNC_USER_ID + " text, "
            + FI_SYNC_LAST_REF +" INTEGER );";

    //payments periods table
    private static final String TAB_FORWARD_PAYMENT_CREATE = "create table if not exists " + TAB_FORWARD_PAYMENT
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_FORWARD_JOBID + " text, "
            + FI_DUEPAYMENT_DATE + " text, "
            + FI_AMOUNT + " text, "
            + FI_PAYMENT_ID +" text );";


    //forwardBalance
    private static final String TAB_JOB_FORWARD_BALANCE_CREATE = "create table if not exists " + TAB_JOB_FORWARD_BALANCE
            +  "(" +ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FI_FORWARD_JOBID + " text, "
            + FI_FORWARD_BALANCE +" text );";


    ///////////////////////ZEBRA////////////////////////////////////////////////////////////////////

    public static synchronized DataBaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DataBaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }
    

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        m_context = context;
    }

    private static  final String TABLE_CLEAN_USERS = "DROP TABLE IF EXISTS " + TAB_USERS;
    private static  final String TABLE_CLEAN_PRODUCTS = "DROP TABLE IF EXISTS " + TAB_PRODUCTS;
    private static  final String TABLE_CLEAN_AREAS = "DROP TABLE IF EXISTS " + TAB_AREAS;
    private static  final String TABLE_CLEAN_JOBS = "DROP TABLE IF EXISTS " + TAB_JOBS;
    private static  final String TABLE_CLEAN_PAYMENTS = "DROP TABLE IF EXISTS " + TAB_PAYMENTS;
    private static  final String TABLE_CLEAN_INVOICES = "DROP TABLE IF EXISTS " + TAB_INVOICES;
    private static  final String TABLE_CLEAN_FORWARD_PAYMENTS = "DROP TABLE IF EXISTS " + TAB_FORWARD_PAYMENT;
    private static  final String TABLE_CLEAN_FORWARD_BALANCE = "DROP TABLE IF EXISTS " + TAB_JOB_FORWARD_BALANCE;
    private static  final String TABLE_CLEAN_SYNC = "DROP TABLE IF EXISTS " + TAB_SYNC_TABLE;


    public static void clearAllTables(SQLiteDatabase db)
    {
        db.execSQL(TABLE_CLEAN_USERS);
        db.execSQL(TABLE_CLEAN_PRODUCTS);
        db.execSQL(TABLE_CLEAN_AREAS);
        db.execSQL(TABLE_CLEAN_JOBS);
        db.execSQL(TABLE_CLEAN_PAYMENTS);
        db.execSQL(TABLE_CLEAN_INVOICES);
        db.execSQL(TABLE_CLEAN_FORWARD_PAYMENTS);
        db.execSQL(TABLE_CLEAN_FORWARD_BALANCE);
        db.execSQL(TABLE_CLEAN_SYNC);

        Log.w("DataBase Helper", "DBHelper clear All Tables;" );
    }

    public static void createAllTables(SQLiteDatabase db)
    {
        db.execSQL(TABLE_USER_CREATE);
        db.execSQL(TABLE_PRODUCTS_CREATE);
        db.execSQL(TABLE_AREAS_CREATE);
        db.execSQL(TABLE_JOBS_CREATE);
        db.execSQL(TABLE_PAYMENT_CREATE);
        db.execSQL(TABLE_INVOICE_CREATE);
        db.execSQL(TAB_FORWARD_PAYMENT_CREATE);
        db.execSQL(TAB_JOB_FORWARD_BALANCE_CREATE);
        db.execSQL(SYNC_TABLE_CREATE);

        Log.w("DataBase Helper", "DBHelper create All Tables;" );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        clearAllTables(db);
      createAllTables(db);

      Log.w("DataBase Helper", "onCreate: DataBasehelper excuted success" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // you should do some logging in here
        // ..

        clearAllTables(db);
        onCreate(db);
    }

}
