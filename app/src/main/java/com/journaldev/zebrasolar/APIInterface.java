package com.journaldev.zebrasolar;

import com.journaldev.zebrasolar.pojo.Logout;
import com.journaldev.zebrasolar.pojo.SignIn;
import com.journaldev.zebrasolar.pojo.area.Area;
import com.journaldev.zebrasolar.pojo.email.EmailResponse;
import com.journaldev.zebrasolar.pojo.email.EmailSend;
import com.journaldev.zebrasolar.pojo.product.Product;
import com.journaldev.zebrasolar.pojo.user.User;
import com.journaldev.zebrasolar.pojo.ft.FTInfo;
import com.journaldev.zebrasolar.pojo.job.JobCreateRequest;
import com.journaldev.zebrasolar.pojo.job.JobOrPaymentSyncRequest;
import com.journaldev.zebrasolar.pojo.job.JobSyncResponse;
import com.journaldev.zebrasolar.pojo.payment.PaymentCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;
import com.journaldev.zebrasolar.pojo.product.ProductItem;
import com.journaldev.zebrasolar.pojo.sales.Customer;
import com.journaldev.zebrasolar.pojo.sales.Route;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by anupamchugh on 09/01/17.
 */

public interface APIInterface {



    @POST("/api/users")
    Call<User> createUser(@Body User user);

    //@FormUrlEncoded
    //@POST("API/Login_API_Controller/loginuser2")
    //Call<SignIn> loginUser( @Header("X-API-KEY") String token,@Field("User_Name") String name, @Field("Password") String password);

    @FormUrlEncoded
    @POST("API/Login_API/login_post")
    Call<SignIn> loginUser(@Field("User_Name") String name, @Field("Password") String password);

    @FormUrlEncoded
    //@POST("API/Login_API/login_post")
    @POST("API/Login_API_Controller/LoginUser2_post")
    Call<SignIn> loginUserWithoutKey(@Field("User_Name") String name, @Field("Password") String password);

    @POST("API/Job/create_job")
    Call<Object> insertJob(@Body JobCreateRequest job);

    @POST("API/Email/send_email")
    Call<EmailResponse> sendEmail(@Body EmailSend email);

    @POST("API/Payment/create_payment")
    Call<Object> insertPayment(@Body PaymentCreateRequest payment);

    @POST("API/Job/get_not_synced_jobs")
    Call<List<JobSyncResponse>> getNotSyncedJobs(@Body List<JobOrPaymentSyncRequest> notSyncedJobs);

    @POST("API/Payment/get_payments")
    Call<List<PaymentSyncResponse>> getNotSyncedPayments(@Body List<JobOrPaymentSyncRequest> notSyncedPayments);

    @GET("API/Login_API_Controller/logoutUser")
    Call<Logout> doLogOut();



    ////////////////////////////////////////////////////////////////////////////////////////////////
    // RefData
    @FormUrlEncoded
    @POST("API/RefData/get_ft_info")
    Call<FTInfo> getFTInfo(@Field("source_id") Integer sourceID);


    @GET("API/RefData/get_prod_items")
    Call<List<ProductItem>> getProductItems();

    @GET("API/RefData/get_routes")
    Call<List<Route>> getRoutes();

    @GET("API/RefData/get_customers")
    Call<List<Customer>> getCustomers();

    @GET("API/RefData/get_users")
    Call<List<User>> getUsers();

    @GET("API/RefData/get_products")
    Call<List<Product>> getProducts();

    @GET("API/RefData/get_areas")
    Call<List<Area>> getAreas();
    ////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Customers
    @POST("API/RefData/create_customer")
    Call<Object> createCustomer(@Body Customer customer);


}
