package com.journaldev.zebrasolar.backend.common;

public class Enums {

    public static enum TableType {
        JOB(1),
        PAYMENT(2);

        private final int value;

        TableType( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static enum Status {
        NEW(1),
        UPDATED(2),
        DELETED(3),
        COMPLETED(4),
        NEXTDATE(5);

        private final int value;

        Status( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static enum NUMPADSOURCE {
        PAYMENTS(1);

        private final int value;

        NUMPADSOURCE( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static enum USERTYPES {
        ADMIN(2),
        SALESREP(3),
        COLLECTOR(4),
        OFFICER(5);


        private final int value;

        USERTYPES( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static enum PAYTYPES {
        CASH(1),
        CHEQUE(2);


        private final int value;

        PAYTYPES( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static enum INSTALLMENTTYPE {
        DAILY(1),
        WEEKLY(2),
        MONTHLY(3);


        private final int value;

        INSTALLMENTTYPE( int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }
}
