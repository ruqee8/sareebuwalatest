package com.journaldev.zebrasolar.backend.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.pojo.SignIn;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Commons {

    private static APIInterface m_apiInterface = null;
    private static APIInterface m_apiInterfaceWithoutKey = null;

    private static  String m_userName = null;
    private static  String m_password = null;

    private static void init(Context context)
    {
        if (m_apiInterface == null)
            m_apiInterface = APIClient.getClient().create(APIInterface.class);
        if (m_apiInterfaceWithoutKey == null)
            m_apiInterfaceWithoutKey = APIClient.getClientWithoutKey().create(APIInterface.class);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        m_userName = sharedPref.getString("userName","none");
        m_password = sharedPref.getString("password","none");
    }

    public static void login(Context context,final LoginListener listener)
    {
        init(context);
        Call<SignIn> call = m_apiInterfaceWithoutKey.loginUserWithoutKey(m_userName, m_password);
        call.request().url();
        Log.d("Test",call.toString());
        call.enqueue(new Callback<SignIn>() {
            @Override
            public void onResponse(Call<SignIn> call, Response<SignIn> response) {

                SignIn signIn = response.body();
                boolean status = signIn.getStatus();

                if(status == false)
                {
                    Log.w("Common Login","Invalid Credentials");
                    return;
                }

                //set Recieved key here
                APIClient.token_key = signIn.getToken();
                listener.onLoggedIn();
            }

            @Override
            public void onFailure(Call<SignIn> call, Throwable t) {
                call.cancel();
                listener.onLogingFailed();
            }
        });
    }
}
