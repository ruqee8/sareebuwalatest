package com.journaldev.zebrasolar.backend.common;

public interface LoginListener {
    public void onLoggedIn();
    public void onLogingFailed();
}
