package com.journaldev.zebrasolar.events;

public class AppEventMsg {
    private AppEventsDictionary.AppEvent m_event;
    private Object m_userData;

    public AppEventMsg(AppEventsDictionary.AppEvent event, Object userData)
    {
        m_event = event;
        m_userData = userData;
    }

    public AppEventsDictionary.AppEvent getEvent() { return m_event; }
    public Object getUserData() { return m_userData; }
}
