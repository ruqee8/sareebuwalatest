package com.journaldev.zebrasolar.events;

public interface AppEventListener {
    void onEvent(AppEventMsg event);
}
