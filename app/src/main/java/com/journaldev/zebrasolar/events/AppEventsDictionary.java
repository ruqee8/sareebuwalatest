package com.journaldev.zebrasolar.events;

public class AppEventsDictionary {
    public enum AppEvent {
        UNDEFINED,

        // BILLING FRAGMENT EVENTS
        BILL_FRAG_ON_ITEM_CLICKED,


        BILL_FRAG_ON_BILL_ITEM_CLICKED,

        BILL_FRAG_BILL_ITEM_DELETE,

        REF_DATA_DYNAMIC_UPDATE,

        INTERNET_ONLINE,

        INTERNET_OFFLINE,

        SENDING_JOB_TO_SERVER,

        SENDING_JOB_TO_SERVER_SUCCESS,

        SENDING_JOB_TO_SERVER_FAILED,

        SENDING_PAYMENT_TO_SERVER,

        SENDING_PAYMENT_TO_SERVER_SUCCESS,

        SENDING_PAYMENT_TO_SERVER_FAILED,

        ON_PAYMENT_TAB,

        PAYMENT_SAVE,

        PAYMENT_PRINT,

        GOTO_PAYMENT,
    }
}

