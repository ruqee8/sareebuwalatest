package com.journaldev.zebrasolar.thread;


public class MessageStruct {

    public static enum MessageType {
        UNDEFINED,

        // BILLING FRAGMENT EVENTS
        INTERNET_CHECK,


        JOB_SYNCHRONICE,

        PAYMENT_SYNCRONIZE
    }




    private MessageType type = MessageType.UNDEFINED;


    //Variables for send bill message
    private Object m_userData;
    private boolean m_isonline;

    public MessageStruct()
    {
        m_isonline = false;
    }

    public void setOnlineStatus(boolean isOnline)
    {
        m_isonline = isOnline;
    }

    public boolean getOnlineStatus()
    {
        return m_isonline;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public void setUserData(Object object) {
        m_userData = object;
    }

    public Object getUserData()
    {
        return m_userData;
    }



}
