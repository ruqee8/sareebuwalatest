package com.journaldev.zebrasolar.thread;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.ft.FtHandler;
import com.journaldev.zebrasolar.events.AppEventListener;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventsDictionary;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.pojo.SignIn;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.pojo.job.JobCreateRequest;
import com.journaldev.zebrasolar.pojo.job.JobCreateResponse;
import com.journaldev.zebrasolar.pojo.payment.PaymentCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentCreateResponse;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import retrofit2.Call;
import retrofit2.Response;

public class BackendSynchronizeHandler extends Handler {

    APIInterface apiInterface;
    APIInterface apiInterfaceWithoutKey;
    AppEventListener m_appEventListener;
    Context m_context;

    Queue<MessageStruct> queue = new LinkedList<>();

    FtHandler m_ftHandler;


    public BackendSynchronizeHandler(Looper myLooper, Context context)
    {
        super(myLooper);
        m_context = context;
        m_ftHandler = new FtHandler(m_context);

        /*REST API Interfaces initialization
         * 1 - KEY Token Base API
         * 2 - API Without Key -- use for login
         */
        apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterfaceWithoutKey = APIClient.getClientWithoutKey().create(APIInterface.class);

    }

    //Main method
    public void handleMessage(Message msg) {



        MessageStruct object = (MessageStruct) msg.obj;

        if((object.getType()== MessageStruct.MessageType.INTERNET_CHECK ) && ((object.getOnlineStatus()) == true ) ){
            Log.w("Syncronize Thread", "Internet connection working");

            MessageStruct tempObj;

            while ((tempObj = queue.peek()) != null) {

                if(tempObj.getType() == MessageStruct.MessageType.JOB_SYNCHRONICE) {

                    if (sendJobMessageToServer(tempObj) == false) {
                        break;
                    } else {
                        queue.poll();
                    }
                }

                if(tempObj.getType() == MessageStruct.MessageType.PAYMENT_SYNCRONIZE) {

                     if (sendPaymentMessageToServer(tempObj) == false) {
                        break;
                    } else {
                        queue.poll();
                    }
                }


            }

            Log.w("Syncronize Thread", "Synchronized");
            AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.INTERNET_ONLINE, null);
            m_appEventListener.onEvent(event);//use to update "label internet" checker
        }
        else if ((object.getType()== MessageStruct.MessageType.INTERNET_CHECK ) && ((object.getOnlineStatus()) == false ))
        {
            Log.w("Syncronize Thread", "Internet connection not working");

            AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.INTERNET_OFFLINE, null);
            m_appEventListener.onEvent(event);//use to update "label internet" checker
        }
        else if (object.getType()== MessageStruct.MessageType.JOB_SYNCHRONICE)
        {
            Log.w("Syncronize Thread", "Synchronizing adding job message to the java queue " );
            queue.add(object);
        }
        else if (object.getType()== MessageStruct.MessageType.PAYMENT_SYNCRONIZE)
        {
            Log.w("Syncronize Thread", "Synchronizing adding payment message to the java queue " );
            queue.add(object);
        }

    }

    private boolean sendJobMessageToServer(MessageStruct jobMsg )
    {


        Job job = (Job) jobMsg.getUserData();
        Log.w("Syncronize Thread", "Sending message to the Server " + job.getJobId() );

        String messageToStatusBar = "Sending Job " + job.getJobId() + " ..." ;
        AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_JOB_TO_SERVER, messageToStatusBar);
        m_appEventListener.onEvent(event);//use to update "label internet" checker



        if (sendJob(job) == false)
        {
            if(userSignIn() == true)
            {
                if(sendJob(job) == true)
                {
                    //add when FT added
                    int refSeqId = ((Job) jobMsg.getUserData()).getRefSeqId();
                    updateFTSyncTable(refSeqId, Enums.TableType.JOB,((Job) jobMsg.getUserData()).getRefId());

                    messageToStatusBar = "Successfully Sent Job " + job.getJobId() ;
                    event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_JOB_TO_SERVER_SUCCESS, messageToStatusBar);
                    m_appEventListener.onEvent(event);//use to update "label internet" checker


                    return true;
                }

                messageToStatusBar = "Failed to Send Job " + job.getJobId() ;
                event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_JOB_TO_SERVER_FAILED, messageToStatusBar);
                m_appEventListener.onEvent(event);//use to update "label internet" checker
                return false;
            }
            else
            {
                messageToStatusBar = "Failed to Send Job " + job.getJobId() ;
                event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_JOB_TO_SERVER_FAILED, messageToStatusBar);
                m_appEventListener.onEvent(event);//use to update "label internet" checker
                return false;
            }
        }
        else
        {
            //add when FT added
            int refSeqId =((Job) jobMsg.getUserData()).getRefSeqId();
            updateFTSyncTable(refSeqId, Enums.TableType.JOB,((Job) jobMsg.getUserData()).getRefId());

            messageToStatusBar = "Successfully Sent Job " + job.getJobId() ;
            event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_JOB_TO_SERVER_SUCCESS, messageToStatusBar);
            m_appEventListener.onEvent(event);//use to update "label internet" checker

            return true;
        }

    }

    private boolean sendPaymentMessageToServer(MessageStruct paymentMsg )
    {
        Payment payment = (Payment) paymentMsg.getUserData();
        Log.w("Syncronize Thread", "Sending payment message to the Server " + payment.getJobId()+" " + payment.getPaymentId() );

        String messageToStatusBar = "Sending Payment for Job - " + payment.getJobId() + "  Payment - " + payment.getPaymentId() ;
        AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_PAYMENT_TO_SERVER, messageToStatusBar);
        m_appEventListener.onEvent(event);//use to update "label internet" checker


        if (sendPayment(payment) == false)
        {
            if(userSignIn() == true)
            {
                if(sendPayment(payment) == true)
                {
                    //add when FT added
                    int refSeqId = ((Payment) paymentMsg.getUserData()).getRefSeqId();
                    updateFTSyncTable(refSeqId, Enums.TableType.PAYMENT,((Payment) paymentMsg.getUserData()).getRefId());

                    messageToStatusBar = "Successfully sent payment for job - " + payment.getJobId() + " Payment - " + payment.getPaymentId() ;
                    event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_PAYMENT_TO_SERVER_SUCCESS, messageToStatusBar);
                    m_appEventListener.onEvent(event);//use to update "label internet" checker

                    return true;
                }

                messageToStatusBar = "Failed to sent payment for job - " + payment.getJobId() + " Payment - " + payment.getPaymentId() ;
                event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_PAYMENT_TO_SERVER_FAILED, messageToStatusBar);
                m_appEventListener.onEvent(event);//use to update "label internet" checker
                return false;
            }
            else
            {
                messageToStatusBar = "Failed to sent payment for job - " + payment.getJobId() + " Payment -  " + payment.getPaymentId() ;
                event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_PAYMENT_TO_SERVER_FAILED, messageToStatusBar);
                m_appEventListener.onEvent(event);//use to update "label internet" checker
                return false;
            }
        }
        else
        {
            //add when FT added
            int refSeqId = ((Payment) paymentMsg.getUserData()).getRefSeqId();
            updateFTSyncTable(refSeqId, Enums.TableType.PAYMENT,((Payment) paymentMsg.getUserData()).getRefId());

            messageToStatusBar = "Successfully sent payment for job - " + payment.getJobId() + " Payment - " + payment.getPaymentId() ;
            event = new AppEventMsg(AppEventsDictionary.AppEvent.SENDING_PAYMENT_TO_SERVER_SUCCESS, messageToStatusBar);
            m_appEventListener.onEvent(event);//use to update "label internet" checker

            return true;
        }

    }

    ////////////////////////////////////////
    //set event handler
    public void setEventHandler(AppEventListener obj)
    {
        m_appEventListener = obj;
    }



    /*/////////////////////////////////////////////////
     * Sign in User
     */////////////////////////////////////////////////
    private boolean userSignIn() {


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(m_context);
        String email = sharedPref.getString("userName","none");
        String password = sharedPref.getString("password","none");

        //String email = "rep";
        //String password = "rep";


        //Call<SignIn> call4 = apiInterface.loginUser("wssko8s4k0ccsks8oc8cc00oc0o4g0sookc880k8",email,password);
        Call<SignIn> call = apiInterfaceWithoutKey.loginUserWithoutKey(email,password);
        call.request().url();
        Log.d("Test",call.toString());


        try {
            Response<SignIn> response = call.execute();
            SignIn signIn = response.body();
            boolean status = signIn.getStatus();

            if(status == false)
            {

                Log.w("Syncronize Thread","Invalid Credentials");
                return false;
            }

            APIClient.token_key = signIn.getToken();
            return true;



        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }


        private boolean sendJob(Job job)
        {

            JobCreateRequest request = job.toJobCreateRequest();

            Call<Object> call = apiInterface.insertJob(request);
            call.request().url();

            try {
                Response<Object> response = call.execute();

                JobCreateResponse jobCreateResponseObj = new Gson().fromJson(new Gson().toJson(((LinkedTreeMap<String, Object>) response.body())), JobCreateResponse.class);

                if (jobCreateResponseObj instanceof JobCreateResponse) {


                    if (jobCreateResponseObj.getStatus()) {

                        Log.w("Syncronize Thread", "Sending Job message to the Server success " + jobCreateResponseObj.getRefSeq() );

                        return true;

                    } else {

                       return false;
                    }

                }
                else{
                   return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }


    private boolean sendPayment(Payment payment)
    {

        PaymentCreateRequest request = payment.toPaymentCreateRequest();

        Call<Object> call = apiInterface.insertPayment(request);
        call.request().url();

        try {
            Response<Object> response = call.execute();

            PaymentCreateResponse paymentCreateResponseObj = new Gson().fromJson(new Gson().toJson(((LinkedTreeMap<String, Object>) response.body())), PaymentCreateResponse.class);

            if (paymentCreateResponseObj instanceof PaymentCreateResponse) {


                if (paymentCreateResponseObj.getStatus()) {

                    Log.w("Syncronize Thread", "Sending payment message to the Server success " + paymentCreateResponseObj.getSourceSeq() );

                    return true;

                } else {
                    return false;
                }

            }
            else{

                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


        //########################################################################
        // update FT Sync Table
        //########################################################################
        private boolean updateFTSyncTable(Integer refSeq, Enums.TableType tableType,int userId)
        {
            Log.w("Syncronize Thread", "Updating Sync Table" + String.valueOf(refSeq) );
            m_ftHandler.onPostSuccess(refSeq,tableType,userId);
            return true;
        }


    }


