package com.journaldev.zebrasolar.thread;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Message;

public class InternetCheckerRunnable implements Runnable {

    BackendSynchronizeHandler m_handler;
    Context m_context;

    public InternetCheckerRunnable(BackendSynchronizeHandler mHandler, Context context)
    {
        m_handler = mHandler;
        m_context = context;


    }
    @Override
    public void run() {

        while (true) {

            try {
                Thread.sleep(10000);

                boolean isOnline = networkStateCheck();


                    Message msg = m_handler.obtainMessage();
                    MessageStruct sendObj = new MessageStruct();
                    sendObj.setType(MessageStruct.MessageType.INTERNET_CHECK);

                    sendObj.setOnlineStatus(isOnline);

                    msg.obj =  sendObj;// Some Arbitrary object
                    m_handler.sendMessage(msg);



            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public boolean networkStateCheck() {
        ConnectivityManager cm =
                (ConnectivityManager) m_context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
