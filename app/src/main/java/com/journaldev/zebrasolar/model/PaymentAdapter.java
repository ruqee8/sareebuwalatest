package com.journaldev.zebrasolar.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.backend.common.Enums;


import java.util.List;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.PaymentViewHolder>{
    private List<Payment> paymentsList;
    Context context;

    public PaymentAdapter(List<Payment> paymentsList , Context context) {
        this.paymentsList = paymentsList ;
        this.context = context;
    }

    @Override
    public PaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View paymentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_card_layout, parent, false);
        PaymentViewHolder gvh = new PaymentViewHolder(paymentView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(PaymentViewHolder holder, final int position) {

        Payment payment = paymentsList.get(position);
        holder.txtPaymentId.setText(Integer.toString(payment.getPaymentId()));
        holder.payDate.setText(payment.getDate());
        holder.amount.setText(Double.toString(payment.getPaymentAmount()));

        //cheque type
        if (payment.getPayCashChequeType() == Enums.PAYTYPES.CASH.getValue())
        {
            holder.cashOrCheque.setText("CASH");
        }
        else
        {
            holder.cashOrCheque.setText("CHEQUE");
        }

        holder.isChequeApproved.setText(Integer.toString(payment.getChequeStatus()));


        holder.txtPaymentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String paymentName = Integer.toString(paymentsList.get(position).getPaymentId());
                Toast.makeText(context, paymentName + " is selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentsList.size();
    }

    public class PaymentViewHolder extends RecyclerView.ViewHolder {

        TextView txtPaymentId;
        TextView payDate;
        TextView amount;
        TextView cashOrCheque;
        TextView isChequeApproved;


        public PaymentViewHolder(View view) {
            super(view);
            txtPaymentId= view.findViewById(R.id.idPaymentId);
            payDate= view.findViewById(R.id.paymentDate);
            amount = view.findViewById(R.id.paymentAmount);
            cashOrCheque = view.findViewById(R.id.cashOrCheque);
            isChequeApproved = view.findViewById(R.id.isChequeApproved);
        }

    }
}