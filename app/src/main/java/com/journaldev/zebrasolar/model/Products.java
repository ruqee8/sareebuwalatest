package com.journaldev.zebrasolar.model;

import android.content.Context;

import com.journaldev.zebrasolar.db.ReferenceDataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Products {
    private static volatile Products m_instance;

    private Map<String,Integer> m_products;
    private Map<Integer,String> m_productsById;

    private Products()
    {
    }

    public final Map<String,Integer> getMapProducts(){return m_products;}
    public final Map<Integer,String> getMapProductsById(){return m_productsById;}

    private static void loadProducts(Context context)
    {
        m_instance.m_products = new HashMap<>();
        m_instance.m_productsById = new HashMap<>();
        ReferenceDataManager referenceDataManager = new ReferenceDataManager(context);
        referenceDataManager.loadProducts(m_instance.m_products,m_instance.m_productsById);
    }

    public List<String> getProductList()
    {
        List<String> prodList = new ArrayList<String>();

        for (Map.Entry<String, Integer> entry : m_instance.m_products.entrySet())
            prodList.add(entry.getKey());

        return prodList;
    }

    public int getProductId(String productName)
    {
        if(m_instance.m_products.get(productName) != null)
        {
            return m_instance.m_products.get(productName);
        }
        return -1;
    }

    public String getProductName(Integer productId)
    {
        if(m_instance.m_productsById.get(productId) != null)
        {
            return m_instance.m_productsById.get(productId);
        }
        return "";
    }


    public static void update(){
        synchronized (Products.class){
            m_instance = null;
        }
    }

    public static Products instance(Context context)
    {
        if (m_instance == null)
        {
            synchronized (Products.class){
                if (m_instance == null)
                {
                    m_instance = new Products();
                    loadProducts(context);
                }
            }
        }
        return m_instance;
    }
}
