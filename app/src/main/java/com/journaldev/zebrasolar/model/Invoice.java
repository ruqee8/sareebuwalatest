package com.journaldev.zebrasolar.model;

import com.journaldev.zebrasolar.pojo.payment.PaymentCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Invoice {


    private String invoiceID;
    private String jobId;
    private String RefId;
    private String RefSeqId;
    private String description;
    private String total;
    private String date;

    public String getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        this.invoiceID = invoiceID;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getRefId() {
        return RefId;
    }

    public void setRefId(String refId) {
        RefId = refId;
    }

    public String getRefSeqId() {
        return RefSeqId;
    }

    public void setRefSeqId(String refSeqId) {
        RefSeqId = refSeqId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total)
    {
        if(total.equals(""))
        {
            this.total = "0";
        }
        else {
            this.total = total;
        }
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getDate(){return date;}


    public PaymentCreateRequest toPaymentCreateRequest()
    {


        PaymentCreateRequest p = new PaymentCreateRequest();

        /*
        p.setPaymentId(invoiceID);
        p.setJobId(jobId);
        p.setRefId(RefId);
        p.setRefSeqId(RefSeqId);
        p.setPaymentAmount(description);
        p.setPanaltyAmount(total);
        p.setDate(date);
        */
        return p;

    }

    public static Invoice getInvoice(PaymentSyncResponse ps)
    {
        Invoice p = new Invoice();

        /*
        p.setInvoiceID(ps.getPaymentId());
        p.setJobId(ps.getJobId());
        p.setRefId(ps.getRefId());
        p.setRefSeqId(ps.getRefSeqId());
        p.setDescription(ps.getPaymentAmount());
        p.setTotal(ps.getPanaltyAmount());

        */
        return p;
    }





}
