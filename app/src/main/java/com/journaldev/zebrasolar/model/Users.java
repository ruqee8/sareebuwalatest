package com.journaldev.zebrasolar.model;

import android.content.Context;

import com.journaldev.zebrasolar.db.ReferenceDataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Users {
    private static volatile Users m_instance;

    private static String m_currentUser;

    private Map<String,Integer> m_users;

    private Map<Integer,String> m_collectors;

    private Users()
    {
    }

    public  static void set_currentUser(String userName)
    {
        m_currentUser = userName;
    }
    public final Map<String,Integer> getMapUsers(){return m_users;}
    public final Map<Integer,String> getMapCollectorUsers(){return m_collectors;}

    private static void loadUsers(Context context)
    {
        m_instance.m_users = new HashMap<>();
        m_instance.m_collectors = new HashMap<>();
        ReferenceDataManager referenceDataManager = new ReferenceDataManager(context);
        referenceDataManager.loadUsers(m_instance.m_users);
        referenceDataManager.loadCollectorUsers(m_instance.m_collectors);
    }

    public int getUserId(String userName)
    {
        if(m_instance.m_users.get(userName) != null)
        {
            return m_instance.m_users.get(userName);
        }
        return -1;
    }

    public static int getCurrentUserId()
    {
        if(m_instance.m_users.get(m_currentUser) != null)
        {
            return m_instance.m_users.get(m_currentUser);
        }
        return -1;
    }

    public List<String> getUserList()
    {
        // using values() for iteration over keys
        List<String> userIds = new ArrayList<>();
        for (Integer val  : m_instance.m_users.values())
            userIds.add(Integer.toString(val));
        return userIds;
    }


    public static void update(){
        synchronized (Users.class){
            m_instance = null;
        }
    }

    public static Users instance(Context context)
    {
        if (m_instance == null)
        {
            synchronized (Users.class){
                if (m_instance == null)
                {
                    m_instance = new Users();
                    loadUsers(context);
                }
            }
        }
        return m_instance;
    }
}
