package com.journaldev.zebrasolar.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ForwardBalanceModel {


    private String jobId;

    private String forwardBalance;


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }


    public String getForwardBalance() {
        return forwardBalance;
    }

    public void setForwardBalance(String forwardBalance) {
        this.forwardBalance = forwardBalance;
    }




}
