package com.journaldev.zebrasolar.model;

import com.journaldev.zebrasolar.pojo.job.JobCreateRequest;
import com.journaldev.zebrasolar.pojo.job.JobGPSCreateRequest;
import com.journaldev.zebrasolar.pojo.job.JobSyncResponse;

public class Job {

    private Integer orginId;

    private Integer refId;

    private Integer refSeqId;

    private String jobId;

    private Integer collectorId;

    private String name;

    private String customerId;

    private String phone;

    private String address;

    private Integer installmentType;

    private double totalAmount;

    private Integer prodId;

    private Integer areaId;

    private double quantity;

    private double downPayment;

    private Integer noOfInstallments;

    private String date;

    private String firstPaymentDate;

    private Integer status;

    private double latitude;

    private double longtitude;

    public Integer getOrginId() {
        return orginId;
    }

    public void setOrginId(Integer orginId) {
        this.orginId = orginId;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public Integer getRefSeqId() {
        return refSeqId;
    }

    public void setRefSeqId(Integer refSeqId) {
        this.refSeqId = refSeqId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Integer getCollectorId() {
        return collectorId;
    }

    public void setCollectorId(Integer collectorId) {
        this.collectorId = collectorId;
    }

    public String getCustomerName() {
        return name;
    }

    public void setCustomerName(String name) {
        this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getInstallmentType() {
        return installmentType;
    }

    public void setInstallmentType(Integer installment) {
        this.installmentType = installment;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getProdId() {
        return prodId;
    }

    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }


    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    public Integer getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(Integer noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFirstPaymentDate() {
        return firstPaymentDate;
    }

    public void setFirstPaymentDate(String date) {
        this.firstPaymentDate = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public JobCreateRequest toJobCreateRequest()
    {

        JobCreateRequest j = new JobCreateRequest();

        j.setOrginId(orginId);
        j.setRefId(refId);
        j.setRefSeqId(refSeqId);
        j.setJobId(jobId);
        j.setCollectorId(collectorId);
        j.setCustomerId(customerId);
        j.setName(name);
        j.setProdId(prodId);
        j.setCustomerId(customerId);
        j.setPhone(phone);
        j.setAddress(address);
        j.setInstallmentType(installmentType);
        j.setTotalAmount(totalAmount);
        j.setDownPayment(downPayment);
        j.setNoOfInstallments(noOfInstallments);
        j.setAreaId(areaId);
        j.setDate(date);
        j.setStatus(status);
        j.setLatitude(latitude);
        j.setLongitude(longtitude);


        return j;
    }


    public JobGPSCreateRequest toJobGPSCreateRequest() {

        JobGPSCreateRequest j = new JobGPSCreateRequest();

        j.setJobId(jobId);

        return j;

    }

    public static Job getJob(JobSyncResponse jr)
    {
        Job j = new Job();
        j.setOrginId(jr.getOrginId());
        j.setRefId(jr.getRefId());
        j.setRefSeqId(jr.getRefSeqId());
        j.setJobId(jr.getJobId());
        j.setCollectorId(jr.getCollectorId());
        j.setCustomerId(jr.getCustomerId());
        j.setCustomerName(jr.getName());
        j.setProdId(jr.getProdId());
        j.setCustomerId(jr.getCustomerId());
        j.setPhone(jr.getPhone());
        j.setAddress(jr.getAddress());
        j.setInstallmentType(jr.getInstallmentType());
        j.setTotalAmount(jr.getTotalAmount());
        j.setDownPayment(jr.getDownPayment());
        j.setNoOfInstallments(jr.getNoOfInstallments());
        j.setProdId(jr.getProdId());
        j.setAreaId(jr.getAreaId());
        j.setDate(jr.getDate());
        j.setFirstPaymentDate(jr.getFirstPaymentDate());
        j.setStatus(jr.getStatus());
        j.setLatitude(jr.getLatitude());
        j.setLongtitude(jr.getLongitude());

        return j;
    }





}
