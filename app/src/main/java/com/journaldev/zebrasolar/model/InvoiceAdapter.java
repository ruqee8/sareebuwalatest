package com.journaldev.zebrasolar.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.journaldev.zebrasolar.R;

import java.util.List;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.InvoiceViewHolder>{
    private List<Invoice> invoiceList;
    Context context;

    public InvoiceAdapter(List<Invoice> invoiceList, Context context) {
        this.invoiceList = invoiceList;
        this.context = context;
    }

    @Override
    public InvoiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View invoiceView = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_card_layout, parent, false);
        InvoiceViewHolder gvh = new InvoiceViewHolder(invoiceView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(InvoiceViewHolder holder, final int position) {

        Invoice invoice = invoiceList.get(position);
        holder.txtInvoiceId.setText(invoice.getInvoiceID());
        holder.invDate.setText(invoice.getDate());
        holder.total.setText(invoice.getTotal());
        holder.description.setText(invoice.getDescription());

        holder.txtInvoiceId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String paymentName = invoiceList.get(position).getInvoiceID().toString();
                Toast.makeText(context, paymentName + " is selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }

    public class InvoiceViewHolder extends RecyclerView.ViewHolder {

        TextView txtInvoiceId;
        TextView invDate;
        TextView description;
        TextView total;


        public InvoiceViewHolder(View view) {
            super(view);
            txtInvoiceId = view.findViewById(R.id.idInvoiceId);
            invDate= view.findViewById(R.id.invoiceDate);
            description = view.findViewById(R.id.description);
            total = view.findViewById(R.id.totalAmount);
        }

    }
}