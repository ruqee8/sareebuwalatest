package com.journaldev.zebrasolar.model;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.pojo.job.JobCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Payment {


    private int paymentId;
    private String jobId;
    private int RefId;
    private int RefSeqId;
    private double paymentAmount;
    private double panaltyAmount;
    private int payCashChequeType;//cash or cheque
    private int payType;//normal or panalty
    private String date;
    private int chequeStatus;//new or rejected
    private String nextDate;




    private Integer status;

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getRefId() {
        return RefId;
    }

    public void setRefId(int refId) {
        RefId = refId;
    }

    public int getRefSeqId() {
        return RefSeqId;
    }

    public void setRefSeqId(int refSeqId) {
        RefSeqId = refSeqId;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getPanaltyAmount() {
        return panaltyAmount;
    }

    public void setPanaltyAmount(double panaltyAmount)
    {
        if(panaltyAmount == 0)
        {
            this.panaltyAmount = 0.0;
        }
        else {
            this.panaltyAmount = panaltyAmount;
        }
    }

    public int getPayCashChequeType() {
        return payCashChequeType;
    }

    public void setPayCashChequeType(int paymentType) {
        this.payCashChequeType = paymentType;

        if(paymentType == Enums.PAYTYPES.CASH.getValue())
        {
            setChequeStatus(1);
        }
        else
        {
            setChequeStatus(2);
        }
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public String getDate() {
        return date;
    }

    /*public void setDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        this.date = df.format(c);;
    }*/

    public void setDate(String date)
    {
        this.date = date;
    }

    public void setDateWhenSynced(String date)
    {
        this.date = date;
    }

    public int getChequeStatus() {
        return chequeStatus;
    }

    public void setChequeStatus(int chequeStatus) {
        this.chequeStatus = chequeStatus;
    }




    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String nextDate) {
        this.nextDate = nextDate;
    }



    public PaymentCreateRequest toPaymentCreateRequest()
    {


        PaymentCreateRequest p = new PaymentCreateRequest();

        p.setJobId(jobId);
        p.setPaymentId(paymentId);
        p.setRefId(RefId);
        p.setRefSeqId(RefSeqId);
        p.setPaymentAmount(paymentAmount);
        p.setPanaltyAmount(panaltyAmount);
        p.setPayCashChequeType(payCashChequeType);
        p.setPayType(payType);
        p.setDate(date);
        p.setNextDate(nextDate);
        p.setChequeStatus(chequeStatus);
        p.setStatus(status);


        return p;

    }

    public static Payment getPayment(PaymentSyncResponse ps)
    {
        Payment p = new Payment();

        p.setPaymentId(ps.getPaymentId());
        p.setJobId(ps.getJobId());
        p.setRefId(ps.getRefId());
        p.setRefSeqId(ps.getRefSeqId());
        p.setPaymentAmount(ps.getPaymentAmount());
        p.setPanaltyAmount(ps.getPanaltyAmount());
        p.setPayCashChequeType(ps.getPayCashChequeType());
        p.setPayType(ps.getPayType());
        p.setDateWhenSynced(ps.getDate());
        p.setChequeStatus(ps.getChequeStatus());
        p.setStatus(ps.getStatus());
        p.setNextDate(ps.getNextDate());


        return p;



    }





}
