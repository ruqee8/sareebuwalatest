package com.journaldev.zebrasolar.model;

import com.journaldev.zebrasolar.pojo.payment.PaymentCreateRequest;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ForwardPayment {


    private String paymentId;
    private String jobId;
    private String paymentAmount;
    private String relatedDuedate;
    private boolean isSet;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }


    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }



    public String getRelatedDuedate() {
        return relatedDuedate;
    }

    public void setRelatedDuedate(String date) {
        relatedDuedate = date;
    }

    public void setDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        this.relatedDuedate = df.format(c);;
    }


    public boolean isSet() {
        return isSet;
    }

    public void setSet(boolean set) {
        isSet = set;
    }





}
