package com.journaldev.zebrasolar.model;

import android.content.Context;

import com.journaldev.zebrasolar.db.ReferenceDataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Areas {
    private static volatile Areas m_instance;

    private Map<String,Integer> m_areas;
    private Map<Integer,String> m_areasById;

    private Areas()
    {
    }

    public final Map<String,Integer> getMapAreas(){return m_areas;}
    public final Map<Integer,String> getMapAreasById(){return m_areasById;}

    private static void loadAreas(Context context)
    {
        m_instance.m_areas = new HashMap<>();
        m_instance.m_areasById = new HashMap<>();
        ReferenceDataManager referenceDataManager = new ReferenceDataManager(context);
        referenceDataManager.loadAreas(m_instance.m_areas,m_instance.m_areasById);
    }

    public List<String> getAreaList()
    {
        List<String> prodList = new ArrayList<String>();

        for (Map.Entry<String, Integer> entry : m_instance.m_areas.entrySet())
            prodList.add(entry.getKey());

        return prodList;
    }

    public int getAreaId(String areaName)
    {
        if(m_instance.m_areas.get(areaName) != null)
        {
            return m_instance.m_areas.get(areaName);
        }
        return -1;
    }

    public String getAreaName(Integer areaId)
    {
        if(m_instance.m_areasById.get(areaId) != null)
        {
            return m_instance.m_areasById.get(areaId);
        }
        return "";
    }


    public static void update(){
        synchronized (Areas.class){
            m_instance = null;
        }
    }

    public static Areas instance(Context context)
    {
        if (m_instance == null)
        {
            synchronized (Areas.class){
                if (m_instance == null)
                {
                    m_instance = new Areas();
                    loadAreas(context);
                }
            }
        }
        return m_instance;
    }
}
