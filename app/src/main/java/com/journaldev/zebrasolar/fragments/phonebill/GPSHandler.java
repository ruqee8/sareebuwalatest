package com.journaldev.zebrasolar.fragments.phonebill;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

public class GPSHandler implements LocationListener {

    Context m_context;
    LocationManager locationManager;
    Activity m_parentActivity;
    private ProgressDialog m_progressDialog;
    private JobUpdater m_jobUpdater;

    public GPSHandler(Context context,Activity activity,JobUpdater jobUpdater)
    {
        m_context = context;
        m_parentActivity = activity;
        m_jobUpdater = jobUpdater;
    }


    public void onClickSetGPS()
    {

        checkPermission();

        m_progressDialog = new ProgressDialog(m_parentActivity);
        m_progressDialog.setMessage("Setting GPS");
        m_progressDialog.show();

        getLocation();
    }

    public void checkPermission()
    {
        if (ContextCompat.checkSelfPermission(m_context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(m_context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(m_parentActivity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) m_parentActivity.getSystemService(Context.LOCATION_SERVICE);
            //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            Log.d("LOCATION", "onLocationChanged:get location executed ");
        }
        catch(SecurityException e) {
            Log.d("LOCATION", "onLocationChanged:exeception ");
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        Log.d("LOCATION", "onLocationChanged: " + "Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude());

        if(location.getLongitude() == 0.0 || location.getLatitude() == 0.0)
        {
            m_progressDialog.dismiss();
            return;
        }

        m_progressDialog.dismiss();

        m_jobUpdater.updateJobWithGPS(location.getLatitude(),location.getLongitude());
        Toast.makeText(m_parentActivity, "GPS saved successfully"+ "Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude(), Toast.LENGTH_SHORT).show();

        //avoid call again and again after first insertion
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
