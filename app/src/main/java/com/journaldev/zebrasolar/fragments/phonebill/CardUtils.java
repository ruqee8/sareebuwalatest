package com.journaldev.zebrasolar.fragments.phonebill;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.model.Job;

import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CardUtils {


    static List<Date> m_poyaDays;


    public static void setPoyaDays()
    {
        m_poyaDays = new ArrayList<Date>();

        String jan2019 = "2019-01-20";
        parseAndSet(jan2019);

        String feb2019 = "2019-02-19";
        parseAndSet(feb2019);

        String mar2019 = "2019-03-20";
        parseAndSet(mar2019);

        String apr2019 = "2019-04-19";
        parseAndSet(apr2019);

        String may2019 = "2019-05-18";
        parseAndSet(may2019);

        String may20192 = "2019-05-19";
        parseAndSet(may20192);

        String may20193 = "2019-05-20";
        parseAndSet(may20193);

        String jun2019 = "2019-06-16";
        parseAndSet(jun2019);

        String jul2019 = "2019-07-16";
        parseAndSet(jul2019);

        String aug2019 = "2019-08-14";
        parseAndSet(aug2019);

        String sep2019 = "2019-09-13";
        parseAndSet(sep2019);

        String oct2019 = "2019-10-13";
        parseAndSet(oct2019);

        String nov2019 = "2019-11-12";
        parseAndSet(nov2019);

        String dec2019 = "2019-12-11";
        parseAndSet(dec2019);


        ///2020

        String jan2020 = "2020-01-10";
        parseAndSet(jan2020);

        String feb2020 = "2020-02-08";
        parseAndSet(feb2020);

        String mar2020 = "2020-03-09";
        parseAndSet(mar2020);

        String apr2020 = "2020-04-07";
        parseAndSet(apr2020);

        String may2020 = "2020-05-07";
        parseAndSet(may2020);

        String may20202 = "2020-05-08";
        parseAndSet(may20202);

        String jun2020 = "2020-06-05";
        parseAndSet(jun2020);

        String jul2020 = "2020-07-04";
        parseAndSet(jul2020);

        String aug2020 = "2020-08-03";
        parseAndSet(aug2020);

        String sep2020 = "2020-09-01";
        parseAndSet(sep2020);

        String oct2020 = "2020-10-01";
        parseAndSet(oct2020);

        String oct20202 = "2020-10-30";
        parseAndSet(oct20202);

        String nov2020 = "2020-11-29";
        parseAndSet(nov2020);

        String dec2020 = "2020-12-29";
        parseAndSet(dec2020);


        ///2021

        String jan2021 = "2021-01-28";
        parseAndSet(jan2021);

        String feb2021 = "2021-02-26";
        parseAndSet(feb2021);

        String mar2021 = "2021-03-28";
        parseAndSet(mar2021);

        String apr2021 = "2021-04-26";
        parseAndSet(apr2021);

        String may2021 = "2021-05-26";
        parseAndSet(may2021);

        String may20212 = "2021-05-27";
        parseAndSet(may20212);

        String jun2021 = "2021-06-24";
        parseAndSet(jun2021);

        String jul2021 = "2021-07-23";
        parseAndSet(jul2021);

        String aug2021 = "2021-08-22";
        parseAndSet(aug2021);

        String sep2021 = "2021-09-20";
        parseAndSet(sep2021);

        String oct2021 = "2021-10-20";
        parseAndSet(oct2021);

        String nov2021 = "2021-11-18";
        parseAndSet(nov2021);

        String dec2021 = "2021-12-18";
        parseAndSet(dec2021);


    }

    private static void parseAndSet(String date)
    {
        try {
            Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(date);
            m_poyaDays.add(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static int getPoyaCount(Date startDate,Date endDate)
    {
        Date min = removeTime(startDate);
        Date max = removeTime(endDate);

        int poyaCount = 0;

        for (Date date : m_poyaDays) {

            if(isDateInBetween(min,max,date))
            {

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                if ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) == false) {
                    poyaCount++;
                }
            }
        }

        return poyaCount;
    }

    public static boolean isDateInBetween(Date min,Date max,Date d)
    {
        //Date min, max;   // assume these are set to something
        //Date d;          // the date in question

        //Source
        //https://stackoverflow.com/questions/883060/how-can-i-determine-if-a-date-is-between-two-dates-in-java

        //return d.after(min) && d.before(max);
        return  !d.before(min) && !d.after(max);
    }

    /* * Java Method to find number of days between two dates * in Java using JodaTime library.
    To find difference * we first need to convert java.util.Date to LocalDate * in JodaTime. */

    public static int daysBetweenUsingJoda(Date d1, Date d2)
    {
        return Days.daysBetween(
                new org.joda.time.LocalDate(d2.getTime()), new org.joda.time.LocalDate(d1.getTime())).getDays();
    }


    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static int getSunDaysBetweenTwoDates(Date sDate, Date eDate) {

        Date startDate = removeTime(sDate);
        Date endDate = removeTime(eDate);

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        int sunDays = 0;

        //Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0;
        }

        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }

        do {
            //excluding start date
            startCal.add(Calendar.DAY_OF_MONTH, 1);
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                ++sunDays;
            }
        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        return sunDays;
    }

    public static int getMonthsDifference(Date date1, Date date2) {
        int m1 = date1.getYear() * 12 + date1.getMonth();
        int m2 = date2.getYear() * 12 + date2.getMonth();
        //return m2 - m1 -1;
        //return m2 - m1+1;
        return m2 - m1;
    }


    public static Date getCurrentDateRelatedMonthsDueDate(Job job)
    {

        Date date = Calendar.getInstance().getTime();

        if (date.getMonth() == 2)
        {
            date.setDate(28);
        }
        else {
            ///////////////////////////////////////////
            String dateStr = job.getFirstPaymentDate();
            if(dateStr == null || dateStr.equals("null"))
            {
                dateStr = job.getDate();
            }

            Date jobStartedDate = null;
            SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                jobStartedDate = format.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ///////////////////////////////////////////


            date.setDate(jobStartedDate.getDate());
        }

        return date;

    }

    public static Date getPrevMonthsDueDate(Job job)
    {

        Date date = Calendar.getInstance().getTime();
        date.setMonth(date.getMonth() -1);//set tp prev Month

        if (date.getMonth() == 2)
        {
            date.setDate(28);
        }
        else {

            ///////////////////////////////////////////
            String dateStr = job.getFirstPaymentDate();

            if(dateStr == null || dateStr.equals("null"))
            {
                dateStr = job.getDate();
            }


            Date jobStartedDate = null;
            SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                jobStartedDate = format.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ///////////////////////////////////////////


            date.setDate(jobStartedDate.getDate());
        }

        return date;

    }


    private int getDateDiff(Date today,Date startedDate)
    {
        long diff = today.getTime() - startedDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return (int)diffDays;
    }


    public static int getNoOfInstallmentsForToday(Job m_currentJob)
    {

        ///////////////////////////////////////////
        String dateStr = m_currentJob.getDate();

        Date jobStartedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            jobStartedDate = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ///////////////////////////////////////////

        Date currDate = Calendar.getInstance().getTime();

        //int dayDiff = getDateDiff(currDate,jobStartedDate);

        int dayDiffForBandaranayake = CardUtils.daysBetweenUsingJoda(currDate,jobStartedDate);
        int sundays = CardUtils.getSunDaysBetweenTwoDates(jobStartedDate,currDate);
        int poyaCount = CardUtils.getPoyaCount(jobStartedDate,currDate);
        int dayDiffForRanaweera = dayDiffForBandaranayake-sundays-poyaCount;


        //int dayDiff=dayDiffForRanaweera;
        int dayDiff=dayDiffForBandaranayake;


        int noOfinstallmentUpToToday = 0;

        if (m_currentJob.getInstallmentType() == Enums.INSTALLMENTTYPE.DAILY.getValue())
        {
            //noOfinstallmentUpToToday = (dayDiff/1)+1;//Ranaweera
            noOfinstallmentUpToToday = (dayDiff/1);//Bandaranayake
        }
        else if(m_currentJob.getInstallmentType() == Enums.INSTALLMENTTYPE.WEEKLY.getValue())
        {
            //noOfinstallmentUpToToday = (dayDiff/7)+1;//Ranaweera
            noOfinstallmentUpToToday = (dayDiff/7);//Bandaranayake
        }
        else {
            noOfinstallmentUpToToday = getMonthDiffForInstallments(m_currentJob);
        }

        //noOfInstallmentsForTodayText.setText(Integer.toString(noOfinstallmentUpToToday));
        return noOfinstallmentUpToToday;


    }


    public static int getMonthDiffForInstallments(Job m_currentJob)
    {
        Date currDate = Calendar.getInstance().getTime();
        Date thisMonthDueDate = CardUtils.getCurrentDateRelatedMonthsDueDate(m_currentJob);

        Date lastMonth = null;

        if (currDate.getDate() < thisMonthDueDate.getDate())
        {
            lastMonth = CardUtils.getPrevMonthsDueDate(m_currentJob);
        }
        else {
            lastMonth = CardUtils.getCurrentDateRelatedMonthsDueDate(m_currentJob);
        }

        ///////////////////////////////////////////
        String dateStr = m_currentJob.getFirstPaymentDate();

        if(dateStr == null || dateStr.equals("null"))
        {
            dateStr = m_currentJob.getDate();
        }

        Date jobStartedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            jobStartedDate = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ///////////////////////////////////////////



        return CardUtils.getMonthsDifference(jobStartedDate,lastMonth);
    }


}
