package com.journaldev.zebrasolar.fragments.cards;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.ReferenceDataManager;
import com.journaldev.zebrasolar.fragments.ArrearsValidator;
import com.journaldev.zebrasolar.model.Areas;
import com.journaldev.zebrasolar.model.Collector;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.pojo.area.Area;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CardsAllFragment extends Fragment implements View.OnClickListener {


    View m_thisView = null;

    //Search box
    private SearchView search;
    private Button arrearsBtn;
    private RecyclerView recyclerlist;
    private MaterialSpinner collectorSpinner;
    private MaterialSpinner areaSpinner;
    private CheckBox arrearsCheckBox;
    private CheckBox dailyCheckBox;
    private CheckBox weeklyCheckBox;
    private CheckBox monthlyCheckBox;
    private AdapterView.OnItemClickListener onItemClickListenerObj;

    private CardAdapter cardRecyclerAdapter;
    ArrayList<CardItem> cardItemsList;

    FragmentActivity m_context = null;


    private int m_currentCollectorId;
    private int m_currentAreaId;


    private boolean isArreasSelected;
    private boolean isDay,isWeek,isMonth;

    //DB Related Classes
    //Reference Data - static data
    ReferenceDataManager m_referenceDataManager;
    JobManager m_jobManager;
    PayManager m_payManager;

    //user name
    String m_user = null;
    Integer m_userId = null;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        //load fragment and get base activity
        m_thisView = inflater.inflate(R.layout.fragment_cards_new, container, false);
        m_context = (SalesMainActivity) getActivity();

        search = (SearchView) m_thisView.findViewById(R.id.search);
        arrearsBtn = (Button) m_thisView.findViewById(R.id.btnArrears);
        recyclerlist = (RecyclerView) m_thisView.findViewById(R.id.recyclerlist);
        collectorSpinner = (MaterialSpinner)m_thisView.findViewById(R.id.item_collector);
        areaSpinner = (MaterialSpinner)m_thisView.findViewById(R.id.item_area);

        //check boxes
        arrearsCheckBox =(CheckBox) m_thisView.findViewById(R.id.arreasBox);
        dailyCheckBox =(CheckBox) m_thisView.findViewById(R.id.dailyBox);
        weeklyCheckBox =(CheckBox) m_thisView.findViewById(R.id.weeklyBox);
        monthlyCheckBox =(CheckBox) m_thisView.findViewById(R.id.monthlyBox);

        //db Related Classes initialization
        m_referenceDataManager = new ReferenceDataManager(m_context);
        m_jobManager = new JobManager(m_context);
        m_payManager = new PayManager(m_context);

        populateUserData();
        //populateCardsRecyclerView();
        configureSearchView();

        populateCollectorSpinner();
        populateAreaSpinner();
        populateCheckBoxes();
        populatePreviousSetValues();

        populateRecyclerViewWithAllFilters();


        //setArrearsBtnBehavior();




        // Inflate the layout for this fragment
        return m_thisView;
    }


    //populate userRelatedData
    private void populateUserData()
    {
        m_user = getActivity().getIntent().getExtras().getString("userName");
        m_userId = Users.instance(m_context).getUserId(m_user);
    }


    private void setArrearsBtnBehavior()
    {
        arrearsBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateCardsRecyclerViewForArrears();
            }
        });

    }


    private void populatePreviousSetValues()
    {
        selectSpinnerPreviousPosition();
        selectAreaSpinnerPreviousPosition();
        Toast.makeText(getActivity(), Areas.instance(getContext()).getAreaName(m_currentAreaId), Toast.LENGTH_SHORT).show();

        setCheckBoxesPreviousValues();

        populateRecyclerViewWithAllFilters();
    }


    private void selectSpinnerPreviousPosition()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(m_context);
        int position = sharedPref.getInt("position",0);
        collectorSpinner.setSelectedIndex(position);

        m_currentCollectorId = sharedPref.getInt("collectorId",-1);//default is ALL -1


    }

    private void selectAreaSpinnerPreviousPosition()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(m_context);
        int position = sharedPref.getInt("area-position",0);
        areaSpinner.setSelectedIndex(position);

        m_currentAreaId = sharedPref.getInt("areaId",-1);//default is ALL -1

    }

    private void setCheckBoxesPreviousValues()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(m_context);
        isArreasSelected = sharedPref.getBoolean("arrears",false);
        arrearsCheckBox.setChecked(isArreasSelected);

        isDay = sharedPref.getBoolean("daily",false);
        dailyCheckBox.setChecked(isDay);

        isWeek = sharedPref.getBoolean("weekly",false);
        weeklyCheckBox.setChecked(isWeek);

        isMonth = sharedPref.getBoolean("monthly",false);
        monthlyCheckBox.setChecked(isMonth);

    }

    private void populateRecyclerViewWithAllFilters()
    {
        ArrearsValidator arrearsValidator = new ArrearsValidator(m_payManager);

        cardItemsList = new ArrayList<CardItem>();
        List<Job> joblist = m_jobManager.getJobListWithArgs(m_currentCollectorId,m_currentAreaId,isDay,isWeek,isMonth);

        for (Job job : joblist) {

            if (isArreasSelected) {

                //check set nextdate is greater than today
                if(isNextDateGreaterThanToday(job)) continue;

                arrearsValidator.setJob(job);

                if (arrearsValidator.isArrearsJob()) {
                    CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                    cardItemsList.add(a);
                }
            }
            else {
                CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                cardItemsList.add(a);
            }

        }


        ////////create recycler view for customers

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlist.setLayoutManager(layoutManager);
        cardRecyclerAdapter = new CardAdapter(cardItemsList,getContext());
        recyclerlist.addItemDecoration(new DividerItemDecoration(recyclerlist.getContext(), layoutManager.getOrientation()));
        recyclerlist.setAdapter(cardRecyclerAdapter);


        //populating onItemClick
        //create onitemclick listner for list view


        cardRecyclerAdapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtjobid = v.findViewById(R.id.text_view3);
                String cardId = txtjobid.getText().toString();
                CardItem selectedItem = findCardItem(cardId);

                ((SalesMainActivity) getActivity()).setM_currentCardItem(selectedItem);
                ((SalesMainActivity) getActivity()).onCardSelected(selectedItem);
            }
        });

    }


    private void populateCardsRecyclerView()
    {

        cardItemsList = new ArrayList<CardItem>();

        if(m_currentCollectorId == -1)//default item
        {
            cardItemsList = new ArrayList<CardItem>();
        }
        else if (m_currentCollectorId == -2)//all customers
        {
            List<Job> joblist =  m_jobManager.getJobList();
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                cardItemsList.add(a);
            }


        }
        else
        {
           List<Job> joblist =  m_jobManager.getJobListForCollectorId(m_currentCollectorId);
           cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                cardItemsList.add(a);
            }



        }

        ////////create recycler view for customers

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlist.setLayoutManager(layoutManager);
        cardRecyclerAdapter = new CardAdapter(cardItemsList,getContext());
        recyclerlist.addItemDecoration(new DividerItemDecoration(recyclerlist.getContext(), layoutManager.getOrientation()));
        recyclerlist.setAdapter(cardRecyclerAdapter);


        //populating onItemClick
        //create onitemclick listner for list view


        //populating onItemClick
        //create onitemclick listner for list view


        cardRecyclerAdapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = recyclerlist.indexOfChild(v);
                TextView txtjobid = v.findViewById(R.id.text_view3);
                String cardId = txtjobid.getText().toString();
                CardItem selectedItem = findCardItem(cardId);

                //CardItem selectedItem = (CardItem) cardItemsList.get(pos);

                //Toast.makeText(getActivity(), Integer.toString(pos), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), selectedItem.getId()
                //+ " " +selectedItem.getCustomerName(), Toast.LENGTH_SHORT).show();



                ((SalesMainActivity) getActivity()).setM_currentCardItem(selectedItem);
                ((SalesMainActivity) getActivity()).onCardSelected(selectedItem);
            }
        });


    }


    private void populateCardsRecyclerViewForArrears()
    {

        ArrearsValidator arrearsValidator = new ArrearsValidator(m_payManager);
        cardItemsList = new ArrayList<CardItem>();

        if(m_currentCollectorId == -1)//default item
        {
            cardItemsList = new ArrayList<CardItem>();
        }
        else if (m_currentCollectorId == -2)//all customers
        {
            List<Job> joblist =  m_jobManager.getJobList();
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                arrearsValidator.setJob(job);

                if(arrearsValidator.isArrearsJob()) {

                    CardItem a = new CardItem(job.getJobId(), job.getCustomerName(), job.getCustomerId());
                    cardItemsList.add(a);
                }
            }


        }
        else
        {
            List<Job> joblist =  m_jobManager.getJobListForCollectorId(m_currentCollectorId);
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {

                //check set nextdate is greater than today
                if(isNextDateGreaterThanToday(job)) continue;



                arrearsValidator.setJob(job);

                if(arrearsValidator.isArrearsJob()) {
                    CardItem a = new CardItem(job.getJobId(), job.getCustomerName(), job.getPhone());
                    cardItemsList.add(a);
                }
            }



        }

        ////////create recycler view for customers

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlist.setLayoutManager(layoutManager);
        cardRecyclerAdapter = new CardAdapter(cardItemsList,getContext());
        recyclerlist.addItemDecoration(new DividerItemDecoration(recyclerlist.getContext(), layoutManager.getOrientation()));
        recyclerlist.setAdapter(cardRecyclerAdapter);


        //populating onItemClick
        //create onitemclick listner for list view


        cardRecyclerAdapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = recyclerlist.indexOfChild(v);
                TextView txtjobid = v.findViewById(R.id.text_view3);
                String cardId = txtjobid.getText().toString();
                CardItem selectedItem = findCardItem(cardId);

                //CardItem selectedItem = (CardItem) cardItemsList.get(pos);

                //Toast.makeText(getActivity(), Integer.toString(pos), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), selectedItem.getId()
                        //+ " " +selectedItem.getCustomerName(), Toast.LENGTH_SHORT).show();



                ((SalesMainActivity) getActivity()).setM_currentCardItem(selectedItem);
                ((SalesMainActivity) getActivity()).onCardSelected(selectedItem);
            }
        });


    }

    CardItem findCardItem(String id)
    {
        for (CardItem item : cardItemsList)
        {
            if(item.getId().equalsIgnoreCase(id))
                return item;
        }

        return null;

    }



    private void configureSearchView()
    {
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String searchText) {
                cardRecyclerAdapter.getFilter().filter(searchText);
                return false;
            }
        });
    }


    private void populateCollectorSpinner()
    {
        List<Collector> collectorList = generateCollectorList();

        //Creating Route Selector
        ArrayAdapter<Collector> collectors = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                collectorList);
        collectorSpinner.setAdapter(collectors);

        //create  onItem Slectetor
        final MaterialSpinner.OnItemSelectedListener  OnItemSelectedListenerObj = new MaterialSpinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                Log.w("Collector Spinner","Collector -ID : " + ((Collector)item).id);
                m_currentCollectorId = ((Collector) item).id;
                //populateCardsRecyclerView();
                populateRecyclerViewWithAllFilters();
                saveSpinnerPosition(position);

            }

        };


        //OnClick in Routes should populate customer list
        collectorSpinner.setOnItemSelectedListener(OnItemSelectedListenerObj);
    }

    private void populateAreaSpinner()
    {
        List<Area> areaList = generateAreaList();

        //Creating Route Selector
        ArrayAdapter<Area> areas = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                areaList);
        areaSpinner.setAdapter(areas);

        //create  onItem Slectetor
        final MaterialSpinner.OnItemSelectedListener  OnItemSelectedListenerObj = new MaterialSpinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                Log.w("Area Spinner","Area -ID : " + ((Area)item).getId());
                m_currentAreaId = ((Area) item).getId();
                populateRecyclerViewWithAllFilters();
                saveAreaSpinnerPosition(position);

            }

        };


        //OnClick in Routes should populate customer list
        areaSpinner.setOnItemSelectedListener(OnItemSelectedListenerObj);
    }


    private void populateCheckBoxes()
    {

        arrearsCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    isArreasSelected = true;
                } else {
                    isArreasSelected = false;
                }

                populateRecyclerViewWithAllFilters();
                saveCheckBoxValues("arrears",isArreasSelected);
            }
        });

        dailyCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    isDay = true;
                } else {
                    isDay = false;
                }
                populateRecyclerViewWithAllFilters();
                saveCheckBoxValues("daily",isDay);
            }
        });


        weeklyCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    isWeek = true;
                } else {
                    isWeek = false;
                }
                populateRecyclerViewWithAllFilters();
                saveCheckBoxValues("weekly",isWeek);
            }
        });

        monthlyCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    isMonth = true;
                } else {
                    isMonth = false;
                }
                populateRecyclerViewWithAllFilters();
                saveCheckBoxValues("monthly",isMonth);
            }
        });

    }


    private List<Collector> generateCollectorList()
    {
        List<Collector> collectorList = new ArrayList<Collector>();

        collectorList.add(new Collector("All",-1));


        Map<Integer,String> collectorsMap = Users.instance(m_context).getMapCollectorUsers();

        if(collectorsMap == null) return collectorList;

        for (Map.Entry<Integer,String> entry : collectorsMap.entrySet())
        {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            collectorList.add(new Collector(entry.getValue(),entry.getKey()));
        }



        /*
        collectorList.add(new Collector("Rukshan",1));
        collectorList.add(new Collector("Dinushan",2));
        */

        return collectorList;
    }


    private List<Area> generateAreaList()
    {
        List<Area> arealist = new ArrayList<Area>();

        Area all = new Area();
        all.setId(-1);
        all.setName("ALL");
        arealist.add(all);


        Map<Integer,String> areasMap = Areas.instance(m_context).getMapAreasById();

        if(areasMap == null) return arealist;

        for (Map.Entry<Integer,String> entry : areasMap.entrySet())
        {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            Area area = new Area();
            area.setId(entry.getKey());
            area.setName(entry.getValue());
            arealist.add(area);
        }


        return arealist;
    }

    /*////////////////////////////////////////////////////////////////////
     *  check user name and password in shared preferences
     *////////////////////////////////////////////////////////////////////
    private void saveSpinnerPosition(int position)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("position",position);
        editor.putInt("collectorId",m_currentCollectorId);
        editor.commit();

    }

    /*////////////////////////////////////////////////////////////////////
     *  check user name and password in shared preferences
     *////////////////////////////////////////////////////////////////////
    private void saveAreaSpinnerPosition(int position)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("area-position",position);
        editor.putInt("areaId",m_currentAreaId);
        editor.commit();

    }

    private void saveCheckBoxValues(String key,boolean value)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key,value);
        editor.commit();

    }


    @Override
    public void onResume() {
        super.onResume();



    }

    @Override
    public void onClick(View v) {

    }


    public boolean isNextDateGreaterThanToday(Job job)
    {

        String nextDate = m_payManager.getMaxNextDate(job.getJobId());

        if (nextDate.equals("") == false) {


            //set current date
            Calendar custom = Calendar.getInstance();
            custom.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
            custom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
            custom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

            Date currentDate = custom.getTime();

            Date nxtDate = null;

            //get Next Date
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                nxtDate = format.parse(nextDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (nxtDate.compareTo(currentDate) > 0) {

                return false;
            }

            else {
                return true;
            }

        }

        return false;


    }
}
