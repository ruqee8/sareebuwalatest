package com.journaldev.zebrasolar.fragments.cards;

public class CardItem {

    private String id;
    private String customerName;
    private String customerIdno;

    public CardItem(String id, String customerName, String customerIdNo) {
        this.id = id;
        this.customerName = customerName;
        this.customerIdno = customerIdNo;
    }

    public String getId()
    {
        return this.id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerIdno() {
        return customerIdno;
    }


    public int getImageResource() {
        return 0;
    }
}
