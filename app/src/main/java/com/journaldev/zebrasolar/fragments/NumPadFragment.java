package com.journaldev.zebrasolar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.journaldev.zebrasolar.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NumPadFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NumPadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NumPadFragment extends Fragment implements View.OnClickListener  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    int sourceId;

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnClr;
    private Button btnDecimal;
    private Button btnOk;



    public NumPadFragment() {
        // Required empty public constructor
    }

    public void setSourceId(int sourceId)
    {
        this.sourceId = sourceId;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NumPadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NumPadFragment newInstance(String param1, String param2) {
        NumPadFragment fragment = new NumPadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_num_pad, container, false);

        //set buttons and Their Listners

        btn0 = v.findViewById(R.id.btnNumpad0);
        btn0.setOnClickListener(this);

        btn1 = v.findViewById(R.id.btnNumpad1);
        btn1.setOnClickListener(this);

        btn2 = v.findViewById(R.id.btnNumpad2);
        btn2.setOnClickListener(this);

        btn3 = v.findViewById(R.id.btnNumpad3);
        btn3.setOnClickListener(this);

        btn4 = v.findViewById(R.id.btnNumpad4);
        btn4.setOnClickListener(this);

        btn5 = v.findViewById(R.id.btnNumpad5);
        btn5.setOnClickListener(this);

        btn6 = v.findViewById(R.id.btnNumpad6);
        btn6.setOnClickListener(this);

        btn7 = v.findViewById(R.id.btnNumpad7);
        btn7.setOnClickListener(this);

        btn8 = v.findViewById(R.id.btnNumpad8);
        btn8.setOnClickListener(this);

        btn9 = v.findViewById(R.id.btnNumpad9);
        btn9.setOnClickListener(this);

        btnDecimal = v.findViewById(R.id.btnNumpadDecimal);
        btnDecimal.setOnClickListener(this);

        btnClr = v.findViewById(R.id.btnNumpadLeft);
        btnClr.setOnClickListener(this);

        btnOk = v.findViewById(R.id.btnNumpadRight);
        btnOk.setOnClickListener(this);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(int val) {
        if (mListener != null) {
            mListener.onNumPadFragmentInteraction(sourceId,val);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

        //Toast.makeText((Context) mListener, " button pressed \n", Toast.LENGTH_SHORT).show();
        if(v == btn0)
        {
            onButtonPressed(0);
        }
        else if(v == btn1)
        {
            onButtonPressed(1);
        }
        else if(v == btn2)
        {
            onButtonPressed(2);
        }
        else if(v == btn3)
        {
            onButtonPressed(3);
        }
        else if(v == btn4)
        {
            onButtonPressed(4);
        }
        else if(v == btn5)
        {
            onButtonPressed(5);
        }
        else if(v == btn6)
        {
            onButtonPressed(6);
        }
        else if(v == btn7)
        {
            onButtonPressed(7);
        }
        else if(v == btn8)
        {
            onButtonPressed(8);
        }
        else if(v == btn9)
        {
            onButtonPressed(9);
        }
        else if(v == btnClr)
        {
            onButtonPressed(10);
        }
        else if(v == btnOk)
        {
            onButtonPressed(11);
        }
        else if( v == btnDecimal)
        {
            onButtonPressed(12);
        }

    }

    public void disableButtons()
    {
        btnOk.setEnabled(false);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onNumPadFragmentInteraction(int sourceId, int value);
    }
}
