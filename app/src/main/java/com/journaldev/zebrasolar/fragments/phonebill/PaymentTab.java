package com.journaldev.zebrasolar.fragments.phonebill;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.ReferenceDataManager;
import com.journaldev.zebrasolar.events.AppEventListener;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventsDictionary;
import com.journaldev.zebrasolar.fragments.NumPadFragment;
import com.journaldev.zebrasolar.fragments.cards.CardItem;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.thread.MessageStruct;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinter;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinterFactory;
import com.journaldev.zebrasolar.utils.billPrinter.PaymentBill;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentTab extends Fragment implements AppEventListener,View.OnFocusChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    boolean isCashPriceSelected = false;
    boolean isChequeSelected = false;


    private View m_thisView;
    TextView cashValueText = null;
    Button chequeDateSetBtn;
    Button currentDateSetBtn;
    TextView dateValueText = null;


    CheckBox nextDateBox = null;

    TextView paymentIDView = null;
    TextView customerNameView = null;


    CardItem current_card;

    String m_currUser = null;
    String m_currentDate;

    //DB Related Classes
    //Reference Data - static data
    ReferenceDataManager m_referenceDataManager;
    PayManager m_payManager;



    //This is the initial reference number
    int m_currBillId;
    CardItem card;

    //Main
    SalesMainActivity salesMainActivity;




    Button printBtn;
    Button showBtn;
    Button saveBtn;
    Button resetBtn;
    Button prevBtn;

    //cheque date
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    Calendar c;
    DatePickerDialog datePickerDialog;
    String chequeDateStr;
    private int mYear, mMonth, mDay;


    //NumPad
    NumPadFragment numPadFragment;


    //List view related
    ListView m_billItemsListView;



    int m_prevBillIndex;//this used for generate previous bills



    private OnFragmentInteractionListener mListener;

    //event listner to comunicate with PhoneBillFragment
    private AppEventListener m_parentAppEventListner;

    public PaymentTab()  {


    }

    public void setEventListner(AppEventListener appEventListener)
    {
        m_parentAppEventListner = appEventListener;
    }


    public void setM_currUser(String user)
    {
        m_currUser = user;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentTab.
     */
    // TODO: Rename and change types and number of parameters
    public static PaymentTab newInstance(String param1, String param2) {
        PaymentTab fragment = new PaymentTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //getting the recyclerview from xml

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        m_thisView = inflater.inflate(R.layout.fragment_phone_payment_tab, container, false);
        m_payManager = new PayManager(getContext());


        salesMainActivity = (SalesMainActivity) getActivity();
        current_card = ((SalesMainActivity) getActivity()).getM_currentCardItem();

        //m_currBillId = FtHandler.getLastBillId() + 1;

        //Top fields start
        paymentIDView = m_thisView.findViewById(R.id.paymentID);
        paymentIDView.setText("බිල් අංකය #" + String.valueOf(m_payManager.getNextPaymentIdforThisJob(current_card.getId())));


        customerNameView = m_thisView.findViewById(R.id.billCustomerName);


        if (current_card == null)
        {
            customerNameView.setText("Please select card");
            return m_thisView;
        }

        customerNameView.setText(current_card.getCustomerName());


        populatePaymentView();


        /** Getting fragment manager */
        //this is to identify which numpad at SalesMainActivity Level
        FragmentManager fm = getActivity().getSupportFragmentManager();
        numPadFragment = (NumPadFragment) this.getChildFragmentManager().findFragmentById(R.id.numPadFragmentForPayments);
        numPadFragment.setSourceId(Enums.NUMPADSOURCE.PAYMENTS.getValue());

        //set button behaviours
        setCurrentDateButtonBehavior();
        setChequeDateButtonBehavior();
        setRadioGroup();
        //setNextDateCheckBoxBehaviour();
        setNextCheckBehaviour();
        setSaveBillButtonBehavior();
        setPrintButtonBehavior();
        setResetButtonBehavior();
        setShowButtonBehavior();



        return m_thisView;
    }

    private void setRadioGroup()
    {
        chequeDateSetBtn.setVisibility(View.INVISIBLE);
        dateValueText.setVisibility(View.INVISIBLE);


        radioSexGroup = (RadioGroup) m_thisView.findViewById(R.id.radioSex);
        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb=(RadioButton)m_thisView.findViewById(checkedId);
                //Toast.makeText(getActivity(), rb.getText(), Toast.LENGTH_SHORT).show();

                switch(checkedId){
                    case R.id.radioCheque:
                        chequeDateSetBtn.setVisibility(View.VISIBLE);
                        dateValueText.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioCash:
                        chequeDateSetBtn.setVisibility(View.INVISIBLE);
                        dateValueText.setVisibility(View.INVISIBLE);
                        // do operations specific to this selection
                        break;
                }

            }
        });

        //radioSexGroup.getCheckedRadioButtonId() == R.id.radioCash
    }


    private void populatePaymentView()
    {


        cashValueText = m_thisView.findViewById(R.id.cashValueText);
        cashValueText.setInputType(0);
        cashValueText.setOnFocusChangeListener(this);
        cashValueText.requestFocus();

        dateValueText = m_thisView.findViewById(R.id.dateValueText);
        dateValueText.setInputType(0);
        //dateValueText.setOnFocusChangeListener(this);

    }
    private void setCurrentDateButtonBehavior()
    {
        //default date
        //set date
        Calendar custom = Calendar.getInstance();
        custom.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
        custom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
        custom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        m_currentDate = sdf.format(custom.getTime());


        // on button click
        currentDateSetBtn =(Button)m_thisView.findViewById(R.id.dateSetBtn);

        currentDateSetBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                Calendar c = Calendar.getInstance();
                                c.set(year, monthOfYear, dayOfMonth);
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
                                m_currentDate =  date;


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


    }


    private void setChequeDateButtonBehavior()
    {
        chequeDateSetBtn =(Button)m_thisView.findViewById(R.id.chequeDateSetBtn);

        chequeDateSetBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                Calendar c = Calendar.getInstance();
                                c.set(year, monthOfYear, dayOfMonth);
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
                                dateValueText.setText(date);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

    }


    private void setNextDateCheckBoxBehaviour()
    {
        nextDateBox = (CheckBox) m_thisView.findViewById(R.id.nextDate);

        nextDateBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( nextDateBox.isSelected() == true)
                {
                    chequeDateSetBtn.setVisibility(View.VISIBLE);
                    dateValueText.setVisibility(View.VISIBLE);
                }
                else {
                    chequeDateSetBtn.setVisibility(View.INVISIBLE);
                    dateValueText.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void setNextCheckBehaviour()
    {
        nextDateBox = (CheckBox) m_thisView.findViewById(R.id.nextDate);
        nextDateBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                   if( isChecked == true)
                   {
                       chequeDateSetBtn.setVisibility(View.VISIBLE);
                       dateValueText.setVisibility(View.VISIBLE);

                       cashValueText.setVisibility(View.INVISIBLE);
                       currentDateSetBtn.setVisibility(View.INVISIBLE);
                       radioSexGroup.setVisibility(View.INVISIBLE);
                   }
                   else {
                       chequeDateSetBtn.setVisibility(View.INVISIBLE);
                       dateValueText.setVisibility(View.INVISIBLE);

                       cashValueText.setVisibility(View.VISIBLE);
                       currentDateSetBtn.setVisibility(View.VISIBLE);
                       radioSexGroup.setVisibility(View.VISIBLE);
                   }

               }
           }
        );
    }



    private void setSaveBillButtonBehavior()
    {
        saveBtn = m_thisView.findViewById(R.id.button_save);
        saveBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {


                //check for whether this is special value
                if(nextDateBox.isChecked() == true)
                {
                    save();
                    return;
                }

                //check for empty values
                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCash)
                {
                    if(cashValueText.getText().toString().equals(""))
                    {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("Empty Cash value Detected")
                                .setMessage("Empty Cash Value Please fill Cash value")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }
                    else
                    {
                        //////////////////////////////////////////////////////////////////////////////////
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("Save")
                                .setMessage("මුදල  Rs"+cashValueText.getText().toString()+
                                        "\n තහවුරු කරන්න.")
                                .setPositiveButton("ඔව්", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        save();
                                    }
                                })
                                .setNegativeButton("නැහැ ", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                        /////////////////////////////////////////////////////////////////////////////////////
                    }
                }

                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCheque)
                {
                    if(cashValueText.getText().toString().equals("") && dateValueText.getText().toString().equals("")) {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("CASH or Cheque date not added")
                                .setMessage("CASH or Cheque date not added")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }

                    else { // when cash values are enteref
                        //////////////////////////////////////////////////////////////////////////////////
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("Save")
                                .setMessage("You are going to save Rs"+cashValueText.getText().toString()+"as cheque"+
                                        "\n Are you sure")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        save();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                       /////////////////////////////////////////////////////////////////////////////////////
                    }

                }
            }
        });

    }



    private void save()
    {
        createPayment();

        AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.PAYMENT_SAVE,null);
        m_parentAppEventListner.onEvent(event);

        manageBlinkEffect();
        cashValueText.setEnabled(false);


        ///Show print Button and current Bill button
        printBtn.setEnabled(true);

        //disable save button to avoid save same bill twice
        saveBtn.setEnabled(false);
        saveBtn.setBackgroundColor(Color.BLACK);
        //billBtn.setEnabled(false);

    }


    private void setShowButtonBehavior()
    {

        showBtn = m_thisView.findViewById(R.id.button_curr_bill);
        showBtn.setEnabled(true);
        showBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCash)
                {
                    if(cashValueText.getText().toString().equals(""))
                    {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("Empty Cash value Detected")
                                .setMessage("Empty Cash Value Please fill Cash value")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }
                    else
                    {
                       showBill();
                    }
                }

                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCheque)
                {
                    if(cashValueText.getText().toString().equals("") && dateValueText.getText().toString().equals("")) {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("CASH or Cheque date not added")
                                .setMessage("CASH or Cheque date not added")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }

                    else {
                        showBill();
                    }

                }
            }
        });

    }


    private void setPrintButtonBehavior()
    {
        printBtn = m_thisView.findViewById(R.id.button_print);
        printBtn.setEnabled(true);
        printBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCash)
                {
                    if(cashValueText.getText().toString().equals(""))
                    {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("Empty Cash value Detected")
                                .setMessage("Empty Cash Value Please fill Cash value")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }
                    else
                    {
                        print();
                    }
                }

                if(radioSexGroup.getCheckedRadioButtonId() == R.id.radioCheque)
                {
                    if(cashValueText.getText().toString().equals("") && dateValueText.getText().toString().equals("")) {
                        new AlertDialog.Builder(m_thisView.getContext())
                                .setTitle("CASH or Cheque date not added")
                                .setMessage("CASH or Cheque date not added")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }

                    else {
                        print();
                    }

                }
            }
        });
    }

    private void print()
    {
        PaymentBill paymentBill = new PaymentBill();
        paymentBill.setJobId(current_card.getId());
        paymentBill.setCustomerName(current_card.getCustomerName());
        paymentBill.setPaymentId(m_payManager.getNextPaymentIdforThisJob(current_card.getId()));
        paymentBill.setPaymentAmount(Double.parseDouble(cashValueText.getText().toString()));

        AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.PAYMENT_PRINT, paymentBill);
        m_parentAppEventListner.onEvent(event);

    }




    private void setResetButtonBehavior(){
        resetBtn = m_thisView.findViewById(R.id.button_reset);

        resetBtn.setOnClickListener(new Button.OnClickListener(){

                                        @Override
                                        public void onClick(View v) {

                                            cashValueText.setText("");

                                            //set date
                                            Calendar custom = Calendar.getInstance();
                                            custom.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
                                            custom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
                                            custom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                            m_currentDate = sdf.format(custom.getTime());

                                            dateValueText.setText(m_currentDate);


                                        }
                                    }

        );
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onEvent(AppEventMsg event) {

        switch (event.getEvent())
        {






        }
    }

    private void manageBlinkEffect() {
        paymentIDView.setText("Select New Card");
        ObjectAnimator anim = ObjectAnimator.ofInt(paymentIDView, "backgroundColor", Color.WHITE, Color.RED,
                Color.WHITE);
        anim.setDuration(1500);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v == cashValueText)
        {
            isCashPriceSelected = hasFocus;
        }
        else if (v == dateValueText)
        {
            isChequeSelected = hasFocus;
        }
    }

    public void setText(String str) {

        TextView inputText = null;

        if(isCashPriceSelected)
        {
            inputText = cashValueText;
        }
        else if(isChequeSelected)
        {
            inputText = dateValueText;
        }

        if(inputText != null) {

            if (str.equals("10")) {

                String strVal = inputText.getText().toString();

                if (strVal.length() != 0) {
                    strVal = strVal.substring(0, strVal.length() - 1);
                    // Now set this Text to your edit text
                    inputText.setText(strVal);
                }

            } else if (str.equals("11")) {


            } else if (str.equals("12")) {
                inputText.append(".");
            } else {
                inputText.append(str);
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void createPayment()
    {
        //create payment
/////////////////////////////////////////

        Payment payment = new Payment();

        payment.setJobId(current_card.getId());

        //if user just want to set next date only
        if(nextDateBox.isChecked() == true)
        {
            payment.setPaymentId(0);
            payment.setStatus(Enums.Status.NEXTDATE.getValue());
            payment.setNextDate(dateValueText.getText().toString());


        }
        else
        {
            payment.setPaymentId(m_payManager.getNextPaymentIdforThisJob(current_card.getId()));
            payment.setPaymentAmount(Double.parseDouble(cashValueText.getText().toString()));
            payment.setPanaltyAmount(0);

            payment.setStatus(Enums.Status.NEW.getValue());
        }



        String currUserId = String.valueOf(Users.instance(getContext()).getUserId(m_currUser));
        payment.setRefId(Users.instance(getContext()).getUserId(m_currUser));

        payment.setRefSeqId(m_payManager.getNextPaymentRefSeq(currUserId));


        //NORMAL OR PANALTY
        int pay_type_val= 1;
        payment.setPayType(pay_type_val);

        int pay_cash_cheque_val=Enums.PAYTYPES.CASH.getValue();
        if (radioSexGroup.getCheckedRadioButtonId() == R.id.radioCash)
        {
            pay_cash_cheque_val=Enums.PAYTYPES.CASH.getValue();
        }
        else {
            pay_cash_cheque_val=Enums.PAYTYPES.CHEQUE.getValue();
        }
        payment.setPayCashChequeType(pay_cash_cheque_val);


        //payment.setDate();

        //set date
        payment.setDate(m_currentDate);


        //send to DB
        m_payManager.insertPayment(payment);

        //send to sync handler

        BackendSynchronizeHandler handler = ((SalesMainActivity)getActivity()).getM_backendSynchronizeHandler();

        Message msg = handler.obtainMessage();

        //create and populate message object
        MessageStruct sendObj = new MessageStruct();
        sendObj.setType(MessageStruct.MessageType.PAYMENT_SYNCRONIZE);
        sendObj.setUserData(payment);
        msg.obj =  sendObj;// Some Arbitrary objec
        handler.sendMessage(msg);

    }


    public void showBill()
    {

        PaymentBill billObj = new PaymentBill();
        billObj.setJobId(current_card.getId());
        billObj.setCustomerName(current_card.getCustomerName());
        billObj.setPaymentId(m_payManager.getNextPaymentIdforThisJob(current_card.getId()));
        billObj.setPaymentAmount(Double.parseDouble(cashValueText.getText().toString()));


        BillPrinter billPrinter = BillPrinterFactory.createBillPrinter(getActivity(), SalesMainActivity.CLIENT_ID);
        String bill = billPrinter.getBillString(billObj);


        LayoutInflater layoutInflater
                = (LayoutInflater) getContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.bill_popup, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        ((EditText)popupWindow.getContentView().findViewById(R.id.billText)).setText(bill);


        Button btnDismiss = (Button) popupView.findViewById(R.id.okbtnCash);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Clear all editTexts
                popupWindow.dismiss();
            }
        });


        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

    }




}
