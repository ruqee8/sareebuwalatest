package com.journaldev.zebrasolar.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.db.DataBaseHelper;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdminFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdminFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdminFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private APIInterface m_apiIF;


    private View thisView;
    private Button clearBtn;
    private Button syncDataBtn;
    private Button lastJobBtn;


    private OnFragmentInteractionListener mListener;

    public AdminFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AdminFragment newInstance(String param1, String param2) {
        AdminFragment fragment = new AdminFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      thisView = inflater.inflate(R.layout.fragment_admin, container, false);

      m_apiIF = APIClient.getClient().create(APIInterface.class);

      clearBtn = thisView.findViewById(R.id.buttonClearTables);
      clearBtn.setOnClickListener(this);

      syncDataBtn = thisView.findViewById(R.id.buttonShowSequenceTable);
      syncDataBtn.setOnClickListener(this);

      lastJobBtn = thisView.findViewById(R.id.buttonLastJob);
      lastJobBtn.setOnClickListener(this);

      return thisView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v == clearBtn)
        {
            AlertDialog alert = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
            builder.setMessage("Warning !! This will erase all the app data ....!\n");
            builder.setCancelable(false);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    clearAndRecreateTables();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            alert = builder.create();
            alert.show();

        }

        if( v == syncDataBtn)
        {
            LayoutInflater layoutInflater
                    = (LayoutInflater) getContext()
                    .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.syncinfo_popup, null);
            final PopupWindow popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            ((TextView)popupWindow.getContentView().findViewById(R.id.syncDetailsTxt)).setMovementMethod(new ScrollingMovementMethod());

            SyncOperations syncOperations = new SyncOperations(getContext());
            ((TextView)popupWindow.getContentView().findViewById(R.id.syncDetailsTxt)).setText(
                    syncOperations.getSynctableData());


            Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
            btnDismiss.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Clear all editTexts
                    popupWindow.dismiss();
                }
            });


            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        }

        if( v == lastJobBtn)
        {
            JobManager jobManager = new JobManager(getContext());
            String jobId = jobManager.getLastJobIdForUser(Integer.toString(Users.getCurrentUserId()));

            AlertDialog alert = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
            builder.setMessage("Current Users Last Job is "+ jobId);
            builder.setCancelable(false);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            alert = builder.create();
            alert.show();
        }
    }


    private void clearAndRecreateTables()
    {
        DataBaseHelper.clearAllTables(DataBaseHelper.getInstance(getContext()).getWritableDatabase());
        DataBaseHelper.createAllTables(DataBaseHelper.getInstance(getContext()).getWritableDatabase());
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
