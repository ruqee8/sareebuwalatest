package com.journaldev.zebrasolar.fragments.stats;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.utils.AppStatsUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatsFragment extends Fragment {


    public StatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        ArrayAdapter<Object> stats = new ArrayAdapter<Object>(
                getActivity().getApplicationContext(),
                R.layout.stat_list_item,
                AppStatsUtils.getAppStats(getActivity().getApplicationContext()).toArray());

        final ListView statList = view.findViewById(R.id.stats_list_view);
        statList.setAdapter(stats);


        return view;
    }

}
