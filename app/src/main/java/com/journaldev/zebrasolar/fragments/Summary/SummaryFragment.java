package com.journaldev.zebrasolar.fragments.Summary;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.DataBaseHelper;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.fragments.ArrearsValidator;
import com.journaldev.zebrasolar.fragments.cards.CardItem;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Users;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SummaryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SummaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SummaryFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private APIInterface m_apiIF;


    private View thisView;
    private TextView collected;
    private TextView daily;
    private TextView weekly;
    private TextView monthly;
    private TextView total;

    //user name
    String m_user = null;
    Integer m_userId = null;

    PayManager m_payManager;
    JobManager m_jobManager;



    private OnFragmentInteractionListener mListener;

    public SummaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SummaryFragment newInstance(String param1, String param2) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      thisView = inflater.inflate(R.layout.fragment_summary, container, false);

      m_apiIF = APIClient.getClient().create(APIInterface.class);

      m_payManager = new PayManager(getContext());
      m_jobManager = new JobManager(getContext());

      populateUserData();

      collected = (TextView) thisView.findViewById(R.id.collectedVal);
      daily = (TextView) thisView.findViewById(R.id.daily);
      weekly = (TextView) thisView.findViewById(R.id.weekly);
      monthly = (TextView) thisView.findViewById(R.id.monthly);
      total = (TextView) thisView.findViewById(R.id.total);

      populateViews();

      return thisView;
    }

    private void populateViews()
    {
        double collectedVal = 0.0;

        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        List<Payment> paylist = m_payManager.getPaymentListForCollectorAndDate(m_userId,currentDate);

        for (Payment payment : paylist) {
            collectedVal = collectedVal + payment.getPaymentAmount();
        }

        collected.setText(Double.toString(collectedVal));


        double dailyVal = 0.0;
        double weeklyVal = 0.0;
        double monthlyVal = 0.0;

        double totalVal =0.0;

        List<Job> joblist =  m_jobManager.getJobListForCollectorId(m_userId);

        ArrearsValidator arrearsValidator = new ArrearsValidator(m_payManager);

        for (Job job : joblist) {
            arrearsValidator.setJob(job);

            if (job.getInstallmentType() == Enums.INSTALLMENTTYPE.DAILY.getValue())
            {
                dailyVal = dailyVal+arrearsValidator.getArrearsValForJob();
            }
            else if(job.getInstallmentType() == Enums.INSTALLMENTTYPE.WEEKLY.getValue())
            {
                weeklyVal = weeklyVal+arrearsValidator.getArrearsValForJob();
            }
            else {
                monthlyVal = monthlyVal+arrearsValidator.getArrearsValForJob();
            }
        }


        totalVal = dailyVal+weeklyVal+monthlyVal;

        daily.setText(Double.toString(dailyVal));
        weekly.setText(Double.toString(weeklyVal));
        monthly.setText(Double.toString(monthlyVal));

        total.setText(Double.toString(totalVal));

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //populate userRelatedData
    private void populateUserData()
    {

        m_user = getActivity().getIntent().getExtras().getString("userName");
        m_userId = Users.instance(getContext()).getUserId(m_user);
    }

}
