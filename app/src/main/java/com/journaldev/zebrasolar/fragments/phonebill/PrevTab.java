package com.journaldev.zebrasolar.fragments.phonebill;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.ReferenceDataManager;
import com.journaldev.zebrasolar.events.AppEventListener;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.fragments.cards.CardItem;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.PaymentAdapter;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PrevTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PrevTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrevTab extends Fragment implements AppEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;




    private Context m_context;
    private View m_thisView;


    private RecyclerView mRecyclerView;
    private PaymentAdapter mAdapter;
    CardItem current_card;

    private List<Payment> mPaymentList;

    //Reference Data - static data
    ReferenceDataManager m_referenceDataManager;
    PayManager m_payManager;


    //event listner to comunicate with PhoneBillFragment
    private AppEventListener m_parentAppEventListner;



    //Main
    SalesMainActivity salesMainActivity;

    int m_prevBillIndex;//this used for generate previous bills




    private OnFragmentInteractionListener mListener;

    public PrevTab() {

    }

    public void setM_context(Context m_context) {
        this.m_context = m_context;
    }

    public void setEventListner(AppEventListener appEventListener)
    {
        m_parentAppEventListner = appEventListener;
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentTab.
     */
    // TODO: Rename and change types and number of parameters
    public static PrevTab newInstance(String param1, String param2) {
        PrevTab fragment = new PrevTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //getting the recyclerview from xml

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        m_thisView = inflater.inflate(R.layout.fragment_phone_prev_tab, container, false);

        salesMainActivity = (SalesMainActivity) getActivity();
        current_card = ((SalesMainActivity) getActivity()).getM_currentCardItem();

        //db Related Classes initialization
        m_referenceDataManager = new ReferenceDataManager(m_context);
        m_payManager = new PayManager(m_context);

        populatePaymentList();



        return m_thisView;
    }

    private void populatePaymentList()
    {

        mRecyclerView = (RecyclerView) m_thisView.findViewById(R.id.idRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(m_context));


        if(current_card == null)
        {
            Toast.makeText(m_context, " Please select a JOB ..\n", Toast.LENGTH_SHORT).show();
            return;
        }

        mPaymentList = m_payManager.getPaymentList(current_card.getId());

        //set adapter to recyclerview
        mAdapter = new PaymentAdapter(mPaymentList,m_context);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


    }










    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onEvent(AppEventMsg event) {

        switch (event.getEvent()) {

            case PAYMENT_SAVE:
            {
                populatePaymentList();
                break;
            }

        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void showPrevBills()
    {



    }






}
