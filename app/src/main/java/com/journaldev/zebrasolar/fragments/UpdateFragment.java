package com.journaldev.zebrasolar.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.R;

import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.ft.FtHandler;

import com.journaldev.zebrasolar.backend.common.Commons;
import com.journaldev.zebrasolar.backend.common.LoginListener;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.ReferenceDataManager;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.model.Areas;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Products;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.pojo.area.Area;
import com.journaldev.zebrasolar.pojo.ft.FTInfo;
import com.journaldev.zebrasolar.pojo.job.JobOrPaymentSyncRequest;
import com.journaldev.zebrasolar.pojo.job.JobSyncResponse;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;
import com.journaldev.zebrasolar.pojo.product.Product;
import com.journaldev.zebrasolar.pojo.user.User;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.utils.ForwardBalance;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFragment extends Fragment {

    View m_thisView = null;
    private final static String TAG = "UpdateFragment";

    private ProgressDialog m_progressDialog;
    private StringBuilder m_progressMessage;

    private APIInterface m_apiIF;
    FragmentActivity m_context = null;

    String currentUserId = null;

    ReferenceDataManager m_referenceDataManager;
    FtHandler m_ftHandler;
    BackendSynchronizeHandler m_backendSyncHandler;

    ForwardBalance m_fwbalance;


    JobManager m_jobManager;
    PayManager m_payManager;
    SyncOperations m_syncOperations;

    public UpdateFragment(BackendSynchronizeHandler backendSynchronizeHandler) {
        m_progressMessage = new StringBuilder();
        currentUserId = Integer.toString(Users.getCurrentUserId());
        m_backendSyncHandler= backendSynchronizeHandler;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        m_thisView =  inflater.inflate(R.layout.fragment_update, container, false);
        m_context = (SalesMainActivity) getActivity();

        m_apiIF = APIClient.getClient().create(APIInterface.class);

        m_fwbalance = new ForwardBalance(m_context);

        m_referenceDataManager = new ReferenceDataManager(getActivity());
        m_jobManager = new JobManager(m_context);
        m_payManager = new PayManager(m_context);
        m_syncOperations = new SyncOperations(m_context);

        m_ftHandler = new FtHandler(m_backendSyncHandler,getActivity().getApplicationContext());

        //zebra update users
        final Button updateUsersButton = m_thisView.findViewById(R.id.button_update_users);
        updateUsersButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateUsersButtonClick();
            }
        });

        //zebra update products

        //zebra update users
        final Button updateProductsButton = m_thisView.findViewById(R.id.button_update_products);
        updateProductsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateProductsButtonClick();
            }
        });

        //zebra update users
        final Button updateAreasButton = m_thisView.findViewById(R.id.button_update_areas);
        updateAreasButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateAreasButtonClick();
            }
        });



        final Button updateAllButton = m_thisView.findViewById(R.id.button_update);
        updateAllButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateAllButtonClick();
            }
        });


        final Button updateJobsButton = m_thisView.findViewById(R.id.button_update_jobs);
        updateJobsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateJobsButtonClick();
            }
        });

        final Button updatePaymentsButton = m_thisView.findViewById(R.id.button_update_payments);
        updatePaymentsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdatePaymentsButtonClick();
            }
        });

        final Button clearSyncButton = m_thisView.findViewById(R.id.button_clear);
        clearSyncButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_referenceDataManager.clearSyncTable();
                populateInitialSyncTable();
            }
        });

        return m_thisView;
    }

    //zebra

    //common login happens here
    private void onUpdateAllButtonClick()
    {
        Log.d(TAG, "update()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updateUsersForAllUpdate(); //update FT info lie in this method
                //onUpdateComplete();  -- moved to end of the flow -- update products
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void onUpdateUsersButtonClick()
    {
        Log.d(TAG, "update users()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating Users In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updateUsers();
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void updateUsers()
    {
        m_referenceDataManager.clearUsersTable();

        Call<List<User>> call = m_apiIF.getUsers();
        call.request().url();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> userList = response.body();
                for (User user : userList) {
                    m_referenceDataManager.insertUser(user);
                }
                //m_referenceDataManager.endDynamicUpdate();
                Users.update();
                Users.instance(getContext());
                currentUserId = Integer.toString(Users.getCurrentUserId());

                populateInitialSyncTable();
                m_progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                call.cancel();
                onUpdateFailed();
            }
        });
    }

    private void updateUsersForAllUpdate()
    {
        m_referenceDataManager.clearUsersTable();

        Call<List<User>> call = m_apiIF.getUsers();
        call.request().url();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> userList = response.body();
                for (User user : userList) {
                    m_referenceDataManager.insertUser(user);
                }
                //m_referenceDataManager.endDynamicUpdate();
                Users.update();

                Users.instance(getContext());
                currentUserId = Integer.toString(Users.getCurrentUserId());

                populateInitialSyncTable();
                updateFTInfo();

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                call.cancel();
                onUpdateFailed();
            }
        });

    }

    private void populateInitialSyncTable()
    {
        Log.d(TAG, "populate Sync Table()");
        List<String> users = Users.instance(m_context).getUserList();
        for (String user : users) {
            //System.out.println(country);
            if( m_referenceDataManager.checkSyncValueExist(user, Enums.TableType.JOB) == false)
            {
                Log.d(TAG, "update jobs()");
                m_referenceDataManager.setInitialSyncData(user, Enums.TableType.JOB);
                //insert here
            }
            if( m_referenceDataManager.checkSyncValueExist(user,Enums.TableType.PAYMENT) == false)
            {
                Log.d(TAG, "update Payments()");
                m_referenceDataManager.setInitialSyncData(user, Enums.TableType.PAYMENT);
                //insert here
            }
        }

    }


    private void onUpdateProductsButtonClick()
    {
        Log.d(TAG, "update products()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating products In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updateProducts();
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });



    }

    private void updateProducts()
    {
        m_referenceDataManager.clearProductsTable();

        Call<List<Product>> call = m_apiIF.getProducts();
        call.request().url();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                List<Product> productList = response.body();
                for (Product product : productList) {
                    m_referenceDataManager.insertProduct(product);
                }

                //m_referenceDataManager.endDynamicUpdate();
                Products.update();
                m_progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                call.cancel();

                onUpdateFailed();
            }
        });

    }

    private void updateProductsForAllUpdate()
    {
        m_referenceDataManager.clearProductsTable();

        Call<List<Product>> call = m_apiIF.getProducts();
        call.request().url();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                List<Product> productList = response.body();
                for (Product product : productList) {
                    m_referenceDataManager.insertProduct(product);
                }

                //m_referenceDataManager.endDynamicUpdate();
                Products.update();
                onUpdateComplete();
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                call.cancel();

                onUpdateFailed();
            }
        });

    }

    private void onUpdateFailed()
    {
        m_ftHandler.init(getContext(),currentUserId);
        m_referenceDataManager.endDynamicUpdate();
        m_progressDialog.dismiss();
        Toast.makeText(getActivity(), "Error occurred while updating. Please check network connection", Toast.LENGTH_SHORT).show();
    }

    private void onUpdateComplete()
    {
        m_ftHandler.init(getContext(),currentUserId);
        m_referenceDataManager.endDynamicUpdate();
        m_progressMessage.append("\n[100%] Update Completed");
        m_progressDialog.setMessage(m_progressMessage.toString());
        final Handler handler = new Handler();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        m_progressDialog.dismiss();
                    }
                });
            }
        }, 2000);


    }

    private void updateFTInfo(){

        List<JobOrPaymentSyncRequest> usersForJobs = m_syncOperations.getUsersAndRefSequences(Enums.TableType.JOB);


        //update jobs
        Call<List<JobSyncResponse>> call = m_apiIF.getNotSyncedJobs(usersForJobs);
        call.request().url();
        call.enqueue(new Callback<List<JobSyncResponse>>() {

            @Override
            public void onResponse(Call<List<JobSyncResponse>> call, Response<List<JobSyncResponse>> response) {

                List<JobSyncResponse> jobsList = response.body();
                if((jobsList!=null) && (jobsList.size() > 0))
                {
                    for (JobSyncResponse jobres : jobsList) {
                        Job job = Job.getJob(jobres);

                        if(job.getStatus() == Enums.Status.NEW.getValue()) {
                            m_jobManager.insertJob(job);
                        }
                        else if (job.getStatus() == Enums.Status.NEXTDATE.getValue()) {
                            m_jobManager.insertJob(job);
                        }
                        else if(job.getStatus() == Enums.Status.DELETED.getValue())
                        {
                            deleteJobData(job);
                        }
                        else if(job.getStatus() == Enums.Status.COMPLETED.getValue())
                        {
                            completeJobData(job);
                        }
                        else if(job.getStatus() == Enums.Status.UPDATED.getValue())
                        {
                            updateJobData(job);
                        }
                        m_syncOperations.checkpointLastSyncedTxnID(job.getRefSeqId(), job.getRefId(), Enums.TableType.JOB);
                    }

                }

                updatePayments();

            }

            @Override
            public void onFailure(Call<List<JobSyncResponse>> call, Throwable t) {
                call.cancel();
            }
        });




    }

    private void updatePayments()
    {
        //update payments
        List<JobOrPaymentSyncRequest> usersForPayments = m_syncOperations.getUsersAndRefSequences(Enums.TableType.PAYMENT);

        Call<List<PaymentSyncResponse>> call2 = m_apiIF.getNotSyncedPayments(usersForPayments);
        call2.enqueue(new Callback<List<PaymentSyncResponse>>() {

            @Override
            public void onResponse(Call<List<PaymentSyncResponse>> call, Response<List<PaymentSyncResponse>> response) {
                List<PaymentSyncResponse> paymentList = response.body();

                if(paymentList.size() > 0)
                {
                    for (PaymentSyncResponse payres : paymentList) {
                        Payment pay = Payment.getPayment(payres);

                        if (m_jobManager.isValidJobID(pay.getJobId())) { //check for a valid m_job

                            if (pay.getStatus() == Enums.Status.NEW.getValue() ) {
                                m_payManager.insertPayment(pay);
                            }
                            else if (pay.getStatus() == Enums.Status.NEXTDATE.getValue()) {
                                m_payManager.insertPayment(pay);
                            }
                            else if (pay.getStatus() == Enums.Status.DELETED.getValue()) {
                                deletePaymentData(pay);
                            } else if (pay.getStatus() == Enums.Status.UPDATED.getValue()) {
                                updatePaymentData(pay);
                            }
                        }
                        m_syncOperations.checkpointLastSyncedTxnID(pay.getRefSeqId(),pay.getRefId(), Enums.TableType.PAYMENT);

                    }
                }

                populateCurrentUsersFtInfo();

            }

            @Override
            public void onFailure(Call<List<PaymentSyncResponse>> call, Throwable t) {
                call.cancel();

            }
        });


    }

    private void populateCurrentUsersFtInfo()
    {

        Call<FTInfo> call2 = m_apiIF.getFTInfo(Users.getCurrentUserId());
        call2.enqueue(new Callback<FTInfo>() {

            @Override
            public void onResponse(Call<FTInfo> call, Response<FTInfo> response) {
                FTInfo ftInfo= response.body();

                m_syncOperations.checkpointLastSyncedTxnID(ftInfo.getJobSeq(),Users.getCurrentUserId(), Enums.TableType.JOB);
                m_syncOperations.checkpointLastSyncedTxnID(ftInfo.getPaySeq(),Users.getCurrentUserId(), Enums.TableType.PAYMENT);

                updateAreasForAllUpdate();

            }

            @Override
            public void onFailure(Call<FTInfo> call, Throwable t) {
                call.cancel();

            }
        });

    }

    private void deleteJobData(Job job)
    {
        m_jobManager.deleteJob(job.getJobId());
        m_payManager.deletePaymentsForJob(job);
        //m_fwbalance.removeForwardBalancesForJob(m_job.getJobId());
    }

    private void completeJobData(Job job)
    {
        m_jobManager.deleteJob(job.getJobId());
        m_payManager.deletePaymentsForJob(job);
        //m_fwbalance.removeForwardBalancesForJob(m_job.getJobId());
    }


    private void deletePaymentData(Payment pay)
    {
        m_payManager.deletePayment(pay);
        //populateJobAgain(pay.getJobId());
    }

    private void updateJobData(Job job)
    {
        m_jobManager.updateJob(job);
        //populateJobAgain(m_job.getJobId());
    }
    private void updatePaymentData(Payment payment)
    {
        m_payManager.updatePayment(payment);
        //populateJobAgain(payment.getJobId());
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////          AREAS                   //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void onUpdateAreasButtonClick()
    {
        Log.d(TAG, "update areas()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating areas In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updateAreas();
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                m_ftHandler.init(getContext(),currentUserId);
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });



    }

    private void updateAreas()
    {
        m_referenceDataManager.clearAreaTable();

        Call<List<Area>> call = m_apiIF.getAreas();
        call.request().url();
        call.enqueue(new Callback<List<Area>>() {
            @Override
            public void onResponse(Call<List<Area>> call, Response<List<Area>> response) {
                List<Area> areaList = response.body();
                for (Area area : areaList) {
                    m_referenceDataManager.insertArea(area);
                }

                //m_referenceDataManager.endDynamicUpdate();
                Areas.update();
                m_progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Area>> call, Throwable t) {
                call.cancel();

                onUpdateFailed();
            }
        });

    }

    private void updateAreasForAllUpdate()
    {
        m_referenceDataManager.clearAreaTable();

        Call<List<Area>> call = m_apiIF.getAreas();
        call.request().url();
        call.enqueue(new Callback<List<Area>>() {
            @Override
            public void onResponse(Call<List<Area>> call, Response<List<Area>> response) {
                List<Area> areaList = response.body();
                for (Area area : areaList) {
                    m_referenceDataManager.insertArea(area);
                }

                //m_referenceDataManager.endDynamicUpdate();
                Areas.update();

                updateProductsForAllUpdate();

            }

            @Override
            public void onFailure(Call<List<Area>> call, Throwable t) {
                call.cancel();

                onUpdateFailed();
            }
        });

    }


    //common login happens here
    private void onUpdateJobsButtonClick()
    {
        Log.d(TAG, "update jobs()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updateJobsInfo();
                onUpdateComplete();
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateJobsInfo(){

        List<JobOrPaymentSyncRequest> usersForJobs = m_syncOperations.getUsersAndRefSequences(Enums.TableType.JOB);


        //update jobs
        Call<List<JobSyncResponse>> call = m_apiIF.getNotSyncedJobs(usersForJobs);
        call.request().url();
        call.enqueue(new Callback<List<JobSyncResponse>>() {

            @Override
            public void onResponse(Call<List<JobSyncResponse>> call, Response<List<JobSyncResponse>> response) {

                List<JobSyncResponse> jobsList = response.body();
                if((jobsList!=null) && (jobsList.size() > 0))
                {
                    for (JobSyncResponse jobres : jobsList) {
                        Job job = Job.getJob(jobres);

                        if(job.getStatus() == Enums.Status.NEW.getValue()) {
                            m_jobManager.insertJob(job);
                        }
                        else if(job.getStatus() == Enums.Status.DELETED.getValue())
                        {
                            deleteJobData(job);
                        }
                        else if(job.getStatus() == Enums.Status.COMPLETED.getValue())
                        {
                            completeJobData(job);
                        }
                        else if(job.getStatus() == Enums.Status.UPDATED.getValue())
                        {
                            updateJobData(job);
                        }
                        m_syncOperations.checkpointLastSyncedTxnID(job.getRefSeqId(), job.getRefId(), Enums.TableType.JOB);
                    }

                }

            }

            @Override
            public void onFailure(Call<List<JobSyncResponse>> call, Throwable t) {

                call.cancel();
            }
        });

    }

    //common login happens here
    private void onUpdatePaymentsButtonClick()
    {

        Log.d(TAG, "update Payments()");

        m_progressDialog = new ProgressDialog(getActivity());
        m_progressMessage.setLength(0);
        m_progressMessage.append("Updating In Progress...");
        m_progressDialog.setMessage(m_progressMessage.toString());
        m_progressDialog.show();

        Commons.login(getActivity(),new LoginListener() {
            @Override
            public void onLoggedIn() {
                updatePaymentsInfo();
                onUpdateComplete();
            }

            @Override
            public void onLogingFailed() {
                m_progressDialog.dismiss();
                Toast.makeText(getActivity(), "Cannot login to server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updatePaymentsInfo() {

        //update payments
        List<JobOrPaymentSyncRequest> usersForPayments = m_syncOperations.getUsersAndRefSequences(Enums.TableType.PAYMENT);

        Call<List<PaymentSyncResponse>> call2 = m_apiIF.getNotSyncedPayments(usersForPayments);
        call2.enqueue(new Callback<List<PaymentSyncResponse>>() {

            @Override
            public void onResponse(Call<List<PaymentSyncResponse>> call, Response<List<PaymentSyncResponse>> response) {
                List<PaymentSyncResponse> paymentList = response.body();

                if(paymentList.size() > 0)
                {
                    for (PaymentSyncResponse payres : paymentList) {
                        Payment pay = Payment.getPayment(payres);

                        if (m_jobManager.isValidJobID(pay.getJobId())) { //check for a valid m_job

                            if (pay.getStatus() == Enums.Status.NEW.getValue()) {
                                m_payManager.insertPayment(pay);
                            } else if (pay.getStatus() == Enums.Status.DELETED.getValue()) {
                                deletePaymentData(pay);
                            } else if (pay.getStatus() == Enums.Status.UPDATED.getValue()) {
                                updatePaymentData(pay);
                            }
                        }
                        m_syncOperations.checkpointLastSyncedTxnID(pay.getRefSeqId(),pay.getRefId(), Enums.TableType.PAYMENT);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<PaymentSyncResponse>> call, Throwable t) {
                call.cancel();

            }
        });

    }

}
