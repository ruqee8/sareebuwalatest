package com.journaldev.zebrasolar.fragments.cards;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.ReferenceDataManager;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventsDictionary;
import com.journaldev.zebrasolar.fragments.ArrearsValidator;
import com.journaldev.zebrasolar.model.Collector;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CardsFragment extends Fragment implements View.OnClickListener {


    View m_thisView = null;

    //Search box
    private SearchView search;
    private Button arrearsBtn;
    private RecyclerView recyclerlist;
    private MaterialSpinner collectorSpinner;
    private AdapterView.OnItemClickListener onItemClickListenerObj;

    private CardAdapter cardRecyclerAdapter;
    ArrayList<CardItem> cardItemsList;

    FragmentActivity m_context = null;


    private int m_currentCollectorId;

    //DB Related Classes
    //Reference Data - static data
    ReferenceDataManager m_referenceDataManager;
    JobManager m_jobManager;
    PayManager m_payManager;

    //user name
    String m_user = null;
    Integer m_userId = null;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        //load fragment and get base activity
        m_thisView = inflater.inflate(R.layout.fragment_cards, container, false);
        m_context = (SalesMainActivity) getActivity();

        search = (SearchView) m_thisView.findViewById(R.id.search);
        arrearsBtn = (Button) m_thisView.findViewById(R.id.btnArrears);
        recyclerlist = (RecyclerView) m_thisView.findViewById(R.id.recyclerlist);
        collectorSpinner = (MaterialSpinner)m_thisView.findViewById(R.id.item_collector);

        //db Related Classes initialization
        m_referenceDataManager = new ReferenceDataManager(m_context);
        m_jobManager = new JobManager(m_context);
        m_payManager = new PayManager(m_context);

        populateUserData();
        populateCardsRecyclerView();
        configureSearchView();

        populateCollectorSpinner();
        selectSpinnerPreviousPosition();
        setArrearsBtnBehavior();




        // Inflate the layout for this fragment
        return m_thisView;
    }


    //populate userRelatedData
    private void populateUserData()
    {
        m_user = getActivity().getIntent().getExtras().getString("userName");
        m_userId = Users.instance(m_context).getUserId(m_user);
    }


    private void setArrearsBtnBehavior()
    {
        arrearsBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateCardsRecyclerViewForArrears();
            }
        });

    }


    private void selectSpinnerPreviousPosition()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(m_context);
        int position = sharedPref.getInt("position",0);
        collectorSpinner.setSelectedIndex(position);

        m_currentCollectorId = sharedPref.getInt("collectorId",0);


        if (m_currentCollectorId !=0) {
            populateCardsRecyclerView();
        }



        //m_currentCollectorId = ((Collector) item).id;
        //populateCardsRecyclerView();
    }


    private void populateCardsRecyclerView()
    {

        cardItemsList = new ArrayList<CardItem>();

        if(m_currentCollectorId == -1)//default item
        {
            cardItemsList = new ArrayList<CardItem>();
        }
        else if (m_currentCollectorId == -2)//all customers
        {
            List<Job> joblist =  m_jobManager.getJobList();
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                cardItemsList.add(a);
            }


        }
        else
        {
           List<Job> joblist =  m_jobManager.getJobListForCollectorId(m_currentCollectorId);
           cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                CardItem a = new CardItem(job.getJobId(),job.getCustomerName(),job.getPhone());
                cardItemsList.add(a);
            }



        }

        ////////create recycler view for customers

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlist.setLayoutManager(layoutManager);
        cardRecyclerAdapter = new CardAdapter(cardItemsList,getContext());
        recyclerlist.addItemDecoration(new DividerItemDecoration(recyclerlist.getContext(), layoutManager.getOrientation()));
        recyclerlist.setAdapter(cardRecyclerAdapter);


        //populating onItemClick
        //create onitemclick listner for list view


        //populating onItemClick
        //create onitemclick listner for list view


        cardRecyclerAdapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = recyclerlist.indexOfChild(v);
                TextView txtjobid = v.findViewById(R.id.text_view3);
                String cardId = txtjobid.getText().toString();
                CardItem selectedItem = findCardItem(cardId);

                //CardItem selectedItem = (CardItem) cardItemsList.get(pos);

                //Toast.makeText(getActivity(), Integer.toString(pos), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), selectedItem.getId()
                //+ " " +selectedItem.getCustomerName(), Toast.LENGTH_SHORT).show();



                ((SalesMainActivity) getActivity()).setM_currentCardItem(selectedItem);
                ((SalesMainActivity) getActivity()).onCardSelected(selectedItem);
            }
        });


    }


    private void populateCardsRecyclerViewForArrears()
    {

        ArrearsValidator arrearsValidator = new ArrearsValidator(m_payManager);
        cardItemsList = new ArrayList<CardItem>();

        if(m_currentCollectorId == -1)//default item
        {
            cardItemsList = new ArrayList<CardItem>();
        }
        else if (m_currentCollectorId == -2)//all customers
        {
            List<Job> joblist =  m_jobManager.getJobList();
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {
                arrearsValidator.setJob(job);

                if(arrearsValidator.isArrearsJob()) {

                    CardItem a = new CardItem(job.getJobId(), job.getCustomerName(), job.getCustomerId());
                    cardItemsList.add(a);
                }
            }


        }
        else
        {
            List<Job> joblist =  m_jobManager.getJobListForCollectorId(m_currentCollectorId);
            cardItemsList = new ArrayList<CardItem>();


            for (Job job : joblist) {

                //check set nextdate is greater than today
                if(isNextDateGreaterThanToday(job)) continue;



                arrearsValidator.setJob(job);

                if(arrearsValidator.isArrearsJob()) {
                    CardItem a = new CardItem(job.getJobId(), job.getCustomerName(), job.getPhone());
                    cardItemsList.add(a);
                }
            }



        }

        ////////create recycler view for customers

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerlist.setLayoutManager(layoutManager);
        cardRecyclerAdapter = new CardAdapter(cardItemsList,getContext());
        recyclerlist.addItemDecoration(new DividerItemDecoration(recyclerlist.getContext(), layoutManager.getOrientation()));
        recyclerlist.setAdapter(cardRecyclerAdapter);


        //populating onItemClick
        //create onitemclick listner for list view


        cardRecyclerAdapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = recyclerlist.indexOfChild(v);
                TextView txtjobid = v.findViewById(R.id.text_view3);
                String cardId = txtjobid.getText().toString();
                CardItem selectedItem = findCardItem(cardId);

                //CardItem selectedItem = (CardItem) cardItemsList.get(pos);

                //Toast.makeText(getActivity(), Integer.toString(pos), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), selectedItem.getId()
                        //+ " " +selectedItem.getCustomerName(), Toast.LENGTH_SHORT).show();



                ((SalesMainActivity) getActivity()).setM_currentCardItem(selectedItem);
                ((SalesMainActivity) getActivity()).onCardSelected(selectedItem);
            }
        });


    }

    CardItem findCardItem(String id)
    {
        for (CardItem item : cardItemsList)
        {
            if(item.getId().equalsIgnoreCase(id))
                return item;
        }

        return null;

    }

    private void populateExampleItems()
    {
        /*
        CardItem a = new CardItem(1,"AAAA","1234");
        cardItemsList.add(a);

        CardItem b = new CardItem(2,"BBBB","5678");
        cardItemsList.add(b);

        CardItem c = new CardItem(3,"CCCC","58");
        cardItemsList.add(c);


        CardItem j = new CardItem(4,"DDDD","5s8");
        cardItemsList.add(j);


        CardItem k = new CardItem(5,"EEEE","5278");
        cardItemsList.add(k);


        CardItem f = new CardItem(6,"FFFF","5648");
        cardItemsList.add(f);


        CardItem i = new CardItem(7,"IIIIII","5278");
        cardItemsList.add(i);
        */
    }

    private void populateExampleItems2()
    {
        /*
        CardItem a = new CardItem(8,"AAAA1","1234");
        cardItemsList.add(a);

        CardItem b = new CardItem(9,"BBBB1","5678");
        cardItemsList.add(b);

        CardItem c = new CardItem(10,"CCCC1","58");
        cardItemsList.add(c);


        CardItem j = new CardItem(11,"DDDD1","5s8");
        cardItemsList.add(j);


        CardItem k = new CardItem(12,"EEEE1","5278");
        cardItemsList.add(k);


        CardItem f = new CardItem(13,"FFFF1","5648");
        cardItemsList.add(f);


        CardItem i = new CardItem(14,"IIIIII1","5278");
        cardItemsList.add(i);
        */
    }


    private void configureSearchView()
    {
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String searchText) {
                cardRecyclerAdapter.getFilter().filter(searchText);
                return false;
            }
        });
    }


    private void populateCollectorSpinner()
    {
        List<Collector> collectorList = generateCollectorList();

        //Creating Route Selector
        ArrayAdapter<Collector> collectors = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                collectorList);
        collectorSpinner.setAdapter(collectors);

        //create  onItem Slectetor
        final MaterialSpinner.OnItemSelectedListener  OnItemSelectedListenerObj = new MaterialSpinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                Log.w("Collector Spinner","Collector -ID : " + ((Collector)item).id);
                m_currentCollectorId = ((Collector) item).id;
                populateCardsRecyclerView();
                saveSpinnerPosition(position);

            }

        };


        //OnClick in Routes should populate customer list
        collectorSpinner.setOnItemSelectedListener(OnItemSelectedListenerObj);
    }


    private List<Collector> generateCollectorList()
    {
        List<Collector> collectorList = new ArrayList<Collector>();


        collectorList.add(new Collector("Please Select Collector",-1));
        collectorList.add(new Collector("All",-2));


        Map<Integer,String> collectorsMap = Users.instance(m_context).getMapCollectorUsers();

        if(collectorsMap == null) return collectorList;

        for (Map.Entry<Integer,String> entry : collectorsMap.entrySet())
        {
            //System.out.println(entry.getKey() + "/" + entry.getValue());
            collectorList.add(new Collector(entry.getValue(),entry.getKey()));
        }



        /*
        collectorList.add(new Collector("Rukshan",1));
        collectorList.add(new Collector("Dinushan",2));
        */

        return collectorList;
    }

    /*////////////////////////////////////////////////////////////////////
     *  check user name and password in shared preferences
     *////////////////////////////////////////////////////////////////////
    private void saveSpinnerPosition(int position)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("position",position);
        editor.putInt("collectorId",m_currentCollectorId);
        editor.commit();

    }


    @Override
    public void onResume() {
        super.onResume();



    }

    @Override
    public void onClick(View v) {

    }


    public boolean isNextDateGreaterThanToday(Job job)
    {

        String nextDate = m_payManager.getMaxNextDate(job.getJobId());

        if (nextDate.equals("") == false) {


            //set current date
            Calendar custom = Calendar.getInstance();
            custom.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
            custom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
            custom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

            Date currentDate = custom.getTime();

            Date nxtDate = null;

            //get Next Date
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                nxtDate = format.parse(nextDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (nxtDate.compareTo(currentDate) > 0) {

                return false;
            }

            else {
                return true;
            }

        }

        return false;


    }
}
