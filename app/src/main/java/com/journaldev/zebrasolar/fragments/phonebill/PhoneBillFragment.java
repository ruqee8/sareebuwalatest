package com.journaldev.zebrasolar.fragments.phonebill;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.events.AppEventListener;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventsDictionary;
import com.journaldev.zebrasolar.ft.FtHandler;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinter;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinterFactory;
import com.journaldev.zebrasolar.utils.billPrinter.PaymentBill;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PhoneBillFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhoneBillFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhoneBillFragment extends Fragment implements AppEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    View m_thisView = null;

    private ProgressDialog m_progressDialog;

    //Activity
    FragmentActivity salesMainActivity = null;
    Context m_context;

    private BackendSynchronizeHandler m_backendSynchronizeHandler;
    private FtHandler m_ftHandler;


    TabLayout tabLayout;
    CustomViewPager viewPager;
    private CardTab m_cardTab;
    private PaymentTab m_paymentTab;
    private PrevTab m_prevTab;

    String m_currUser = null;

    boolean isSaved = false;




    //checker to save bill
    public boolean iscurrentPaymentSaved = false;
    AppEventListener m_appEventListener;



    private OnFragmentInteractionListener mListener;


    //event listner to comunicate with CardTab
    private AppEventListener m_cardTabAppEventListner;
    private AppEventListener m_listTabAppEventListner;
    private AppEventListener m_prevTabAppEventListner;

    public void setM_currUser(String user)
    {
        m_currUser = user;
    }

    public PhoneBillFragment() {
        // Required empty public constructor
    }

    public CardTab getBillTabFragment()
    {
        return m_cardTab;
    }

    public PaymentTab getListTabFragment()
    {
        return m_paymentTab;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhoneBillFragment newInstance(String param1, String param2) {
        PhoneBillFragment fragment = new PhoneBillFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        m_thisView = inflater.inflate(R.layout.fragment_phone_bill, container, false);
        salesMainActivity = (SalesMainActivity) getActivity();
        m_context = salesMainActivity.getApplicationContext();


        createTabView();



        return m_thisView;
    }


    private void createTabView()
    {
        ////////////Set tab layout start

        tabLayout = m_thisView.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("කාඩ්"));//Card
        tabLayout.addTab(tabLayout.newTab().setText("ගෙවීම්"));//Payments
        tabLayout.addTab(tabLayout.newTab().setText("පෙරගෙවීම්"));//Prev
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);




        viewPager = (CustomViewPager) m_thisView.findViewById(R.id.viewPager);
        final PhoneBillPagerAdapter pagerAdapter = new PhoneBillPagerAdapter(this.salesMainActivity.getSupportFragmentManager(),tabLayout.getTabCount());


        m_cardTab = new CardTab();
        m_cardTab.setEventListner(this);
        m_cardTab.setM_currUser(m_currUser);
        m_cardTabAppEventListner = m_cardTab;


        m_paymentTab = new PaymentTab();
        m_paymentTab.setEventListner(this);
        m_listTabAppEventListner = m_paymentTab;
        m_paymentTab.setM_currUser(m_currUser);

        m_prevTab = new PrevTab();
        m_prevTab.setM_context(m_context);
        m_prevTab.setEventListner(this);
        m_prevTabAppEventListner = m_prevTab;


        pagerAdapter.setCardTab(m_cardTab);
        pagerAdapter.setPaymentTab(m_paymentTab);
        pagerAdapter.setPrevTab(m_prevTab);


        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(pagerAdapter);

        // Attach the page change listener inside the activity
        viewPager.addOnPageChangeListener(getOnPageChangeListener());


        //Set Tab Layout end


        ////initially currentbill is in notsaved mode
        iscurrentPaymentSaved = false;

        //seteventListenr
        m_appEventListener = (SalesMainActivity)getActivity();

    }



    public BackendSynchronizeHandler getM_backendSynchronizeHandler() {
        return m_backendSynchronizeHandler;
    }

    public void setM_backendSynchronizeHandler(BackendSynchronizeHandler m_backendSynchronizeHandler) {
        this.m_backendSynchronizeHandler = m_backendSynchronizeHandler;
    }

    public FtHandler getM_ftHandler() {
        return m_ftHandler;
    }

    public void setM_ftHandler(FtHandler m_ftHandler) {

        this.m_ftHandler = m_ftHandler;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onEvent(AppEventMsg event) {


        switch (event.getEvent())
        {
            //SAREE
            case GOTO_PAYMENT:
            {
                //scroll to payment tab on "payment button" click in the Card Tab
                viewPager.setCurrentItem(1);
                break;
            }

            case PAYMENT_SAVE:
            {
                m_prevTabAppEventListner.onEvent(event);
                m_cardTabAppEventListner.onEvent(event);
                break;
            }

            case PAYMENT_PRINT:
            {
                PaymentBill paymentBill = (PaymentBill) event.getUserData();
                print(paymentBill);
                break;

            }
            default:
            {
                break;
            }

        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void print(PaymentBill paymentBill)
    {
        m_progressDialog = new ProgressDialog(salesMainActivity);
        m_progressDialog.setMessage("Printing bill.. Please wait..");
        m_progressDialog.show();

        boolean isPrint = true;
        boolean isPrintSuccess = false;
        if (isPrint) {
            BillPrinter billPrinter = BillPrinterFactory.createBillPrinter(getActivity(), SalesMainActivity.CLIENT_ID);

            isPrintSuccess = billPrinter.doPrint(paymentBill);
        }

        m_progressDialog.dismiss();
        if (isPrintSuccess) {
            Toast.makeText(getActivity(), "Bill is printing ...", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getActivity(), "Bill print failed. Please connect bluetooth printer", Toast.LENGTH_LONG).show();
        }

    }


   /* private ViewPager.OnTouchListener getOnTouchListener()
    {
        return new ViewPager.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isSwipeEnabled == false) {
                    return true;
                }
                return false;
            }
        };
    }*/



    //onPageListner for Tabviews pager
    private ViewPager.OnPageChangeListener getOnPageChangeListener()
    {
        return new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {

                /*Toast.makeText(salesMainActivity,
                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();*/
                if(position == 0)//selected
                {
                    if (iscurrentPaymentSaved)
                    {
                        viewPager.arrowScroll(View.FOCUS_RIGHT);
                    }
                    else {
                        Toast.makeText(salesMainActivity,
                                "Bill Items", Toast.LENGTH_SHORT).show();
                    }

                }
                else if (position == 1)
                {
                    Toast.makeText(salesMainActivity,
                            "Bill List", Toast.LENGTH_SHORT).show();
                    AppEventMsg updateEvent = new AppEventMsg(AppEventsDictionary.AppEvent.ON_PAYMENT_TAB, null);
                    m_listTabAppEventListner.onEvent(updateEvent);
                }
                else if (position == 2)
                {
                    Toast.makeText(salesMainActivity,
                            "Prev Bill List", Toast.LENGTH_SHORT).show();
                    m_prevTab.showPrevBills();
                }

            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        };
    }
}
