package com.journaldev.zebrasolar.fragments;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.fragments.phonebill.CardUtils;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;

import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ArrearsValidator {

    PayManager m_payManager;
    Job m_job;

    public ArrearsValidator(PayManager payManager)
    {
        m_payManager = payManager;
    }

    public void setJob(Job job)
    {
        this.m_job = job;
    }

    public boolean isArrearsJob()
    {
        double installmentAmount = m_job.getTotalAmount()/ m_job.getNoOfInstallments();
        int numOfinstallmentsUptoDate = CardUtils.getNoOfInstallmentsForToday(m_job);
        double totalPayableMoney = numOfinstallmentsUptoDate*installmentAmount;
        double totalPaid = getPaymentSum();

        if (totalPayableMoney > totalPaid)
        {
            return true;
        }

        return false;

    }

    public double getArrearsValForJob()
    {
        double installmentAmount = m_job.getTotalAmount()/ m_job.getNoOfInstallments();
        int numOfinstallmentsUptoDate = CardUtils.getNoOfInstallmentsForToday(m_job);
        double totalPayableMoney = numOfinstallmentsUptoDate*installmentAmount;
        double totalPaid = getPaymentSum();

        return totalPayableMoney - totalPaid;

    }


    private double getPaymentSum()
    {
        List<Payment> payList = m_payManager.getPaymentList(m_job.getJobId());

        double sumPayments = 0.0 ;
        sumPayments = m_job.getDownPayment();

        for (Payment payment : payList) {

            sumPayments = sumPayments +payment.getPaymentAmount();
        }

        return sumPayments;
    }


}
