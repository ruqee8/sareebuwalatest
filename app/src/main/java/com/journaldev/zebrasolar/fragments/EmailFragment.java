package com.journaldev.zebrasolar.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.journaldev.zebrasolar.APIClient;
import com.journaldev.zebrasolar.APIInterface;
import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.pojo.email.EmailResponse;
import com.journaldev.zebrasolar.pojo.email.EmailSend;
import com.journaldev.zebrasolar.pojo.job.JobSyncResponse;
import com.journaldev.zebrasolar.pojo.payment.PaymentSyncResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private APIInterface m_apiIF;


    String currentUserId = null;
    private View thisView;
    private Button emailBtn ;
    private EditText emailtxt;

    private OnFragmentInteractionListener mListener;

    public EmailFragment() {
        currentUserId = Integer.toString(Users.getCurrentUserId());
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmailFragment newInstance(String param1, String param2) {
        EmailFragment fragment = new EmailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      thisView = inflater.inflate(R.layout.fragment_email, container, false);

      m_apiIF = APIClient.getClient().create(APIInterface.class);

      emailBtn = thisView.findViewById(R.id.buttonEmail);
      emailBtn.setOnClickListener(this);
      emailtxt = thisView.findViewById(R.id.editTextEmail);

      return thisView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v == emailBtn)
        {
            sendEmail();
        }
    }


    private void sendEmail()
    {

        EmailSend request =new EmailSend();
        request.setRefId(getCurrentUserId());
        request.setMessage(emailtxt.getText().toString());
        Call<EmailResponse> call =  m_apiIF.sendEmail(request);
        call.request().url();
        call.enqueue(new Callback<EmailResponse>() {
            @Override
            public void onResponse(Call<EmailResponse> call, Response<EmailResponse> response) {

                EmailResponse signIn = response.body();
                boolean status = signIn.getStatus();

                if(status == true)
                {
                    Log.d("EMail", "onResponse: send sucess");
                    Toast.makeText(getContext(),  " Email send sucess\n", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EmailResponse> call, Throwable t) {
                Log.d("EMail", "onResponse: send failed");
                Toast.makeText(getContext(),  " Email send failed\n", Toast.LENGTH_SHORT).show();

            }


        });


    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
