package com.journaldev.zebrasolar.fragments.phonebill;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.journaldev.zebrasolar.R;
import com.journaldev.zebrasolar.SalesMainActivity;
import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.events.AppEventListener;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventsDictionary;
import com.journaldev.zebrasolar.fragments.cards.CardItem;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.pojo.job.JobGPSCreateRequest;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.thread.MessageStruct;

import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


interface JobUpdater
{
    public void updateJobWithGPS(double latitude,double longitude);
}


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CardTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CardTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CardTab extends Fragment
        implements View.OnClickListener,
        View.OnFocusChangeListener,
        AppEventListener,
        JobUpdater{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    int additionalItemId = 6;



    //views
    View this_view = null;

    String m_currUser = null;



    TextView paymentIDView = null;
    TextView customerNameView = null;


    TextView cardIText = null;
    TextView adressText = null;
    TextView phoneText = null;
    TextView totalAmountText = null;
    TextView startedDateText = null;
    TextView initialDownPaymentText = null;
    TextView noOfInstallmentsText = null;
    TextView installmentAmountText = null;
    TextView cardTypeText = null;

    TextView noOfInstallmentsForTodayText = null;
    TextView totalMoneyText = null;
    TextView paymentSumText = null;
    TextView arriersText = null;
    TextView outStandingText = null;
    TextView nextDateText = null;

    Button paymentBtn = null;
    Button setGPSBtn = null;
    Button getGPSBtn = null;

    Button callBtn = null;



    //This is the initial reference number
    int m_currPaymentId;
    CardItem card;


    //Main
    SalesMainActivity salesMainActivity;
    PayManager m_payManager;
    JobManager m_jobManager;

    Job m_currentJob;

    double m_installmentAmount;
    int m_periodCountforToday;
    double m_sumPayments;
    double m_totalPayableMoney;


    //event listner to comunicate with PhoneBillFragment
    private AppEventListener m_parentAppEventListner;


    private OnFragmentInteractionListener mListener;

    public CardTab() {
        CardUtils.setPoyaDays();
    }

    public void setEventListner(AppEventListener appEventListener)
    {
        m_parentAppEventListner = appEventListener;
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CardTab.
     */
    // TODO: Rename and change types and number of parameters
    public static CardTab newInstance(String param1, String param2) {
        CardTab fragment = new CardTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this_view = inflater.inflate(R.layout.fragment_phone_card_tab, container, false);
        salesMainActivity = (SalesMainActivity) getActivity();

        m_payManager = new PayManager(getContext());
        m_jobManager = new JobManager(getContext());

        //m_currPaymentId = FtHandler.getLastBillId() + 1;


        //Top fields start
        paymentIDView = this_view.findViewById(R.id.billID);



        customerNameView = this_view.findViewById(R.id.billCustomerName);
        card = ((SalesMainActivity) salesMainActivity).getM_currentCardItem();
        if (card == null)
        {
            customerNameView.setText("Please select a card");
            return this_view;
        }

        m_currentJob = m_jobManager.getJob(card.getId());

        paymentIDView.setText("බිල් අංකය #" + String.valueOf(m_payManager.getNextPaymentIdforThisJob(card.getId())));//Payment No


        customerNameView.setText(card.getCustomerName());

        populateViews();

        ///Payment Button
        setPaymentButtonBehavior();
        setSetGPSButtonBehavior();
        setGetGPSButtonBehavior();

        setCallButtonBehavior();


        return this_view;

    }

    public void setM_currUser(String user)
    {
        m_currUser = user;
    }

    private void setPaymentButtonBehavior()
    {
        paymentBtn = (Button) this_view.findViewById(R.id.paymentBtn);
        paymentBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to payment Tab
                //send billItem to parent fragment
                AppEventMsg event = new AppEventMsg(AppEventsDictionary.AppEvent.GOTO_PAYMENT,null);
                m_parentAppEventListner.onEvent(event);
            }
        });

    }

    private void setSetGPSButtonBehavior()
    {
        setGPSBtn = (Button) this_view.findViewById(R.id.setGPSBtn);
        final GPSHandler gpsHandler = new GPSHandler(getContext(),getActivity(),this);

        setGPSBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {


                gpsHandler.onClickSetGPS();

                //String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 46.414382, 10.013988);
                //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                // Make the Intent explicit by setting the Google Maps package
                //intent.setPackage("com.google.android.apps.maps");
                //getContext().startActivity(intent);


                /*
                String uri = "http://maps.google.com/maps?q=loc:" + 6.9061 + "," + 79.9696;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                */

            }
        });
    }

    private void setGetGPSButtonBehavior()
    {
        getGPSBtn = (Button) this_view.findViewById(R.id.getLocBtn);
        m_currentJob = m_jobManager.getJob(card.getId());

        getGPSBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(m_currentJob.getLatitude() == 0.0)
                {
                    ////////Dialog box start
                    AlertDialog alert = null;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Please Set Location");
                    builder.setCancelable(false);

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            return;
                        }
                    });
                    alert = builder.create();
                    alert.show();
                    return;
                }

                String uri = "http://maps.google.com/maps?q=loc:" + m_currentJob.getLatitude() + "," + m_currentJob.getLongtitude();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });



    }


    private void setCallButtonBehavior()
    {
        callBtn = (Button) this_view.findViewById(R.id.phoneCall);

        callBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+m_currentJob.getPhone()));
                startActivity(callIntent);

            }
        });
    }

    private void populateViews()
    {

        cardIText = this_view.findViewById(R.id.cardId);
        adressText = this_view.findViewById(R.id.addressVal);
        phoneText = this_view.findViewById(R.id.phoneValue);
        totalAmountText = this_view.findViewById(R.id.totalAmount);
        startedDateText = this_view.findViewById(R.id.startedDate);
        noOfInstallmentsText = this_view.findViewById(R.id.noOfInstallments);
        initialDownPaymentText =this_view.findViewById(R.id.initialDownPayment);
        installmentAmountText = this_view.findViewById(R.id.installmentAmount);
        cardTypeText = this_view.findViewById(R.id.cardType);

        noOfInstallmentsForTodayText = this_view.findViewById(R.id.noOfInstallmentsForToday);
        totalMoneyText = this_view.findViewById(R.id.totalMoney);
        paymentSumText = this_view.findViewById(R.id.paymentSum);
        arriersText = this_view.findViewById(R.id.arriersAmount);
        outStandingText = this_view.findViewById(R.id.outstandingAmount);
        nextDateText = this_view.findViewById(R.id.nextDate);
        // ---- views population end ------//

        populateInitialValues();
    }

    private void populateInitialValues()
    {
        cardIText.setText(m_currentJob.getJobId());
        adressText.setText(m_currentJob.getAddress());
        phoneText.setText(m_currentJob.getPhone());
        totalAmountText.setText(Double.toString(m_currentJob.getTotalAmount()));
        startedDateText.setText(m_currentJob.getDate());
        initialDownPaymentText.setText(Double.toString(m_currentJob.getDownPayment()));
        noOfInstallmentsText.setText(Integer.toString(m_currentJob.getNoOfInstallments()));

        m_installmentAmount = (m_currentJob.getTotalAmount() - m_currentJob.getDownPayment())/m_currentJob.getNoOfInstallments();
        installmentAmountText.setText(Double.toString(m_installmentAmount));

        if (m_currentJob.getInstallmentType() == Enums.INSTALLMENTTYPE.DAILY.getValue())
        {
            cardTypeText.setText("Daily");
        }
        else if(m_currentJob.getInstallmentType() == Enums.INSTALLMENTTYPE.WEEKLY.getValue())
        {
            cardTypeText.setText("Weekly");
        }
        else {
            cardTypeText.setText("Monthly");
        }

        populateAllValues();

    }


    private void populateAllValues()
    {
        m_periodCountforToday = CardUtils.getNoOfInstallmentsForToday(m_currentJob);
        noOfInstallmentsForTodayText.setText(Integer.toString(m_periodCountforToday));

        populateTotalPayableAmount();

        populatePaymentSum();

        populateArriers();

        populateTotalOutstanding();

        populateSpecialDate();
    }


    private void populateTotalPayableAmount()
    {
        m_totalPayableMoney =0.0;
        m_totalPayableMoney = m_periodCountforToday*m_installmentAmount;
        totalMoneyText.setText(Double.toString(m_totalPayableMoney));
    }



    private void populatePaymentSum()
    {
        List<Payment> payList = m_payManager.getPaymentList(m_currentJob.getJobId());

        m_sumPayments = m_currentJob.getDownPayment();

        for (Payment payment : payList) {

            m_sumPayments = m_sumPayments +payment.getPaymentAmount();
        }
        paymentSumText.setText(Double.toString(m_sumPayments));
    }


    private void populateArriers()
    {
        double arriesVal = m_totalPayableMoney - m_sumPayments;
        arriersText.setText(Double.toString(arriesVal));
    }

    private void populateTotalOutstanding()
    {
        double outVal = m_currentJob.getTotalAmount() - m_sumPayments;
        outStandingText.setText(Double.toString(outVal));
    }

    private void populateSpecialDate()
    {
        String nextDate = m_payManager.getMaxNextDate(m_currentJob.getJobId());

        if (nextDate.equals("") == false) {


            //set current date
            Calendar custom = Calendar.getInstance();
            custom.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
            custom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
            custom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

            Date currentDate = custom.getTime();

            Date nxtDate = null;

            //get Next Date
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                nxtDate = format.parse(nextDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (nxtDate.compareTo(currentDate) > 0) {

                nextDateText.setText(nextDate);
            }

        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public void onEvent(AppEventMsg event) {

        switch (event.getEvent())
        {
            case PAYMENT_SAVE:
            {
                populateAllValues();
                paymentIDView.setText("Select New Card");
                manageBlinkEffect();
                break;
            }

            default:
                break;
        }

    }

    private void manageBlinkEffect() {
        ObjectAnimator anim = ObjectAnimator.ofInt(paymentIDView, "backgroundColor", Color.WHITE, Color.RED,
                Color.WHITE);
        anim.setDuration(1500);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void updateJobWithGPS(double latitude,double longitude)
    {

        /*JobGPSCreateRequest job = m_currentJob.toJobGPSCreateRequest();

        String currUserId = String.valueOf(Users.instance(getContext()).getUserId(m_currUser));
        job.setRefId(Users.instance(getContext()).getUserId(m_currUser));

        int newRefSeq = m_jobManager.getNextRefSeq(Users.instance(getContext()).getUserId(m_currUser));
        job.setRefSeqId(newRefSeq);

        job.setLatitude(latitude);
        job.setLongitude(longitude);*/

        String currUserId = String.valueOf(Users.instance(getContext()).getUserId(m_currUser));
        m_currentJob.setRefId(Users.instance(getContext()).getUserId(m_currUser));

        int newRefSeq = m_jobManager.getNextRefSeq(Users.instance(getContext()).getUserId(m_currUser));


        m_currentJob.setRefSeqId(newRefSeq);
        m_currentJob.setLongtitude(longitude);
        m_currentJob.setLatitude(latitude);
        m_currentJob.setStatus(Enums.Status.UPDATED.getValue());

        //send to DB
        m_jobManager.updateJob(m_currentJob);

        //send to sync handler

        BackendSynchronizeHandler handler = ((SalesMainActivity)getActivity()).getM_backendSynchronizeHandler();

        Message msg = handler.obtainMessage();

        //create and populate message object
        MessageStruct sendObj = new MessageStruct();
        sendObj.setType(MessageStruct.MessageType.JOB_SYNCHRONICE);
        sendObj.setUserData(m_currentJob);
        msg.obj =  sendObj;// Some Arbitrary objec
        handler.sendMessage(msg);



        ////////Dialog box start
        AlertDialog alert = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Location set successful");
        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                return;
            }
        });
        alert = builder.create();
        alert.show();
        return;

    }



















}

