package com.journaldev.zebrasolar.fragments.phonebill;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PhoneBillPagerAdapter extends FragmentStatePagerAdapter {

    int m_noOfTabs;

    private CardTab cardTab;
    private PaymentTab paymentTab;
    private PrevTab prevTab;


    public PhoneBillPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        m_noOfTabs = numOfTabs;
    }

    public CardTab getCardTab() {
        return cardTab;
    }

    public void setCardTab(CardTab cardTab) {
        this.cardTab = cardTab;
    }

    public PaymentTab getPaymentTab() {
        return paymentTab;
    }

    public void setPaymentTab(PaymentTab paymentTab) {
        this.paymentTab = paymentTab;
    }

    public PrevTab getPrevTab() {
        return prevTab;
    }

    public void setPrevTab(PrevTab prevTab) {
        this.prevTab = prevTab;
    }



    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return getCardTab();
            case 1:
                return getPaymentTab();
            case 2:
                return getPrevTab();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return m_noOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0)
        {
            return  "කාඩ්";//CARD
        }
        else if(position == 1)
        {
            return "ගෙවීම්";//PAYMENTS
        }
        else if(position == 2)
        {
            return "පෙරගෙවීම්";//PREV
        }

        return "-";
    }


}
