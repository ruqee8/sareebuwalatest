package com.journaldev.zebrasolar;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CrashHandlerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_handler);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Please contact technical support", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Intent intent = getIntent();

        setTitle("App Crash Report [" + intent.getStringExtra("ACT") + "]");

        TextView t1 =  findViewById(R.id.crashErrorTitle);
        t1.setText("An Error Occured. Error logged in file : " + intent.getStringExtra("ERRFILE"));

        TextView t =  findViewById(R.id.crashErrorText);
        t.setText(intent.getStringExtra("REPORT"));

        Button btn = findViewById(R.id.crashExitBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
                System.exit(2);
            }
        });
    }

}
