package com.journaldev.zebrasolar.utils.billPrinter.impl;

import android.app.Activity;

import com.journaldev.zebrasolar.utils.BluetoothPrinter;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinter;
import com.journaldev.zebrasolar.utils.billPrinter.PaymentBill;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BandaranayakaBillPrinter extends BillPrinter {
    public BandaranayakaBillPrinter(Activity activity)
    {
        super(activity);
    }

    protected void printFooter(BluetoothPrinter mPrinter) {
        mPrinter.addNewLine();
        mPrinter.addNewLine();
        mPrinter.addNewLine();

        mPrinter.printText("------------------           ------------------");mPrinter.addNewLine();
        mPrinter.printText("Customer Signature          Sales Rep Signature");mPrinter.addNewLine();mPrinter.addNewLine();

        //                  -----------------------------------------------
        mPrinter.printText("   We are proud about you purchasing product ");mPrinter.addNewLine();
        mPrinter.printText("                 of Sri Lanka.");mPrinter.addNewLine();
        mPrinter.printText("Please call our hotline 0775865696 for your");mPrinter.addNewLine();
        mPrinter.printText(" valued suggestions and comments");mPrinter.addNewLine();

        final Calendar calendar = Calendar.getInstance();
        String timeStr = new SimpleDateFormat("h.mm a").format(calendar.getTime());

        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
        mPrinter.printText("THANK YOU!  " + "Time End: " + timeStr);

        mPrinter.addNewLine();
        mPrinter.addNewLine();
        mPrinter.addNewLine();
        mPrinter.flush();
    }

    protected void printHeader(BluetoothPrinter mPrinter, PaymentBill bill)
    {
        //                  -----------------------------------------------
        mPrinter.setBold(true);
        mPrinter.printText("Bandaranayaka Trading LTD"); mPrinter.addNewLine();
        mPrinter.flush();mPrinter.setBold(false);
        mPrinter.printText("    Owatta, Hingula       Reg.No:PV00205543"); mPrinter.addNewLine();
        mPrinter.addNewLine();

        /*
        String repName = bill.getIssuedBy().substring(0, Math.min(21, bill.getIssuedBy().length()));
        mPrinter.printText("Rep: " + repName + padLeft(" Factory", 30 - repName.length())); mPrinter.addNewLine();
        mPrinter.printText("Contact: 0769295151         Hot:    +94775865696"); mPrinter.addNewLine();

        String routeName = InitialDataOperations.mapRoutes.get(bill.getCustomer().routeid).name;// "Mawanella - Molligoda";
        routeName = routeName.substring(0, Math.min(19, routeName.length()));
        mPrinter.printText("Route: " + routeName + padLeft("Office: +94352247599", 41 - routeName.length())); mPrinter.addNewLine();
        mPrinter.printText("         email:rudilproducts@gmail.com"); mPrinter.addNewLine();
        mPrinter.printLine();mPrinter.addNewLine();
        */

        mPrinter.flush();
    }
}
