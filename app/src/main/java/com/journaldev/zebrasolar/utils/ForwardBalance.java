package com.journaldev.zebrasolar.utils;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.journaldev.zebrasolar.db.ForwardBalanceManager;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.model.ForwardBalanceModel;
import com.journaldev.zebrasolar.model.ForwardPayment;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class ForwardBalance {


    PayManager m_payManager;
    ForwardBalanceManager m_forwardBalnceManager;
    JobManager m_jobManager;
    Context m_context;
    Calendar m_calendar;

    public ForwardBalance(Context context)
    {
        m_context = context;
        m_payManager = new PayManager(m_context);
        m_forwardBalnceManager = new ForwardBalanceManager(m_context);
        m_jobManager = new JobManager(m_context);
        m_calendar = new GregorianCalendar();
    }


   public void generateForwardBalanceForJobCreation(Job job)
   {
       ForwardBalanceModel fwb = new ForwardBalanceModel();
       fwb.setJobId(job.getJobId());
       fwb.setForwardBalance("0");
       m_forwardBalnceManager.insertForwardBalance(fwb);


       ForwardPayment fwpayment = new ForwardPayment();
       fwpayment.setJobId(job.getJobId());
       fwpayment.setPaymentId("0");
       fwpayment.setPaymentAmount("0");

       Date zerothDueDate = getZerothDueDate(job);


       String dateStr = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(zerothDueDate);
       fwpayment.setRelatedDuedate(dateStr);

       m_forwardBalnceManager.insertForwardPayment(fwpayment);

   }

   public void generateForwardBalanceForPayment(Payment payment)
   {
       ForwardPayment fwPayment = m_forwardBalnceManager.getMaxDatedForwardPayment(payment.getJobId());

       Date paymentDate = getGivenDate(payment.getDate());

       Date lastPaidPaymentDueDate = getGivenDate(fwPayment.getRelatedDuedate());
       Date currentMonthsDueDate = getPaymentMonthsDueDate(payment);
       Date currentMonthsPanaltyDate = getGivenMonthsPanaltyDate(currentMonthsDueDate);
       Date nextMonthsDueDate = getNextMonthsDueDate(currentMonthsDueDate);
       Date prevMonthsDueDate = getPreviousMonthsDueDate(currentMonthsDueDate);
       Date prevMonthsPanaltyDate = getPreviousMonthsPanaltyDate(currentMonthsDueDate);


       ForwardPayment fwpayment = new ForwardPayment();
       fwpayment.setJobId(payment.getJobId());
       //fwpayment.setPaymentId(payment.getPaymentId());
       //fwpayment.setPaymentAmount(payment.getPaymentAmount());


       Date paymentValidDueDate = null;

       //currnent date is on or before previous month panalty date
       if (paymentDate.before(prevMonthsPanaltyDate) || isEqualDates(paymentDate,prevMonthsPanaltyDate))
       {
           paymentValidDueDate = prevMonthsDueDate;

       }
       else if (paymentDate.after(prevMonthsPanaltyDate) && ( paymentDate.before(currentMonthsPanaltyDate) || isEqualDates(paymentDate,currentMonthsPanaltyDate))  )
       {
           paymentValidDueDate = currentMonthsDueDate;
       }
       else
       {
           paymentValidDueDate = nextMonthsDueDate;

       }

       /*m_calendar.setTime(paymentValidDueDate);

       String dateStr = Integer.toString(m_calendar.get(Calendar.YEAR))+
               Integer.toString(m_calendar.get(Calendar.MONTH)+1)+
               Integer.toString(m_calendar.get(Calendar.DAY_OF_MONTH));*/

       String dateStr = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(paymentValidDueDate);

       fwpayment.setRelatedDuedate(dateStr);
       m_forwardBalnceManager.insertForwardPayment(fwpayment);

       updateForwardBalance(payment,paymentValidDueDate,lastPaidPaymentDueDate);

   }


    private void updateForwardBalance(Payment payment,Date paymentValidDueDate,Date lastPaidPaymentDueDate)
    {
        /*
        ForwardBalanceModel fwb = new ForwardBalanceModel();
        fwb.setJobId(payment.getJobId());

        int notPaidMonths = getMonthsDifference(lastPaidPaymentDueDate,paymentValidDueDate);

        if(notPaidMonths > 0)
        {
            Job job = m_jobManager.getJob(payment.getJobId());

            String forwardBalanceWithoutDueBalance = generatePercentageValue(job.getInstallment(),notPaidMonths);
            Double realForwardValuedouble = Double.parseDouble(m_forwardBalnceManager.getCurrentForwardBalance(payment.getJobId()))
                                            + Double.parseDouble(forwardBalanceWithoutDueBalance)
                                            - Double.parseDouble(payment.getPanaltyAmount());
            String realForwardValueStr = Double.toString(realForwardValuedouble);
            fwb.setForwardBalance(realForwardValueStr);

            m_forwardBalnceManager.updateForwardBalance(fwb);
        }
        else {

        }
        */
    }


    //get forward balance while load jobs
    public String generateForwardBalanceWhenJobLoad(Job job)
    {
        ForwardPayment fwPayment = m_forwardBalnceManager.getMaxDatedForwardPayment(job.getJobId());

        Date currentDate = Calendar.getInstance().getTime();

        Date lastPaidPaymentDueDate = getGivenDate(fwPayment.getRelatedDuedate());
        Date currentMonthsDueDate = getCurrentDateRelatedMonthsDueDate(job);
        Date currentMonthsPanaltyDate = getGivenMonthsPanaltyDate(currentMonthsDueDate);
        Date nextMonthsDueDate = getNextMonthsDueDate(currentMonthsDueDate);
        Date prevMonthsDueDate = getPreviousMonthsDueDate(currentMonthsDueDate);
        Date prevMonthsPanaltyDate = getPreviousMonthsPanaltyDate(currentMonthsDueDate);


        /*ForwardPayment fwpayment = new ForwardPayment();
        fwpayment.setJobId(payment.getJobId());
        fwpayment.setPaymentId(payment.getPaymentId());
        fwpayment.setPaymentAmount(payment.getPaymentAmount());
        */

        Date paymentValidDueDate = null;

        //currnent date is on or before previous month panalty date
        if (currentDate.before(prevMonthsPanaltyDate) || isEqualDates(currentDate,prevMonthsPanaltyDate))
        {
            paymentValidDueDate = prevMonthsDueDate;

        }
        else if (currentDate.after(prevMonthsPanaltyDate) && ( currentDate.before(currentMonthsPanaltyDate) || isEqualDates(currentDate,currentMonthsPanaltyDate))  )
        {
            paymentValidDueDate = currentMonthsDueDate;
        }
        else
        {
            paymentValidDueDate = nextMonthsDueDate;

        }

        return getForwardBalance(job,paymentValidDueDate,lastPaidPaymentDueDate);

    }

    private String getForwardBalance(Job job,Date paymentValidDueDate,Date lastPaidPaymentDueDate)
    {/*
        ForwardBalanceModel fwb = new ForwardBalanceModel();
        fwb.setJobId(job.getJobId());

        int notPaidMonths = getMonthsDifference(lastPaidPaymentDueDate,paymentValidDueDate);

        String forwardBalance = "0";

        if(notPaidMonths > 0)
        {
            String forwardBalanceWithoutDueBalance = generatePercentageValue(job.getInstallment(),notPaidMonths);
            Double realForwardValuedouble = Double.parseDouble(m_forwardBalnceManager.getCurrentForwardBalance(job.getJobId()))
                    + Double.parseDouble(forwardBalanceWithoutDueBalance);
            String realForwardValueStr = Double.toString(realForwardValuedouble);
            forwardBalance = realForwardValueStr;

        }
        else {
            forwardBalance = m_forwardBalnceManager.getCurrentForwardBalance(job.getJobId());
        }

        return forwardBalance;
        */

        return "0";
    }


    //get forward balance while load jobs
    public String generateTotalBalanceWhenJobLoad(Job job)
    {
        ForwardPayment fwPayment = m_forwardBalnceManager.getMaxDatedForwardPayment(job.getJobId());

        Date currentDate = Calendar.getInstance().getTime();

        Date lastPaidPaymentDueDate = getGivenDate(fwPayment.getRelatedDuedate());
        Date currentMonthsDueDate = getCurrentDateRelatedMonthsDueDate(job);
        Date currentMonthsPanaltyDate = getGivenMonthsPanaltyDate(currentMonthsDueDate);
        Date nextMonthsDueDate = getNextMonthsDueDate(currentMonthsDueDate);
        Date prevMonthsDueDate = getPreviousMonthsDueDate(currentMonthsDueDate);
        Date prevMonthsPanaltyDate = getPreviousMonthsPanaltyDate(currentMonthsDueDate);


        /*ForwardPayment fwpayment = new ForwardPayment();
        fwpayment.setJobId(payment.getJobId());
        fwpayment.setPaymentId(payment.getPaymentId());
        fwpayment.setPaymentAmount(payment.getPaymentAmount());
        */

        Date paymentValidDueDate = null;

        //currnent date is on or before previous month panalty date
        if (currentDate.before(prevMonthsPanaltyDate) || isEqualDates(currentDate,prevMonthsPanaltyDate))
        {
            paymentValidDueDate = prevMonthsDueDate;

        }
        else if (currentDate.after(prevMonthsPanaltyDate) && ( currentDate.before(currentMonthsPanaltyDate) || isEqualDates(currentDate,currentMonthsPanaltyDate))  )
        {
            paymentValidDueDate = currentMonthsDueDate;
        }
        else
        {
            paymentValidDueDate = nextMonthsDueDate;

        }

        Date finalDueDate = getFinalDueDateForJob(job.getJobId());

        return getTotalBalance(job,paymentValidDueDate,lastPaidPaymentDueDate,finalDueDate);

    }



    private String getTotalBalance(Job job,Date paymentValidDueDate,Date lastPaidPaymentDueDate,Date finalPaymentDueDateForJob)
    {/*
        ForwardBalanceModel fwb = new ForwardBalanceModel();
        fwb.setJobId(job.getJobId());


        //scenario to check lastPaidpayment due date is equal to zeroth payment due date
        //if that user should pay installment * all nu of installments
        boolean isZerothDuedate = isEqualDates(lastPaidPaymentDueDate,getFirstDueDate(job.getJobId()));


        int notPaidMonths = getMonthsDifference(lastPaidPaymentDueDate,paymentValidDueDate);


        int remainingFutureMonthsAfterToday = getMonthsDifference(paymentValidDueDate,finalPaymentDueDateForJob);
        Double remainingAmount = Double.parseDouble(job.getInstallment())* remainingFutureMonthsAfterToday;


        Double totalBalance = 0.0;

        if(notPaidMonths > 0)
        {
            String forwardBalanceWithoutDueBalance = generatePercentageValue(job.getInstallment(),notPaidMonths);
            Double realForwardValuedouble = Double.parseDouble(m_forwardBalnceManager.getCurrentForwardBalance(job.getJobId()))
                    + Double.parseDouble(forwardBalanceWithoutDueBalance);
            totalBalance = realForwardValuedouble;

        }
        else if(notPaidMonths == -1)//there is payment for current month
        {
            totalBalance = Double.parseDouble(m_forwardBalnceManager.getCurrentForwardBalance(job.getJobId()));
            if(isZerothDuedate == false) {
                remainingAmount = Double.parseDouble(job.getInstallment()) * (remainingFutureMonthsAfterToday - 1);
            }
            else
            {
                remainingAmount = Double.parseDouble(job.getInstallment()) * remainingFutureMonthsAfterToday;
            }
        }
        else {
            totalBalance = Double.parseDouble(m_forwardBalnceManager.getCurrentForwardBalance(job.getJobId()));
        }

        totalBalance = totalBalance + remainingAmount;


        return Double.toString(totalBalance);
        */

        return "0";
    }

    private int getRemainingNoOfMonthsAfterToday(Date paymentValidDueDate,Date finalPaymentDueDate)
    {
        return getMonthsDifference(paymentValidDueDate,finalPaymentDueDate);
    }


   private Date getFirstDueDate(String jobId)
   {/*
        Job job = m_jobManager.getJob(jobId);
        String dateStr = job.getDate();

       Date date = null;
       SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
       try {
           date = format.parse(dateStr);
       } catch (ParseException e) {
           e.printStackTrace();
       }


       date.setMonth(date.getMonth()+1);
       date.setDate(Integer.parseInt(job.getDueDate()));

       return date;*/
       return null;
   }

   private Date getFinalDueDateForJob(String jobId)
   {
       /*
       Date firstDueDate = getFirstDueDate(jobId);
       Job job = m_jobManager.getJob(jobId);

       Date lastDueDate = firstDueDate;
       lastDueDate.setMonth(lastDueDate.getMonth()+Integer.parseInt(job.getNoOfInstallments()));

       return lastDueDate;
    */

       return null;
   }

    private Date getZerothDueDate(Job job)
    {
        /*
        String dateStr = job.getDate();

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        date.setDate(Integer.parseInt(job.getDueDate()));

        return date;
        */

        return null;
    }

   private Date getPaymentMonthsDueDate(Payment payment)
   {/*
       Job job = m_jobManager.getJob(payment.getJobId());

       String dateStr = payment.getDate();
       Date date = getGivenDate(dateStr);
       date.setDate(Integer.parseInt(job.getDueDate()));

       return date;
*/

       return null;
   }

    private Date getCurrentDateRelatedMonthsDueDate(Job job)
    {
        /*
        Date date = Calendar.getInstance().getTime();
        date.setDate(Integer.parseInt(job.getDueDate()));

        return date;

*/

        return null;
    }



   private Date getGivenDate(String dateStr)//dd-MMM-yyyy hh:mm:ss
   {
       Date date = null;
       SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
       try {
             date = format.parse(dateStr);
       } catch (ParseException e) {
           e.printStackTrace();
       }

       return date;

   }

   private Date getPreviousMonthsPanaltyDate(Date givenMonthDueDate)
   {
       Date prevMonthPanaltyDate = (Date) givenMonthDueDate.clone();
       prevMonthPanaltyDate.setMonth(prevMonthPanaltyDate.getMonth() -1);
       prevMonthPanaltyDate = getGivenMonthsPanaltyDate(prevMonthPanaltyDate);
       return prevMonthPanaltyDate;
   }

   private Date getNextMonthsDueDate(Date givenMonthDueDate)
   {
       Date nextMonthDueDate = (Date) givenMonthDueDate.clone();
       nextMonthDueDate.setMonth(nextMonthDueDate.getMonth() +1);
       return nextMonthDueDate;

   }

    private Date getPreviousMonthsDueDate(Date givenMonthDueDate)
    {
        Date previousMonthDueDate = (Date) givenMonthDueDate.clone();
        previousMonthDueDate.setMonth(previousMonthDueDate.getMonth() -1);
        return previousMonthDueDate;

    }

    private Date getGivenMonthsPanaltyDate(Date givenMonthDueDate)
    {
        Date givenMonthPanaltyDate = (Date) givenMonthDueDate.clone();
        givenMonthPanaltyDate.setDate(givenMonthPanaltyDate.getDate()+5);
        return givenMonthPanaltyDate;
    }

    private boolean isEqualDates(Date date1,Date date2)
    {
        if (date1.getYear() == date2.getYear() &&
            date2.getMonth() == date2.getMonth() &&
            date1.getDate() == date2.getDate() )
        {
            return true;
        }

        return false;
    }

    private String generatePercentageValue(String installmentValue , int noOfInstallments)
    {
        return Double.toString(Double.parseDouble(installmentValue)*noOfInstallments*1.1);
    }

    public int getMonthsDifference(Date date1, Date date2) {
        int m1 = date1.getYear() * 12 + date1.getMonth();
        int m2 = date2.getYear() * 12 + date2.getMonth();
        return m2 - m1 -1;
    }


    public void removeForwardBalancesForJob(String jobid)
    {
        m_forwardBalnceManager.removeForwardBalancesForJob(jobid);
    }
}
