package com.journaldev.zebrasolar.utils.billPrinter;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.util.Log;

/*import com.journaldev.storeerp.SalesMainActivity;
import com.journaldev.storeerp.model.Bill;
import com.journaldev.storeerp.model.BillHelper;
import com.journaldev.storeerp.model.BillItem;
import com.journaldev.storeerp.utils.BluetoothPrinter;*/

import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.utils.BluetoothPrinter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.NoSuchElementException;

public abstract class BillPrinter {
    protected final Activity m_activity;
    DecimalFormat df;

    public BillPrinter(Activity activity)
    {
        this.m_activity = activity;
        df = new DecimalFormat("#.###");
    }

    public boolean printPrevBill(final String str)
    {
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        try {

            //crash fix on connectivity
            if (btAdapter.getBondedDevices().iterator().hasNext() == false) {

                showError();
                return false;

            }


            final BluetoothDevice mBtDevice = btAdapter.getBondedDevices().iterator().next();   // Get first paired device

            final BluetoothPrinter mPrinter = new BluetoothPrinter(mBtDevice);
            mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

                @Override
                public void onConnected() {
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);

                    mPrinter.printText(str);

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mPrinter.finish(true);


                }

                @Override
                public void onFailed() {

                    Log.d("BluetoothPrinter", "Conection failed");
                    showError();
                }

            });
            return true;

        } catch (NullPointerException e) {
            showError();

            return false;
        } catch (NoSuchElementException e) {

            showError();
            // Print to Console output

            return false;

        }

    }

    public boolean doPrint(final PaymentBill bill) {

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        try {

           //crash fix on connectivity
            if (btAdapter.getBondedDevices().iterator().hasNext() == false) {

                showError();
                return false;

            }


            final BluetoothDevice mBtDevice = btAdapter.getBondedDevices().iterator().next();   // Get first paired device

            final BluetoothPrinter mPrinter = new BluetoothPrinter(mBtDevice);
            mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

                @Override
                public void onConnected() {
                    mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);

                    printHeader(mPrinter, bill);
                    printHeading(mPrinter, bill);

                    printBody(mPrinter, bill);

                    printFooter(mPrinter);

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mPrinter.finish(true);


                }

                @Override
                public void onFailed() {

                    Log.d("BluetoothPrinter", "Conection failed");
                    showError();
                }

            });
            return true;

        } catch (NullPointerException e) {
            showError();
            // Print to Console output
            boolean isDebug = true;
            if (isDebug) {
                final BluetoothPrinter mPrinter = new BluetoothPrinter(null);
                mPrinter.connectPrinter();


                printHeader(mPrinter, bill);
                printHeading(mPrinter, bill);

                printBody(mPrinter, bill);

                printFooter(mPrinter);

                mPrinter.finish(true);

            }


            return false;
        } catch (NoSuchElementException e) {

            showError();
            // Print to Console output
            boolean isDebug = true;
            if (isDebug) {
                final BluetoothPrinter mPrinter = new BluetoothPrinter(null);
                mPrinter.connectPrinter();


                printHeader(mPrinter, bill);
                printHeading(mPrinter, bill);

                printBody(mPrinter, bill);

                printFooter(mPrinter);

                mPrinter.finish(true);

            }

            return false;

        }

    }

    public void showError()
    {

        new AlertDialog.Builder(m_activity)
                .setTitle("Blue tooth Connection error")
                .setMessage("Please check buetooth connection")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public String getBillString(final PaymentBill bill)
    {
        final BluetoothPrinter mPrinter = new BluetoothPrinter(null);
        mPrinter.connectPrinter();
        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);

        printHeader(mPrinter, bill);

        printHeading(mPrinter, bill);

        printBody(mPrinter, bill);

        printFooter(mPrinter);

        return mPrinter.getFinishedString();

    }

    private void printHeading(BluetoothPrinter mPrinter, PaymentBill bill) {

        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy MMM dd");
        String dateStr = formatter.format(calendar.getTime());
        String timeStr = new SimpleDateFormat("h.mm a").format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_WEEK, 7);

        mPrinter.printText("Date:" + dateStr + padRight("  Time: " + timeStr, 27)); mPrinter.addNewLine();
        mPrinter.printLine();mPrinter.addNewLine();

        String invoiceNo = "Job No : " + bill.getJobId();

        mPrinter.printText(invoiceNo);mPrinter.addNewLine();
        mPrinter.printLine();mPrinter.addNewLine();
        mPrinter.flush();
    }

    protected static String padLeft(String s, int n) {
        String ss = String.format("%1$" + n + "s", s);
        return ss;
    }

    protected static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    protected static String padSides(String s, int width){
        return String.format("%-" + width  + "s",
                String.format("%" + (s.length() + (width - s.length()) / 2) + "s", s));
    }

    // Template methods
    protected abstract void printFooter(BluetoothPrinter mPrinter) ;

    protected void printBody(BluetoothPrinter mPrinter, PaymentBill bill){
        mPrinter.printText("Payment Amount");mPrinter.addNewLine();
        mPrinter.flush();
        mPrinter.printLine();mPrinter.addNewLine();


        mPrinter.addNewLine();
        mPrinter.addNewLine();

        mPrinter.printLine();mPrinter.addNewLine();
        mPrinter.printText("Payment Amount" + padLeft(String.format("%.2f", bill.getPaymentAmount()), 38));mPrinter.addNewLine();
        mPrinter.addNewLine();

        mPrinter.flush();
    }

    protected void printChequeDetails(BluetoothPrinter mPrinter, PaymentBill bill){
        /*mPrinter.printText("Cheque Date " + padLeft(bill.getChequeDate(), 28));mPrinter.addNewLine();*/

    }

    protected abstract void printHeader(BluetoothPrinter mPrinter, PaymentBill bill);


}
