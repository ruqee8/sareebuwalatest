package com.journaldev.zebrasolar.utils.billPrinter;

import android.app.Activity;

import com.journaldev.zebrasolar.utils.billPrinter.impl.BandaranayakaBillPrinter;
import com.journaldev.zebrasolar.utils.BluetoothPrinter;

public class BillPrinterFactory {
    public enum BILL_PRINT_ID{
        DEFAULT,
        BANDARANAYAKA
    }

    public static BillPrinter createBillPrinter(Activity activity, BILL_PRINT_ID id){
        switch (id){
            case DEFAULT:
            case BANDARANAYAKA:
                return new BandaranayakaBillPrinter(activity);

            default:
                return new BandaranayakaBillPrinter(activity);

        }
    }
}
