package com.journaldev.zebrasolar.utils;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CrashReportUtils {
    public static String generateCrashReport(Thread paramThread, Throwable paramThrowable) {
        StackTraceElement[] arr = paramThrowable.getStackTrace();
        final StringBuffer report = new StringBuffer(paramThrowable.toString() + "\n");
        final String lineSeperator = "----------------------------------------------------------------------------\n\n";
        report.append(lineSeperator);

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(currentTime.getTime());
        report.append("\n   TIME STAMP : " + formattedDate + "\n");
        report.append("   THREAD : " + paramThread.getName() + "\n\n");

        report.append("-- Stack trace -------------------------------------\n\n");
        for (int i = 0; i < arr.length; i++) {
            report.append("    ");
            report.append(arr[i].toString());
            report.append("\n");
        }
        // If the exception was thrown in a background thread inside
        // AsyncTask, then the actual exception can be found with getCause
        report.append("\n-- Cause --------------------------------------------\n\n");
        Throwable cause = paramThrowable.getCause();
        if (cause != null) {
            report.append(cause.toString());
            report.append("\n\n");
            arr = cause.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                report.append("    ");
                report.append(arr[i].toString());
                report.append("\n");
            }
        }
        // Getting the Device brand,model and sdk verion details.
        report.append("\n-- Device --------------------------------------------\n\n");
        report.append(" Brand: ");
        report.append(Build.BRAND);
        report.append("\n");
        report.append(" Device: ");
        report.append(Build.DEVICE);
        report.append("\n");
        report.append(" Model: ");
        report.append(Build.MODEL);
        report.append("\n");
        report.append(" Id: ");
        report.append(Build.ID);
        report.append("\n");
        report.append(" Product: ");
        report.append(Build.PRODUCT);
        report.append("\n");
        report.append("\n-- Firmware --------------------------------------------\n\n");
        report.append(" SDK: ");
        report.append(Build.VERSION.SDK_INT);
        report.append("\n");
        report.append(" Release: ");
        report.append(Build.VERSION.RELEASE);
        report.append("\n");
        report.append(" Incremental: ");
        report.append(Build.VERSION.INCREMENTAL);
        report.append("\n");
        report.append(lineSeperator);

        return report.toString();
    }

    public static String writeCrashReport(String strReport) {
        // TODO write to internal storage and send content to backend
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(dir, "smart_rep_log.txt");
        String filePath = "";
        try {
            filePath = file.getCanonicalPath();
        } catch (IOException e) {
            return null;
        }
        try (FileWriter fileWriter = new FileWriter(file, true)) {
            Date currentTime = Calendar.getInstance().getTime();
            fileWriter.append(strReport);
            fileWriter.flush();
            fileWriter.close();
            Log.d("ERRORHAN", "File Written");
            return filePath;

        } catch (IOException e) {
            return null;
        }
    }
}
