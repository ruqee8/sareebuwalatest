package com.journaldev.zebrasolar.ft;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Payment;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.thread.MessageStruct;

import java.util.List;

public class FtHandler {

    private  static int lastJobSeq;
    private  static int lastPaySeq;

    private BackendSynchronizeHandler mSyncHandler = null;
    private Context mContext;

    public FtHandler(BackendSynchronizeHandler mHandler, Context context)
    {
        mSyncHandler = mHandler;
        mContext = context;
    }

    public FtHandler(Context context)
    {
        mContext = context;
    }

    public void preInit(Context context,String userId)
    {

    }

    public void init(Context context,String userId)
    {
        SyncOperations syncOperations = new SyncOperations(context);

        //update last bill id with database sync table
        lastJobSeq = syncOperations.getLastSeq(
                Enums.TableType.JOB,userId);

        lastPaySeq = syncOperations.getLastSeq(
                Enums.TableType.PAYMENT,userId);



        //IF bills available with greater number than lastBillID then them should be push to synchronous thread
        checkJobTableAndUploadToServer();
        checkPaymentTableAndUploadToServer();


        //set MAX refID from bill table to last billID
        Log.w("Main Thread", "Last Job Ref "+ lastJobSeq + "Last Pay Ref" + lastPaySeq);

    }


    public static int getLastJobSeq() {
        return lastJobSeq;
    }

    public int getCurrentBillId()
    {
        return ++lastJobSeq;
    }

    public void setLastBillId(int lastBillId) {
        this.lastJobSeq = lastBillId;
    }


    //There can be bills which are not synchronize with server when app start
    //send all the bills which billId is greater than last synced bill.
    public void checkJobTableAndUploadToServer() {


        JobManager jobManager = new JobManager(mContext);
        List<Job> jobs = jobManager.getNotSyncedJobList(lastJobSeq);


        if (jobs.size() > 0) {
            Log.d("jobs", "checkJobTableAndUploadToServer: ");

            for (int counter = 0; counter < jobs.size(); counter++) {
                Job job = jobs.get(counter);

                //send to queue
                Message msg = mSyncHandler.obtainMessage();

                //create and populate message object
                MessageStruct sendObj = new MessageStruct();

                sendObj.setType(MessageStruct.MessageType.JOB_SYNCHRONICE);
                sendObj.setUserData(job);;

                msg.obj = sendObj;// Some Arbitrary objec
                mSyncHandler.sendMessage(msg);
            }
        }


    }


    public void checkPaymentTableAndUploadToServer() {


        PayManager payManager = new PayManager(mContext);
        List<Payment> payments = payManager.getNotSyncedPaymentList(lastPaySeq);


        if (payments.size() > 0) {
            Log.d("jobs", "checkPaymentTableAndUploadToServer: ");

            for (int counter = 0; counter < payments.size(); counter++) {
                Payment payment = payments.get(counter);

                //send to queue
                Message msg = mSyncHandler.obtainMessage();

                //create and populate message object
                MessageStruct sendObj = new MessageStruct();

                sendObj.setType(MessageStruct.MessageType.PAYMENT_SYNCRONIZE);
                sendObj.setUserData(payment);

                msg.obj = sendObj;// Some Arbitrary objec
                mSyncHandler.sendMessage(msg);
            }
        }


    }

    public void onPostSuccess(int refSeq, Enums.TableType tableType,int userId)
    {
        if (mContext == null)
            return;

        SyncOperations syncOperations = new SyncOperations(mContext);
        syncOperations.checkpointLastSyncedTxnID(refSeq,userId,tableType);
    }

    public void updateLastTxnID(int lastTxn) {
        if (mContext == null)
            return;

        //SyncOperations.checkpointLastSyncedTxnID(mContext, lastTxn, true);

    }

}
