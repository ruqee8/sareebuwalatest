package com.journaldev.zebrasolar;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.journaldev.zebrasolar.backend.common.Enums;
import com.journaldev.zebrasolar.db.DataBaseHelper;
import com.journaldev.zebrasolar.fragments.Summary.SummaryFragment;
import com.journaldev.zebrasolar.fragments.cards.CardsAllFragment;
import com.journaldev.zebrasolar.fragments.cards.CardsFragment;
import com.journaldev.zebrasolar.fragments.phonebill.CardTab;
import com.journaldev.zebrasolar.fragments.phonebill.CardUtils;
import com.journaldev.zebrasolar.fragments.phonebill.PaymentTab;
import com.journaldev.zebrasolar.fragments.phonebill.PhoneBillFragment;
import com.journaldev.zebrasolar.fragments.phonebill.PrevTab;
import com.journaldev.zebrasolar.fragments.NumPadFragment;
import com.journaldev.zebrasolar.ft.FtHandler;
import com.journaldev.zebrasolar.db.JobManager;
import com.journaldev.zebrasolar.db.PayManager;
import com.journaldev.zebrasolar.db.SyncOperations;
import com.journaldev.zebrasolar.events.AppEventMsg;
import com.journaldev.zebrasolar.events.AppEventListener;

import com.journaldev.zebrasolar.fragments.AdminFragment;
import com.journaldev.zebrasolar.fragments.EmailFragment;
import com.journaldev.zebrasolar.fragments.UpdateFragment;

import com.journaldev.zebrasolar.fragments.cards.CardItem;
import com.journaldev.zebrasolar.model.Job;
import com.journaldev.zebrasolar.model.Users;
import com.journaldev.zebrasolar.thread.InternetCheckerRunnable;
import com.journaldev.zebrasolar.thread.BackendSynchronizeHandler;
import com.journaldev.zebrasolar.utils.CrashReportUtils;
import com.journaldev.zebrasolar.utils.billPrinter.BillPrinterFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class SalesMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
         AppEventListener,
        EmailFragment.OnFragmentInteractionListener,
        SummaryFragment.OnFragmentInteractionListener,
        AdminFragment.OnFragmentInteractionListener,
        PhoneBillFragment.OnFragmentInteractionListener,
        CardTab.OnFragmentInteractionListener,
        PrevTab.OnFragmentInteractionListener,
        PaymentTab.OnFragmentInteractionListener,
        NumPadFragment.OnFragmentInteractionListener

        {

    public static int SOURCE_ID = 1; // TODO should be rep's ID
    public static String APP_USER_NAME = "Mr. Mahinda"; // TODO get from D

    public static BillPrinterFactory.BILL_PRINT_ID CLIENT_ID = BillPrinterFactory.BILL_PRINT_ID.BANDARANAYAKA;

    //user name
    String m_user = null;
    Integer m_userId = null;
    JobManager m_jobManager = null;
    PayManager m_payManager = null;
    SyncOperations m_syncOperations = null;

    private Fragment m_currFragment = null;

    TextView internetSatus = null;
    TextView jobPaySatus = null;
    ImageView statusImage = null;

    //**************Job set****************************************//
    public Job get_currentJob() {
        return m_currentJob;
    }
    public void set_currentJob(Job m_currentJob) {
        this.m_currentJob = m_currentJob;
    }
    private Job m_currentJob = null;
    //***************END Customer set************************************//

    //**************Card set****************************************//
    public CardItem getM_currentCardItem() {
        return m_currentCardItem;
    }

    public void setM_currentCardItem(CardItem m_currentCardItem) {
        this.m_currentCardItem = m_currentCardItem;
    }

    private CardItem m_currentCardItem = null;


    //Fault Tolaranace handler
    private FtHandler m_ftHandler = null;

    //syncronize handler
    private BackendSynchronizeHandler m_syncHandler = null;


    //constructor
    public SalesMainActivity() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //crash report
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {

                try{
                    DataBaseHelper dbhelper = DataBaseHelper.getInstance(getApplicationContext());
                    //dbhelper.close();
                }
                catch (Exception e)
                {

                }

                String strReport = CrashReportUtils.generateCrashReport(paramThread, paramThrowable);
                Log.e("Report ::", strReport);
                String filePath = CrashReportUtils.writeCrashReport(strReport);

                /*Intent crashedIntent = new Intent(SalesMainActivity.this, CrashHandlerActivity.class);
                crashedIntent.putExtra("REPORT", strReport);
                crashedIntent.putExtra("ERRFILE", filePath);
                crashedIntent.putExtra("ACT", "Sales");
                crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(crashedIntent);*/

            }
        });

        //crash report end















        setContentView(R.layout.activity_sales_main);

        CardUtils.setPoyaDays();

        //TODO check following code is valid or not
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Navigation Drawer initialization
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        internetSatus = (TextView) this.findViewById(R.id.internet_status);
        jobPaySatus = (TextView) this.findViewById(R.id.job_pay_status);
        statusImage = (ImageView) this.findViewById(R.id.status_image);

        m_jobManager = new JobManager(getApplicationContext());
        m_payManager = new PayManager(getApplicationContext());
        m_syncOperations = new SyncOperations(getApplicationContext());


        //**********Thread Creation for BackendSynchronizeHandler****************//
        HandlerThread myThread = new HandlerThread("Queue Handler Thread");
        myThread.start();
        Looper mLooper = myThread.getLooper();
        BackendSynchronizeHandler mHandler = new BackendSynchronizeHandler(mLooper, getApplicationContext());
        m_syncHandler = mHandler;
        m_syncHandler.setEventHandler(this);
        //**********Thread Creation End******************************************//



        //*************ft handler initialization********************************//
        m_user = this.getIntent().getExtras().getString("userName");
        Users.set_currentUser(m_user);

        //user instance should create to call Users.getCurrentUserId()
        Users.instance(this);
        m_userId = Users.getCurrentUserId();
        FtHandler ftHandlerObj = new FtHandler(m_syncHandler,this);

        if(m_userId == -1)//This is the first time user login to this app.So Ask them to update
        {
            Toast.makeText(getApplicationContext(), "First Time Login.Please Update and Continue to work.\n", Toast.LENGTH_SHORT).show();
        }
        else
        {
            ftHandlerObj.init(getApplicationContext(),Integer.toString(m_userId));
        }
        m_ftHandler = ftHandlerObj;


        //*************current fragment setter********************************//
        m_currFragment = new CardsAllFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.content_frame, m_currFragment);
        ft.commit();



        //*************set internet connection check thread here***************//
        InternetCheckerRunnable internetCheckerRunnable =  new InternetCheckerRunnable(mHandler,getApplicationContext());
        Thread internetThread = new Thread(internetCheckerRunnable);
        internetThread.start();


        //*************SMS Granting******************************************//
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            Log.d("PLAYGROUND", "Permission is not granted, requesting");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 123);

        } else {
            Log.d("PLAYGROUND", "Permission is granted");
        }

        //*************SMS Granting******************************************//
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("PLAYGROUND", "Permission is not granted, requesting");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);

        } else {
            Log.d("PLAYGROUND", "Permission is granted");
        }



    }


    public BackendSynchronizeHandler getM_backendSynchronizeHandler() {
        return m_syncHandler;
    }



    public void onCardSelected(CardItem cardItem){


        m_currFragment = new PhoneBillFragment();
        ((PhoneBillFragment) m_currFragment).setM_ftHandler(m_ftHandler);
        ((PhoneBillFragment) m_currFragment).setM_backendSynchronizeHandler(m_syncHandler);
        ((PhoneBillFragment) m_currFragment).setM_currUser(m_user);


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, m_currFragment);
        ft.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {

        ////////Checker to check bill is saved to the data base//////////////////////
        //check current fragment is billing
        /*if(m_currFragment instanceof PaymentFragment)
        {
            final PaymentFragment bill = (PaymentFragment)m_currFragment;
            if (bill.iscurrentBillSaved == false)
            {
                Log.e("Bill saving status","current bill not saved Do you want to continue?");
                new AlertDialog.Builder(this)
                        .setTitle("Bill Not Saved")
                        .setMessage("Current bill not saved Do you want to go to selcted menu item?(Click yes) \n " +
                                "To Go back and save bill click Cancel ")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               bill.iscurrentBillSaved = true;
                               onNavigationItemSelected(menuItem);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();


                return false;
            }
            else{
                Log.e("Bill saving status","current bill  saved");
            }

        }*/
        ////////Checker END (to check bill is saved to the data base)//////////////////////



        int id = menuItem.getItemId();

        Intent intent = null;
        switch (id)
        {
            case R.id.nav_job_new: {
                m_currFragment = new CardsAllFragment();
                break;
            }

            case R.id.nav_job: {
                m_currFragment = new CardsFragment();
                break;
            }

            case R.id.nav_summary: {
                m_currFragment = new SummaryFragment();
                break;
            }


            case R.id.nav_update:{
                m_currFragment = new UpdateFragment(m_syncHandler);
                break;
            }
            /*case R.id.nav_email:
            {
                m_currFragment = new EmailFragment();
                break;
            }
            case R.id.nav_admin:
            {
                m_currFragment = new AdminFragment();
                break;
            }*/
            case R.id.nav_logout:{
                startActivity(new Intent(this, SignInActivity.class));
                break;
            }
            default:
                m_currFragment = new CardsFragment();


        }
        if (m_currFragment != null)
        {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, m_currFragment);
            ft.commit();
        }
        else
        {
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } /*else {
            super.onBackPressed();
        }*/


        else if (m_currFragment instanceof PhoneBillFragment) {
            final PhoneBillFragment bill = (PhoneBillFragment) m_currFragment;

            m_currFragment = new CardsAllFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, m_currFragment);
            ft.commit();


        }

        else{
            super.onBackPressed();
        }
    }


    @Override
    public void onEvent(final AppEventMsg event) {
        switch (event.getEvent())
        {
            case BILL_FRAG_ON_ITEM_CLICKED:
            {
            }

            case REF_DATA_DYNAMIC_UPDATE:
            {

            }

            case INTERNET_ONLINE:
            {
                Log.w("Sales Main Thread", "Online");
                this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        internetSatus.setText("Online");
                        statusImage.setImageResource(R.drawable.greenbulb);
                        jobPaySatus.setText("Total Payments " + m_payManager.getTodayPayments() + " are synced.");


                    }
                });

                break;
            }
            case INTERNET_OFFLINE:
            {
                Log.w("Sales Main Thread", "offline");
                this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        internetSatus.setText("Offline");
                        statusImage.setImageResource(R.drawable.redbulb);

                        //update last bill id with database sync table

                        int syncedLastPaySeq = m_syncOperations.getLastSeq(
                                Enums.TableType.PAYMENT,Integer.toString(Users.getCurrentUserId()));

                        int currentLastPaySeq = m_payManager.getLastPaySeqForUser(Integer.toString(Users.getCurrentUserId()));

                        int payDiff = currentLastPaySeq - syncedLastPaySeq;

                        jobPaySatus.setText("offline - payments to Sync "+payDiff);

                    }
                });

                break;
            }
            case SENDING_JOB_TO_SERVER:
            case SENDING_JOB_TO_SERVER_SUCCESS:
            case SENDING_JOB_TO_SERVER_FAILED:
            case SENDING_PAYMENT_TO_SERVER:
            case SENDING_PAYMENT_TO_SERVER_SUCCESS:
            case SENDING_PAYMENT_TO_SERVER_FAILED:
            {
                Log.w("Sales Main Thread", "sending job or payment");
                this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        String msg = (String)event.getUserData();
                        jobPaySatus.setText(msg);

                    }
                });

                break;
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onNumPadFragmentInteraction(int sourceId,int value) {
        //Toast.makeText(this, " button pressed on main activity  \n", Toast.LENGTH_SHORT).show();

        if(sourceId == Enums.NUMPADSOURCE.PAYMENTS.getValue()) {
            ((PhoneBillFragment) m_currFragment).getListTabFragment().setText(Integer.toString(value));
        }
    }

    }

