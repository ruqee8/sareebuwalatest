package com.journaldev.zebrasolar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.journaldev.zebrasolar.db.DataBaseHelper;
import com.journaldev.zebrasolar.pojo.SignIn;
import com.journaldev.zebrasolar.utils.CrashReportUtils;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextEmail, editTextPassword;
    private Button buttonSignIn;

    APIInterface apiInterface;
    APIInterface apiInterfaceWithoutKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {

                try{
                    DataBaseHelper dbhelper = DataBaseHelper.getInstance(getApplicationContext());
                    //dbhelper.close();
                }
                catch (Exception e)
                {

                }

                String strReport = CrashReportUtils.generateCrashReport(paramThread, paramThrowable);
                Log.e("Report ::", strReport);
                String filePath = CrashReportUtils.writeCrashReport(strReport);

                Log.e("Report ::", strReport);
                /*Intent crashedIntent = new Intent(SignInActivity.this, CrashHandlerActivity.class);
                crashedIntent.putExtra("REPORT", strReport);
                crashedIntent.putExtra("ACT", "Sign In");
                crashedIntent.putExtra("ERRFILE", filePath);
                crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                crashedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(crashedIntent);*/
            }
        });













        setContentView(R.layout.activity_sign_in);

        //initialize gui components
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonSignIn = (Button) findViewById(R.id.buttonSignIn);


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String email = sharedPref.getString("userName","none");
        String password = sharedPref.getString("password","none");
        editTextEmail.setText(email);
        editTextPassword.setText(password);


        /*REST API Interfaces initialization
        * 1 - KEY Token Base API
        * 2 - API Without Key -- use for login
        */
        apiInterface = APIClient.getClient().create(APIInterface.class);
        apiInterfaceWithoutKey = APIClient.getClientWithoutKey().create(APIInterface.class);


        //Setting listners for buttons
        buttonSignIn.setOnClickListener(this);


    }

    /*/////////////////////////////////////////////////
     * Button CallBacks implements here
     */////////////////////////////////////////////////
    @Override
    public void onClick(View view) {
        if (view == buttonSignIn) {
            userSignIn();
        }
    }

    /*/////////////////////////////////////////////////
    * Sign in User
     */////////////////////////////////////////////////
    private void userSignIn() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing In...");
        progressDialog.show();

        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();


        if(isFirstTime() == false) {

            boolean isPrevLogged = isValidUser(email, password);

            if (isPrevLogged == true) {
                callSalesMainActivity();
                progressDialog.dismiss();
                return;
            }

            Toast.makeText(getApplicationContext(), " System support only one user\n" +
                    "Invalid Credentials. Please Try Again..\n", Toast.LENGTH_SHORT).show();
            return;
        }

        Call<SignIn> call = apiInterfaceWithoutKey.loginUserWithoutKey(email,password);
        call.request().url();
        Log.d("Test",call.toString());
        call.enqueue(new Callback<SignIn>() {
            @Override
            public void onResponse(Call<SignIn> call, Response<SignIn> response) {

                SignIn signIn = response.body();
                boolean status = signIn.getStatus();

                if(status == false)
                {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), " Invalid Credentials. Please Try Again..\n", Toast.LENGTH_SHORT).show();
                    return;
                }

                //set Recieved key here
                APIClient.token_key = signIn.getToken();
                progressDialog.dismiss();

                saveValidUser(email,password);

                //if the validation is ok then go marker
                callSalesMainActivity();
            }

            @Override
            public void onFailure(Call<SignIn> call, Throwable t) {
                Toast.makeText(getApplicationContext(),  " Failed\n", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                call.cancel();
            }
        });
    }

    /*////////////////////////////////////////////////////////////////////
   *  on Success login for marker this method will start marker activity
    *////////////////////////////////////////////////////////////////////
    private void callSalesMainActivity()
    {
        Intent intent = new Intent(this, SalesMainActivity.class);
        intent.putExtra("userName", editTextEmail.getText().toString().trim());
        startActivity(intent);
    }




    /*////////////////////////////////////////////////////////////////////
     *  check user name and password in shared preferences
     *////////////////////////////////////////////////////////////////////
    private boolean isValidUser(String userName,String password)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String uName = sharedPref.getString("userName","none");
        String pwd = sharedPref.getString("password","none");

        if(userName.equals(uName) && password.equals(pwd)) {
            Log.d("Test","logged via shared preferences.");
            return true;
        }

        return false;


    }

    private boolean isFirstTime()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPref.contains("userName") == false)
        {
            return true;
        }

        return false;
    }

    /*////////////////////////////////////////////////////////////////////
     *  check user name and password in shared preferences
     *////////////////////////////////////////////////////////////////////
    private void saveValidUser(String userName,String password)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("userName",userName);
        editor.putString("password",password);
        editor.commit();

    }
}