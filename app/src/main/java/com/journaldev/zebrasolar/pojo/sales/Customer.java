package com.journaldev.zebrasolar.pojo.sales;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("route_id")
    @Expose
    private String routeId;

    @SerializedName("carry_forward")
    @Expose
    private Integer carryForward;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public Integer getCarryForward() { return carryForward; }

    public void setCarryForward(Integer carry_forward) { this.carryForward = carry_forward; }

    public String getPhone() { return  phone; }

    public void setPhone(String phone) { phone = phone; }
}