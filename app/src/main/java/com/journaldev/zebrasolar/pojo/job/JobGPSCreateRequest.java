package com.journaldev.zebrasolar.pojo.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobGPSCreateRequest {

    @SerializedName("job_id")
    @Expose
    private String jobId;

    @SerializedName("ref_id")
    @Expose
    private int RefId;

    @SerializedName("ref_seq_id")
    @Expose
    private int RefSeqId;



    @SerializedName("latitude")
    @Expose
    private float latitude;

    @SerializedName("longitude")
    @Expose
    private float longitude;


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getRefId() {
        return RefId;
    }

    public void setRefId(int refId) {
        RefId = refId;
    }

    public int getRefSeqId() {
        return RefSeqId;
    }

    public void setRefSeqId(int refSeqId) {
        RefSeqId = refSeqId;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }


    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

}
