
        package com.journaldev.zebrasolar.pojo;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class Logout {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}