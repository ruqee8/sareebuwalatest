package com.journaldev.zebrasolar.pojo.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobCreateResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("ref_id")
    @Expose
    private int refId;

    @SerializedName("ref_seq_id")
    @Expose
    private int refSeq;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getRefSeq() {
        return refSeq;
    }

    public void setRefSeq(int refSeq) {
        this.refSeq = refSeq;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }
}
