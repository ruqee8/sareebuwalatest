package com.journaldev.zebrasolar.pojo.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category_id")
    @Expose
    private Integer category_id;
    @SerializedName("UOM_id")
    @Expose
    private Integer UOM_id;
    @SerializedName("quantification_val")
    @Expose
    private Double quantVal;
    @SerializedName("printed_price")
    @Expose
    private Double printedPrice;
    @SerializedName("selling_price")
    @Expose
    private Double sellingPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUOMId() {
        return UOM_id;
    }

    public void setUOMId(Integer measurementId) {
        this.UOM_id = measurementId;
    }

    public Integer getCategory_id() { return category_id; }

    public void setCategory_id(Integer category_id) { this.category_id = category_id; }

    public Double getQuantVal() { return quantVal; }

    public void setQuantVal(Double quantVal) { this.quantVal = quantVal; }

    public Double getSellingPrice() { return sellingPrice;}

    public void setSellingPrice(Double price) { this.sellingPrice = price; }

    public Double getPrintedPrice() { return printedPrice;}

    public void setPrintedPrice(Double price) { this.printedPrice = price; }

}