package com.journaldev.zebrasolar.pojo.email;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailSend {

    @SerializedName("ref_id")
    @Expose
    private String refId;


    @SerializedName("time")
    @Expose
    private String time;



    @SerializedName("msg")
    @Expose
    private String message;


    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
