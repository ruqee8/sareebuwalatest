package com.journaldev.zebrasolar.pojo.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentCreateRequest {


    @SerializedName("payment_id")
    @Expose
    private int PaymentId;

    @SerializedName("job_id")
    @Expose
    private String jobId;

    @SerializedName("ref_id")
    @Expose
    private int RefId;

    @SerializedName("ref_seq")
    @Expose
    private int RefSeqId;

    @SerializedName("payment_amount")
    @Expose
    private double paymentAmount;

    @SerializedName("panalty_amount")
    @Expose
    private double panaltyAmount;

    @SerializedName("pay_cash_cheque_type")
    @Expose
    private int payCashChequeType;

    @SerializedName("pay_type")
    @Expose
    private int payType;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("next_date")
    @Expose
    private String nextDate;

    @SerializedName("cheque_status")
    @Expose
    private int chequeStatus;

    @SerializedName("status")
    @Expose
    private int status;



    public int getPaymentId() {
        return PaymentId;
    }

    public void setPaymentId(int paymentId) {
        PaymentId = paymentId;
    }


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getRefId() {
        return RefId;
    }

    public void setRefId(int refId) {
        RefId = refId;
    }

    public int getRefSeqId() {
        return RefSeqId;
    }

    public void setRefSeqId(int refSeqId) {
        RefSeqId = refSeqId;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getPanaltyAmount() {
        return panaltyAmount;
    }

    public void setPanaltyAmount(double panaltyAmount) {
        this.panaltyAmount = panaltyAmount;
    }

    public int getPayCashChequeType() {
        return payCashChequeType;
    }

    public void setPayCashChequeType(int payCashChequeType) {
        this.payCashChequeType = payCashChequeType;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String date) {
        this.nextDate = date;
    }

    public int getChequeStatus() {
        return chequeStatus;
    }

    public void setChequeStatus(int chequeStatus) {
        this.chequeStatus = chequeStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
