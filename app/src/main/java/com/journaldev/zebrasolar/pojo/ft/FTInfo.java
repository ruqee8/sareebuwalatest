package com.journaldev.zebrasolar.pojo.ft;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FTInfo {

    @SerializedName("job_seq")
    @Expose
    private Integer jobSeq;

    @SerializedName("payment_seq")
    @Expose
    private Integer paySeq;

    public Integer getJobSeq() {
        return jobSeq;
    }

    public void setJobSeq(Integer id) {
        this.jobSeq = id;
    };

    public Integer getPaySeq() {
        return paySeq;
    }

    public void setPaySeq(Integer id) {
        this.paySeq = id;
    };
}