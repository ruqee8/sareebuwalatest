package com.journaldev.zebrasolar.pojo.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobOrPaymentSyncRequest {

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastRefSeq() {
        return lastRefSeq;
    }

    public void setLastRefSeq(String lastRefSeq) {
        this.lastRefSeq = lastRefSeq;
    }

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("last_ref_seq")
    @Expose
    private String lastRefSeq;


}
