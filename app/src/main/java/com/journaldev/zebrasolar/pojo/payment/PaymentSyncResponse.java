package com.journaldev.zebrasolar.pojo.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentSyncResponse {

    @SerializedName("ref_id")
    @Expose
    private Integer refId;
    @SerializedName("ref_seq_id")
    @Expose
    private Integer refSeqId;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("payment_amount")
    @Expose
    private double paymentAmount;

    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;


    @SerializedName("panalty_amount")
    @Expose
    private double panaltyAmount;
    @SerializedName("pay_type")
    @Expose
    private Integer payType;
    @SerializedName("pay_cash_cheque_type")
    @Expose
    private Integer payCashChequeType;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("next_date")
    @Expose
    private String nextDate;

    @SerializedName("cheque_status")
    @Expose
    private Integer chequeStatus;

    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getChequeStatus() {
        return chequeStatus;
    }

    public void setChequeStatus(Integer chequeStatus) {
        this.chequeStatus = chequeStatus;
    }


    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public Integer getRefSeqId() {
        return refSeqId;
    }

    public void setRefSeqId(Integer refSeqId) {
        this.refSeqId = refSeqId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }


    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getPanaltyAmount() {
        return panaltyAmount;
    }

    public void setPanaltyAmount(double panaltyAmount) {
        this.panaltyAmount = panaltyAmount;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getPayCashChequeType() {
        return payCashChequeType;
    }

    public void setPayCashChequeType(Integer payCashChequeType) {
        this.payCashChequeType = payCashChequeType;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String date) {
        this.nextDate = date;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}
