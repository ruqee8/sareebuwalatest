package com.journaldev.zebrasolar.pojo.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentCreateResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("source_seq")
    @Expose
    private String sourceSeq;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getSourceSeq() {
        return sourceSeq;
    }

    public void setSourceSeq(String sourceSeq) {
        this.sourceSeq = sourceSeq;
    }
}
