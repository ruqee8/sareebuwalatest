package com.journaldev.zebrasolar.pojo.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobCreateRequest {

    @SerializedName("orgin_id")
    @Expose
    private Integer orginId;
    @SerializedName("ref_id")
    @Expose
    private Integer refId;
    @SerializedName("ref_seq_id")
    @Expose
    private Integer refSeqId;
    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("collector_id")
    @Expose
    private Integer collectorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("installment_type")
    @Expose
    private Integer installmentType;
    @SerializedName("total_amount")
    @Expose
    private double totalAmount;
    @SerializedName("prod_id")
    @Expose
    private Integer prodId;
    @SerializedName("area_id")
    @Expose
    private Integer areaId;
    @SerializedName("quantity")
    @Expose
    private double quantity;
    @SerializedName("down_payment")
    @Expose
    private double downPayment;
    @SerializedName("no_of_installments")
    @Expose
    private Integer noOfInstallments;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;


    public Integer getOrginId() {
        return orginId;
    }

    public void setOrginId(Integer orginId) {
        this.orginId = orginId;
    }

    public Integer getRefId() {
        return refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public Integer getRefSeqId() {
        return refSeqId;
    }

    public void setRefSeqId(Integer refSeqId) {
        this.refSeqId = refSeqId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Integer getCollectorId() {
        return collectorId;
    }

    public void setCollectorId(Integer collectorId) {
        this.collectorId = collectorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getInstallmentType() {
        return installmentType;
    }

    public void setInstallmentType(Integer installment) {
        this.installmentType = installment;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getProdId() {
        return prodId;
    }

    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }


    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    public Integer getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(Integer noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
