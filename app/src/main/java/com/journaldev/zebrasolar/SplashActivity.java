package com.journaldev.zebrasolar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rukshanr
 * This is the first activity (Defined as launcher activity in android manifest xml)
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Timer().schedule(new TimerTask(){
            public void run() {
                SplashActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        //This will call MainActivity after the Splash
                        startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                    }
                });
            }
        }, 2000);
    }
}
